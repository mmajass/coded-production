/**
 * Copyright (c) 2016 - 2018, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "nordic_common.h"
#include "app_error.h"
#include "app_uart.h"
#include "ble_db_discovery.h"
#include "app_timer.h"
#include "app_util.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "ble_gap.h"
#include "ble_hci.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "ble_advdata.h"
#include "ble_nus_c.h"
#include "nrf_ble_gatt.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_drv_twi.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


#define APP_BLE_CONN_CFG_TAG    1                                       /**< A tag that refers to the BLE stack configuration we set with @ref sd_ble_cfg_set. Default tag is @ref BLE_CONN_CFG_TAG_DEFAULT. */
#define APP_BLE_OBSERVER_PRIO   3                                       /**< Application's BLE observer priority. You shoulnd't need to modify this value. */

#define UART_TX_BUF_SIZE        256                                     /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE        256                                     /**< UART RX buffer size. */

#define NUS_SERVICE_UUID_TYPE   BLE_UUID_TYPE_VENDOR_BEGIN              /**< UUID type for the Nordic UART Service (vendor specific). */

#define SCAN_INTERVAL           0x0050                                  /**< Original 50 0x00A0 Determines scan interval in units of 0.625 millisecond. */
#define SCAN_WINDOW             0x0050                                  /**< Determines scan window in units of 0.625 millisecond. */
#define SCAN_DURATION           0x0000                                  /**< Timout when scanning. 0x0000 disables timeout. */
#define SCAN_TIMEOUT            0x0000                                  /**< Timout when scanning. 0x0000 disables timeout. */


#define MIN_CONNECTION_INTERVAL MSEC_TO_UNITS(7.5, UNIT_1_25_MS)         /**< Determines minimum connection interval in millisecond. */
#define MAX_CONNECTION_INTERVAL MSEC_TO_UNITS(30, UNIT_1_25_MS)         /**< Determines maximum connection interval in millisecond. */
#define SLAVE_LATENCY           0                                       /**< Determines slave latency in counts of connection events. */
#define SUPERVISION_TIMEOUT     MSEC_TO_UNITS(4000, UNIT_10_MS)         /**<  ORIGINAL 4000 Determines supervision time-out in units of 10 millisecond. */

#define ECHOBACK_BLE_UART_DATA  0                                       /**< Echo the UART data that is received over the Nordic UART Service back to the sender. */

#define COMPANY_ID                  0x0059                              /**< Nordic Semiconductor. */
#define MANU_SPEC_DATA_MIN_LENGTH   24                                  /**< Minimum length of manufacturer specific data. */


BLE_NUS_C_DEF(m_ble_nus_c);                                             /**< BLE NUS service client instance. */
NRF_BLE_GATT_DEF(m_gatt);                                               /**< GATT module instance. */
BLE_DB_DISCOVERY_DEF(m_db_disc);                                        /**< DB discovery module instance. */

//static uint16_t                         m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */




static uint16_t m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - OPCODE_LENGTH - HANDLE_LENGTH; /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */

/* Timer for setting up sensor readings = 1 second*/
#define SENSOR_DATA_INTERVAL     APP_TIMER_TICKS(1000)                 /**< Tick / Ms */
/* Timer for saving sensor readings in seconds*/
#define SAVE_DATA_INTERVAL			900 
#define FLASH_DATA_INTERVAL     APP_TIMER_TICKS(1000)   
/* Timer for managing activities while connected to phone */
APP_TIMER_DEF(snsr_timer_id);
/* Timer for saving data while not connected */
APP_TIMER_DEF(snsr_flash_id);

static bool isConnected = false;
static uint8_t isFirstPacket = 0;
int uptime_counter;
static char sensor_name[20];
char sensor_names[50][21];
uint8_t arr_counter = 0;

static volatile uint8_t rssi;
static const char m_target_periph_name[] = "MCV";    /**< Name of the device we try to connect to. This name is searched for in the scan report data*/

ble_gap_evt_adv_report_t const * p_adv_report;

char device_name[4];
int buff_counter;
uint8_t ubuff[125];
static volatile bool name_matches = false;
static volatile bool beacon_scan = true;
static volatile bool sensor_sequence = true;
/* Number of packets added into buffer before sending */

#define	BUFFSIZE	3

/* The board uses SKYWORKS SKY66114-11 amplifier, here are the amplifier controls */

#define CPS          NRF_GPIO_PIN_MAP(1,15)
#define CRX          NRF_GPIO_PIN_MAP(1,04)
#define CTX          NRF_GPIO_PIN_MAP(1,13)

/* TWI instance ID. */
#define TWI_INSTANCE_ID     0

/* Common addresses definition for temperature sensor. */
#define SHTC1_ADDR         	0x70U // (0x90U >> 1)

#define SHTC1_ReadIdR1 			 						0xEF // command: read ID register
#define SHTC1_ReadIdR2 			 						0xC8 // command: read ID register
#define SHTC1_SoftReset1 		 						0x80 // soft reset
#define SHTC1_SoftReset2 		 						0x5D // soft reset
#define SHTC1_MeasureTempHumPolling1 	 	0x78 // meas. read T first, clock stretching disabled
#define SHTC1_MeasureTempHumPolling2 	 	0x66 // meas. read T first, clock stretching disabled
#define SHTC1_MeasureTempHumClockstr1 	0x7C // meas. read T first, clock stretching enabled
#define SHTC1_MeasureTempHumClockstr2 	0xA2 // meas. read T first, clock stretching enabled
#define SHTC1_MeasureHumTempPolling 		0x58E0 // meas. read RH first, clock stretching disabled
#define SHTC1_MeasureHumTempClockstr 		0x5C24 // meas. read RH first, clock stretching enabled


static volatile float mTemperature;
static volatile float mHumidity;

/* Indicates if operation on TWI has ended. */
static volatile bool m_xfer_done = false;

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

/* Buffer for samples read from temperature sensor. */
static uint8_t m_sample[6];

static void ble_nus_chars_received_uart_print(uint8_t * p_data, uint16_t data_len);
/**
 * @brief Function for setting active mode on MMA7660 accelerometer.
 */
void SHTC1_set_mode(void)
{
    ret_code_t err_code;
		m_xfer_done = false;
    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
	
    uint8_t reg[2] = {SHTC1_MeasureTempHumClockstr1, SHTC1_MeasureTempHumClockstr2};
		NRF_LOG_INFO("setting mode");
		
    err_code = nrf_drv_twi_tx(&m_twi, SHTC1_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		NRF_LOG_INFO("mode setup done");
		

}


/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
			
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
                //NRF_LOG_INFO(" RX event done with data %d - %d - %d - %d - %d - %d", m_sample[0],m_sample[1],m_sample[2],m_sample[3], m_sample[4], m_sample[5]);
								//NRF_LOG_FLUSH();
								
								uint16_t val1;
								val1 = (m_sample[0] << 8) + m_sample[1];
								mTemperature = -45.0 + 175.0 * (val1 / 65535.0);
							
								//NRF_LOG_INFO("Temperature: %.2f Celsius degrees.", mTemperature);
								uint16_t val2;
								val2 = (m_sample[3] << 8) + m_sample[4];
								mHumidity = 100.0 * (val2 / 65535.0);
							
								//NRF_LOG_INFO("Humidity: %.2f percent.", mHumidity);
								uint8_t buff1[64];
								memset(buff1, 0, sizeof(buff1));	
					
								/** Sending either temperature or humidity at time to avoid problems with merged strings and buffer */
							 if(!sensor_sequence){
									
									sprintf((char *)&buff1, "n=%st&v=%.2f",device_name, mTemperature);
									sensor_sequence = true;
									
								}else{
									
									sprintf((char *)&buff1, "n=%sh&v=%.2f",device_name, mHumidity);
									sensor_sequence = false;
								}
									
								printf((char *)buff1);
								//sprintf((char *)&buff2, "n=%sh&v=%.2f",device_name, mHumidity);
								//NRF_LOG_INFO("uart->%s", (char *)&buff1);
								//ble_nus_chars_received_uart_print(buff1, sizeof(buff1));
								//ble_nus_chars_received_uart_print(buff2, sizeof(buff2));
								memset(m_sample, 0, sizeof(m_sample));
								
							
								//LM75B_reset();
								//LM75B_set_mode();
            }
            m_xfer_done = true;
            break;
        default:
            break;
    }
}

/**
 * @brief TWI initialization.
 */
void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_shtc1_config = {
       .scl                = 4,
       .sda                = 26,
       .frequency          = NRF_DRV_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = true
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_shtc1_config, twi_handler, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}


/**
 * @brief Function for reading data from temperature sensor.
 */
static void read_sensor_id()
{
    m_xfer_done = false;
		/* Write to the SHTC1 address */
		uint8_t reg[2] = {SHTC1_ReadIdR1, SHTC1_ReadIdR2};
    ret_code_t err_code = nrf_drv_twi_tx(&m_twi, SHTC1_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		
		m_xfer_done = false;
    /* Read from the SHTC1 address */
    err_code = nrf_drv_twi_rx(&m_twi, SHTC1_ADDR, m_sample, sizeof(m_sample));
    APP_ERROR_CHECK(err_code);
		while (m_xfer_done == false);
		
}



/**
 * @brief Function for reading data from temperature sensor.
 */
static void read_sensor_data()
{
    m_xfer_done = false;

    /* Read data from SHTC1 address */
    ret_code_t err_code = nrf_drv_twi_rx(&m_twi, SHTC1_ADDR, m_sample, sizeof(m_sample));
    APP_ERROR_CHECK(err_code);
}



/**@brief Connection parameters requested for connection. */
static ble_gap_conn_params_t const m_connection_param =
{
    (uint16_t)MIN_CONNECTION_INTERVAL,  // Minimum connection
    (uint16_t)MAX_CONNECTION_INTERVAL,  // Maximum connection
    (uint16_t)SLAVE_LATENCY,            // Slave latency
    (uint16_t)SUPERVISION_TIMEOUT       // Supervision time-out
};

static uint8_t m_scan_buffer_data[BLE_GAP_SCAN_BUFFER_MIN]; /**< buffer where advertising reports will be stored by the SoftDevice. */

/**@brief Pointer to the buffer where advertising reports will be stored by the SoftDevice. */
static ble_data_t m_scan_buffer =
{
    m_scan_buffer_data,
    BLE_GAP_SCAN_BUFFER_MIN
};

/** @brief Parameters used when scanning. */
static ble_gap_scan_params_t const m_scan_params =
{
    
	 
	//working 1MBPS phy
		.active   = 1,
    .interval = SCAN_INTERVAL,
    .window   = SCAN_WINDOW,
    .timeout          = SCAN_DURATION,
    .scan_phys        = BLE_GAP_PHY_1MBPS, //BLE_GAP_PHY_CODED or BLE_GAP_PHY_1MBPS
    .filter_policy    = BLE_GAP_SCAN_FP_ACCEPT_ALL,
	
	/*

		not working coded phy
	
	  .active            = 0,
    .interval          = SCAN_INTERVAL,
    .window            = SCAN_WINDOW,
    .filter_policy     = BLE_GAP_SCAN_FP_ACCEPT_ALL,
    .filter_duplicates = BLE_GAP_SCAN_DUPLICATES_REPORT,
    .scan_phy          = BLE_GAP_PHY_CODED,
    .duration          = SCAN_TIMEOUT,
    .period            = 0x0000, // No period.
	
	
	
	
	
		.active            = 0,
    .interval          = SCAN_INTERVAL,
    .window            = SCAN_WINDOW,
    .filter_policy     = BLE_GAP_SCAN_FP_ACCEPT_ALL,
    .filter_duplicates = BLE_GAP_SCAN_DUPLICATES_REPORT,
    .scan_phy          = BLE_GAP_PHY_CODED,
    .duration          = SCAN_TIMEOUT,
    .period            = 0x0000, // No period.
	
	*/
};

/**@brief NUS uuid. */
static ble_uuid_t const m_nus_uuid =
{
    .uuid = BLE_UUID_NUS_SERVICE,
    .type = NUS_SERVICE_UUID_TYPE
};


/**@brief Function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num     Line number of the failing ASSERT call.
 * @param[in] p_file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(0xDEADBEEF, line_num, p_file_name);
}


/**@brief Function to start scanning. */
static void scan_start(void)
{
    ret_code_t ret;
		
		NRF_LOG_INFO("Start scanning for device name %s.\r\n", (uint32_t)m_target_periph_name);
    ret = sd_ble_gap_scan_start(&m_scan_params, &m_scan_buffer);
    APP_ERROR_CHECK(ret);

    ret = bsp_indication_set(BSP_INDICATE_SCANNING);
    APP_ERROR_CHECK(ret);
}


/**@brief Function for handling database discovery events.
 *
 * @details This function is callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function should forward the events
 *          to their respective services.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */
static void db_disc_handler(ble_db_discovery_evt_t * p_evt)
{
    ble_nus_c_on_db_disc_evt(&m_ble_nus_c, p_evt);
}


/**@brief Function for handling characters received by the Nordic UART Service.
 *
 * @details This function takes a list of characters of length data_len and prints the characters out on UART.
 *          If @ref ECHOBACK_BLE_UART_DATA is set, the data is sent back to sender.
 */
static void ble_nus_chars_received_uart_print(uint8_t * p_data, uint16_t data_len)
{
    ret_code_t ret_val;

    NRF_LOG_DEBUG("Receiving data.");
    NRF_LOG_HEXDUMP_DEBUG(p_data, data_len);

    for (uint32_t i = 0; i < data_len; i++)
    {
        do
        {
            ret_val = app_uart_put(p_data[i]);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("app_uart_put failed for index 0x%04x.", i);
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }
    if (p_data[data_len-1] == '\r')
    {
        while (app_uart_put('\n') == NRF_ERROR_BUSY);
    }
    if (ECHOBACK_BLE_UART_DATA)
    {
        // Send data back to peripheral.
        do
        {
            ret_val = ble_nus_c_string_send(&m_ble_nus_c, p_data, data_len);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("Failed sending NUS message. Error 0x%x. ", ret_val);
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }
}


/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to
 *          a string. The string will be be sent over BLE when the last character received was a
 *          'new line' '\n' (hex 0x0A) or if the string has reached the maximum data length.
 */
void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint16_t index = 0;
    uint32_t ret_val;

    switch (p_event->evt_type)
    {
        /**@snippet [Handling data from UART] */
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&data_array[index]));
            index++;

            if ((data_array[index - 1] == '\n') || (index >= (m_ble_nus_max_data_len)))
            {
                NRF_LOG_DEBUG("Ready to send data over BLE NUS");
                NRF_LOG_HEXDUMP_DEBUG(data_array, index);

                do
                {
                    ret_val = ble_nus_c_string_send(&m_ble_nus_c, data_array, index);
                    if ( (ret_val != NRF_ERROR_INVALID_STATE) && (ret_val != NRF_ERROR_BUSY) )
                    {
                        APP_ERROR_CHECK(ret_val);
                    }
                } while (ret_val == NRF_ERROR_BUSY);

                index = 0;
            }
            break;

        /**@snippet [Handling data from UART] */
        case APP_UART_COMMUNICATION_ERROR:
            NRF_LOG_ERROR("Communication error occurred while handling UART.");
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            NRF_LOG_ERROR("Error occurred in FIFO module used by UART.");
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}


/**@brief Callback handling NUS Client events.
 *
 * @details This function is called to notify the application of NUS client events.
 *
 * @param[in]   p_ble_nus_c   NUS Client Handle. This identifies the NUS client
 * @param[in]   p_ble_nus_evt Pointer to the NUS Client event.
 */

/**@snippet [Handling events from the ble_nus_c module] */
static void ble_nus_c_evt_handler(ble_nus_c_t * p_ble_nus_c, ble_nus_c_evt_t const * p_ble_nus_evt)
{
    ret_code_t err_code;

    switch (p_ble_nus_evt->evt_type)
    {
        case BLE_NUS_C_EVT_DISCOVERY_COMPLETE:
            NRF_LOG_INFO("Discovery complete.");
            err_code = ble_nus_c_handles_assign(p_ble_nus_c, p_ble_nus_evt->conn_handle, &p_ble_nus_evt->handles);
            APP_ERROR_CHECK(err_code);

            err_code = ble_nus_c_tx_notif_enable(p_ble_nus_c);
            APP_ERROR_CHECK(err_code);
            NRF_LOG_INFO("Connected to device with Nordic UART Service.");
				
						isConnected = true;
						isFirstPacket = 0;
						ret_code_t err_code;
						err_code = app_timer_start(snsr_timer_id, SENSOR_DATA_INTERVAL, NULL);
						APP_ERROR_CHECK(err_code);
						//NRF_LOG_INFO("app timer start err code %d", err_code);
						
            break;

        case BLE_NUS_C_EVT_NUS_TX_EVT:
				{
            //ble_nus_chars_received_uart_print(p_ble_nus_evt->p_data, p_ble_nus_evt->data_len);
						/*for(int i = 0; i< p_ble_nus_evt->data_len; i++)
            {
							NRF_LOG_INFO("from uart %c",p_ble_nus_evt->p_data[i]);
            }
						*/
						//NRF_LOG_INFO("all from %s has rssi %d is %s", sensor_name, rssi, (const char *) p_ble_nus_evt->p_data); 
						/* Sending device name and Device ID to the server */
						nrf_gpio_pin_toggle(LED_4);
						char buf[25];
						sprintf(buf, "%s", (const char *) p_ble_nus_evt->p_data);
						int i = 0;
						char *p = strtok (buf, "/");
						char *vals[3];

						while (p != NULL)
						{
								vals[i++] = p;
								p = strtok (NULL, "/");
						}
						uint8_t buff1[64];
						//uint8_t buff2[64];
						//uint8_t buff3[64];
						memset(buff1, 0, sizeof(buff1));		 
						NRF_LOG_INFO("name is %s", sensor_name);
							/** Sending either temperature or humidity at time to avoid problems with merged strings and buffer */
						if (strlen(sensor_name) > 0){	 
							if(sensor_sequence){
									
									if(isFirstPacket < 2){
										//NRF_LOG_INFO("ok will try to add name to array %d", arr_counter);
										sprintf((char *)&buff1, "n=%s&v=%s&r=%d",sensor_name, vals[2], rssi);
										//int i = sizeof(sensor_names)/sizeof(sensor_names[0]);
										strncpy(sensor_names[arr_counter], sensor_name, 10);
										//memcpy(sensor_names[arr_counter], (const char*)sensor_name, 10);
										//NRF_LOG_INFO("added name, all names are %d, first name is %s, ", arr_counter, sensor_names[0]);
										arr_counter++;
										
										
										
									}else{
										
										sprintf((char *)&buff1, "n=%sh&v=%s",sensor_name, vals[0]);
										
									}
									sensor_sequence = false;
										
									
								}else{
									
									sprintf((char *)&buff1, "n=%st&v=%s",sensor_name, vals[1]);
									sensor_sequence = true;
								}
									
								printf((char *)buff1);
								
							}
								//NRF_LOG_INFO("uart->%s",(char *)buff1);
								
								
						//sprintf((char *)&buff1, "n=11h&v=51");
						//NRF_LOG_INFO("uart->%s", (char *)&buff1);
						//ble_nus_chars_received_uart_print(buff1, sizeof(buff1));
						//printf((char *)buff1);
						
//						memset(buff2, 0, sizeof(buff2));		 
//						sprintf((char *)&buff2, "n=%st&v=%s&r=%d",sensor_name, vals[1], rssi);
//						//NRF_LOG_INFO("uart->%s", (char *)&buff2);
//						ble_nus_chars_received_uart_print(buff2, sizeof(buff2));
//						//printf((char *)buff2);
//						memset(buff3, 0, sizeof(buff3));		 
//						sprintf((char *)&buff3, "n=%sb&v=%s&r=%d",sensor_name, vals[2], rssi);
//						NRF_LOG_INFO("uart->%s", (char *)&buff3);
//						ble_nus_chars_received_uart_print(buff3, sizeof(buff3));
//						/* Dump to UART. */
//						//printf((char *)buff3);
						
					}
            break;

        case BLE_NUS_C_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected.");
						memset(sensor_name, 0, 20);
						isConnected = false;
						name_matches = false;
						 //err_code = sd_ble_gap_scan_start(NULL, &m_scan_buffer);
						//APP_ERROR_CHECK(err_code);
						
						err_code = app_timer_stop(snsr_timer_id);
						APP_ERROR_CHECK(err_code);
				
						//try to reconnect
				/*
						err_code = sd_ble_gap_connect(&p_adv_report->peer_addr,
																			 &m_scan_params,
                                       &m_connection_param,
                                       APP_BLE_CONN_CFG_TAG);
						if (err_code != NRF_SUCCESS)
						{
								NRF_LOG_ERROR("Connection Request Failed, reason %d\r\n", err_code);
						}
				
						// scan is automatically stopped by the connect
								err_code = bsp_indication_set(BSP_INDICATE_IDLE);
								APP_ERROR_CHECK(err_code);
						
						NRF_LOG_INFO("Connecting to target");
   
						*/
            if (beacon_scan) scan_start();
            break;
    }
}
/**@snippet [Handling events from the ble_nus_c module] */




/**
 * @brief Function for shutdown events.
 *
 * @param[in]   event       Shutdown type.
 */
static bool shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
    ret_code_t err_code;

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    switch (event)
    {
        case NRF_PWR_MGMT_EVT_PREPARE_WAKEUP:
            // Prepare wakeup buttons.
            err_code = bsp_btn_ble_sleep_mode_prepare();
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(shutdown_handler, APP_SHUTDOWN_HANDLER_PRIORITY);

/**
 * @brief Parses advertisement data, providing length and location of the field in case
 *        matching data is found.
 *
 * @param[in]  type       Type of data to be looked for in advertisement data.
 * @param[in]  p_advdata  Advertisement report length and pointer to report.
 * @param[out] p_typedata If data type requested is found in the data report, type data length and
 *                        pointer to data will be populated here.
 *
 * @retval NRF_SUCCESS if the data type is found in the report.
 * @retval NRF_ERROR_NOT_FOUND if the data type could not be found.
 */
static uint32_t adv_report_parse(uint8_t type, uint8_array_t * p_advdata, uint8_array_t * p_typedata)
{
    uint32_t  index = 0;
    uint8_t * p_data;

    p_data = p_advdata->p_data;
		//uint8_t datal = p_advdata->size;
	
			nrf_gpio_pin_toggle(LED_2);
			nrf_gpio_pin_clear(LED_3);
			nrf_gpio_pin_clear(LED_4);
			
	
		
		//NRF_LOG_INFO("parsing for name.... data size is %d", datal);

    while (index < p_advdata->size)
    {
        uint8_t field_length = p_data[index];
        uint8_t field_type   = p_data[index + 1];
				//NRF_LOG_INFO("field is %d but required is %d",field_type, type);
				memset(sensor_name, 0, 20);
				
        if (field_type == type)
        {
            p_typedata->p_data = &p_data[index + 2];
            p_typedata->size   = field_length - 1;
						
						sprintf(sensor_name, "%s",(const char *) &p_data[index + 2]);
						
						NRF_LOG_INFO("found name %s", sensor_name);
					
						bool found_equal = false;
					
						nrf_gpio_pin_set(LED_3);
									
						for(int j=0;j<arr_counter; j++)
						{
							NRF_LOG_INFO("in memory %d is %s",j,sensor_names[j]);
							if(strncmp(sensor_names[j], sensor_name, 10) == 0){
								
								NRF_LOG_INFO("they are equal");
								found_equal = true;
								
							}
						}
						
						if (found_equal) NRF_LOG_INFO("found equal name, not connecting....");
						
            if(found_equal){
							
							return NRF_ERROR_NOT_FOUND;
							
						}else{
							
							return NRF_SUCCESS;
						}
        }
        index += field_length + 1;
    }
    return NRF_ERROR_NOT_FOUND;
}


/**@brief Function for handling the advertising report BLE event.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
//static void on_adv_report(const ble_evt_t * const p_ble_evt)
//{
//    uint32_t      err_code;
//    uint8_array_t adv_data;
//    uint8_array_t dev_name;
//    bool          do_connect = false;

//    // For readibility.
//    const ble_gap_evt_t * const p_gap_evt    = &p_ble_evt->evt.gap_evt;
//    const ble_gap_addr_t  * const peer_addr  = &p_gap_evt->params.adv_report.peer_addr;

//    // Initialize advertisement report for parsing
//    adv_data.p_data = (uint8_t *)p_gap_evt->params.adv_report.data.p_data;
//    adv_data.size   = p_gap_evt->params.adv_report.data.len;


//    //search for advertising names
//    bool found_name = false;
//    err_code = adv_report_parse(BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME,
//                                &adv_data,
//                                &dev_name);
//		NRF_LOG_INFO("parsing ad report -error %d and %s", err_code, &dev_name);
//    if (err_code != NRF_SUCCESS)
//    {
//        // Look for the short local name if it was not found as complete
//        err_code = adv_report_parse(BLE_GAP_AD_TYPE_SHORT_LOCAL_NAME, &adv_data, &dev_name);
//        if (err_code != NRF_SUCCESS)
//        {
//            // If we can't parse the data, then exit
//            return;
//        }
//        else
//        {
//            found_name = true;
//        }
//    }
//    else
//    {
//        found_name = true;
//    }
//    if (found_name)
//    {
//        if (strlen(m_target_periph_name) != 0)
//        {
//            
//						NRF_LOG_INFO("name is %s", m_target_periph_name);
//						if (memcmp(m_target_periph_name, dev_name.p_data, dev_name.size) == 0)
//            {
//                NRF_LOG_INFO("yes it works, lets connect");
//								do_connect = true;
//            }
//        }
//    }

//    if (do_connect)
//    {
//        // Initiate connection.
//         err_code = sd_ble_gap_connect(peer_addr,
//																			 &m_scan_params,
//                                       &m_connection_param,
//                                       APP_BLE_CONN_CFG_TAG);
//        if (err_code != NRF_SUCCESS)
//        {
//            NRF_LOG_ERROR("Connection Request Failed, reason %d\r\n", err_code);
//        }
//				// scan is automatically stopped by the connect
//            err_code = bsp_indication_set(BSP_INDICATE_IDLE);
//            APP_ERROR_CHECK(err_code);
//						
//						NRF_LOG_INFO("Connecting to target");
//    }
//		else    
//		{
//        err_code = sd_ble_gap_scan_start(NULL, &m_scan_buffer);
//        APP_ERROR_CHECK(err_code);
//    }
//}

/**@brief Function for handling the advertising report BLE event.
 *
 * @param[in] p_adv_report  Advertising report from the SoftDevice.
 */
static void on_adv_report(ble_gap_evt_adv_report_t const * p_adv_report)
{
    ret_code_t err_code;
		uint8_array_t adv_data;
    uint8_array_t dev_name;
		
		//NRF_LOG_INFO("still works");

    if (ble_advdata_uuid_find(p_adv_report->data.p_data, p_adv_report->data.len, &m_nus_uuid))
    {
      
			  adv_data.p_data = (uint8_t *)p_adv_report->data.p_data;
				adv_data.size = p_adv_report->data.len;
			
				//NRF_LOG_INFO("found service");
			
				err_code = adv_report_parse(BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME,
                                &adv_data,
                                &dev_name);
			if(err_code == NRF_SUCCESS){
				
					NRF_LOG_INFO("found name");
				if (strlen(m_target_periph_name) != 0)
        {
            if (memcmp(m_target_periph_name, dev_name.p_data, 3) == 0)
            {
                name_matches = true;
								NRF_LOG_INFO("name matches");
							
						}
						
        }
				
			}//else{
//				
//				// NRF_LOG_INFO("restart scan");
//				//scan_start();
//			  //err_code = sd_ble_gap_scan_start(NULL, &m_scan_buffer);
//        //APP_ERROR_CHECK(err_code);
//				
//				
//			}
			
    }
		
		if(name_matches){
			
			NRF_LOG_INFO("trying to connect");
			err_code = sd_ble_gap_connect(&p_adv_report->peer_addr,
                                      &m_scan_params,
                                      &m_connection_param,
                                      APP_BLE_CONN_CFG_TAG);
			
			//uint8_t * p_data = (uint8_t *)p_adv_report->data.p_data;
			rssi = p_adv_report->rssi;
			rssi = abs(rssi - 256);
			//uint8_t datas = sizeof(p_data);

			if (err_code == NRF_SUCCESS)
			{
					// scan is automatically stopped by the connect
					
					err_code = bsp_indication_set(BSP_INDICATE_IDLE);
					APP_ERROR_CHECK(err_code);
			}else{
				
				NRF_LOG_INFO("failed to connect....%d", err_code);
				
			}
		}else{
			
		  err_code = sd_ble_gap_scan_start(NULL, &m_scan_buffer);
      APP_ERROR_CHECK(err_code);
		//	NRF_LOG_INFO("scan, buf_counter is %d", buff_counter);
			//if (beacon_scan) scan_start();
			
	
			//NRF_LOG_INFO("from sensor temp is %f and hum is %f", m_stTempHumSensor.temperature_DegC, m_stTempHumSensor.humidity_Perc);
	
			
		}
  		
}



/**@brief Reads an advertising report and checks if a EarTag manufactuer specific
 *        data is present.
 *
 * @details The function is able to search for manufactuer specific data having
 *          length at least 24 bytes, and having company ID 0x0059.
 */
static uint32_t find_eartag_data(ble_gap_evt_adv_report_t const * const p_adv_report,
                                 uint8_t                        * p_buff,
                                 uint8_t                        * p_len)
{
    uint16_t  index  = 0;
    uint8_t * p_data = (uint8_t *)p_adv_report->data.len;
	


    while (index < p_adv_report->data.len)
    {
        uint8_t field_length = p_data[index];
        uint8_t field_type   = p_data[index + 1];

        if (field_type == BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA)
        {
            uint16_t company_id = p_data[index + 2] | (p_data[index + 3] << 8);

            if (company_id == COMPANY_ID && field_length >= (MANU_SPEC_DATA_MIN_LENGTH + 2))
            {
                *p_len = (*p_len > field_length-3) ? field_length-3: *p_len;
                memcpy(p_buff, &p_data[index + 4], *p_len);
                return NRF_SUCCESS;
            }
        }
        index += field_length + 1;
    }
    return NRF_ERROR_NOT_FOUND;
}



/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t            err_code;
    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_ADV_REPORT:
				{
            //NRF_LOG_INFO("adv report");
						on_adv_report(&p_gap_evt->params.adv_report);
						//on_adv_report(p_ble_evt);
						
//						ble_gap_evt_adv_report_t const * p_adv_report = &p_gap_evt->params.adv_report;
//            uint8_t buff[64];
//						memset(buff, 0, sizeof(buff));
//						
//           
//					
//						/* prepending the UART transmission with package type (1) that is required for handling data on server side */
//						//sprintf((char *)&buff[0], "1:");
//						uint8_t len = sizeof(buff);
//						
//            err_code = find_eartag_data(p_adv_report, buff, &len);
//            if (err_code == NRF_SUCCESS)
//            {
//                /* Fill non-ASCII data by 'x'. */
//                for (uint16_t i = 0; i < len; i++)
//                {
//                    if (buff[i] < 1 || buff[i] > 127)
//                    {
//                        buff[i] = 'x';
//                    }
//                }

//                /* Cascade by RSSI. */
//                //sprintf((char *)&buff[len], ":%2x\r\n", (uint8_t)p_adv_report->rssi);
//								/* Flash the LED */
//								nrf_gpio_pin_toggle(LED_4);
//								/* Calculate RSSI. */
//								signed int rssi_calc = (uint8_t)p_adv_report->rssi;
//								rssi_calc -= 256;
//							
//                snprintf((char *)&buff[len], sizeof(buff), ":%d:%s;", abs(rssi_calc), device_name);
//								NRF_LOG_INFO("rssi is %d", abs(rssi_calc));
//								/*add packet type */
//								uint8_t len2 = strlen((char *)&ubuff);
//								sprintf((char *)&ubuff[len2], "1:");
//								/*concentate strings to fill the buffer */
//								strcat((char *)ubuff, (char *)buff);
//								
//								buff_counter ++;
//                
//								if(buff_counter > BUFFSIZE){
//									/* Dump to UART. */
//									//printf((char *)ubuff);
//									NRF_LOG_INFO("uart-> %s", (char *)ubuff);
//									memset(ubuff, 0, sizeof(ubuff));
//									buff_counter = 0;
//									
//								}
//            }
					}
            break; // BLE_GAP_EVT_ADV_REPORT

        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected to target");
            err_code = ble_nus_c_handles_assign(&m_ble_nus_c, p_ble_evt->evt.gap_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);

            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);

            // start discovery of services. The NUS Client waits for a discovery result
            err_code = ble_db_discovery_start(&m_db_disc, p_ble_evt->evt.gap_evt.conn_handle);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_TIMEOUT:
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_SCAN)
            {
                NRF_LOG_INFO("Scan timed out.");
                if (beacon_scan) scan_start();
            }
            else if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                NRF_LOG_INFO("Connection Request timed out.");
            }
            break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
						NRF_LOG_INFO("SEC_PARAMS_REQUEST");
            err_code = sd_ble_gap_sec_params_reply(p_ble_evt->evt.gap_evt.conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
            // Accepting parameters requested by peer.
						NRF_LOG_INFO("CONN_PARAMS_UPDATE_REqUEST)");
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                                    &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)
    {
        NRF_LOG_INFO("ATT MTU exchange completed.");

        m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        NRF_LOG_INFO("Ble NUS max data length set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
}


/**@brief Function for initializing the GATT library. */
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_central_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in] event  Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
    ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
            break;

        case BSP_EVENT_DISCONNECT:
						NRF_LOG_INFO("BSP_EVENT_DISCONNECT");
            err_code = sd_ble_gap_disconnect(m_ble_nus_c.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        default:
            break;
    }
}

/**@brief Function for initializing the UART. */
static void uart_init(void)
{
    ret_code_t err_code;

    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_ENABLED,
        .use_parity   = false,
        .baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);

    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the NUS Client. */
static void nus_c_init(void)
{
    ret_code_t       err_code;
    ble_nus_c_init_t init;

    init.evt_handler = ble_nus_c_evt_handler;

    err_code = ble_nus_c_init(&m_ble_nus_c, &init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing buttons and leds. */
static void buttons_leds_init(void)
{
    ret_code_t err_code;
    bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LEDS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the timer. */
static void timer_init(void)
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the nrf log module. */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/** @brief Function for initializing the Database Discovery Module. */
static void db_discovery_init(void)
{
    ret_code_t err_code = ble_db_discovery_init(db_disc_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details Handle any pending log operation(s), then sleep until the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}

// Timeout handler for the repeated timer
static void timer_flash_handler(void * p_context)
{
	   UNUSED_PARAMETER(p_context);
			
		buff_counter++;
	
		if(!(buff_counter % 238)){
			 SHTC1_set_mode();
		}
		
		if(! (buff_counter % 240)){
			
				while (m_xfer_done == false);

        read_sensor_data();
			
		}
	
		if(buff_counter > 200){
			
			
			//scanning stop
			if(beacon_scan) NRF_LOG_INFO("scan stop");
			nrf_gpio_pin_toggle(LED_2);
			nrf_gpio_pin_toggle(LED_3);
			nrf_gpio_pin_toggle(LED_4);
			beacon_scan = false;
			sd_ble_gap_scan_stop();
			
			arr_counter = 0;
			memset(sensor_names, 0, sizeof(sensor_names));
			//turning amp off
			//nrf_gpio_pin_clear(CRX);
		}
		
		if(buff_counter > 2000){
			
			//scanning start
			//turning amp on
			//nrf_gpio_pin_set(CRX);
			beacon_scan = true;
			scan_start();
			NRF_LOG_INFO("scan start");
			buff_counter = 0;
	   //get sensor data and save
			
		}
	
	
	
	
}


// Timeout handler for the repeated timer
static void timer_snsr_handler(void * p_context)
{
	   UNUSED_PARAMETER(p_context);
	
		if (isConnected){
					
						
						if(isFirstPacket > 5){
							//disconnect if deleting is not in progress
							NRF_LOG_INFO("TIMER DISCONNECT");
							ret_code_t err_code = sd_ble_gap_disconnect(m_ble_nus_c.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
							APP_ERROR_CHECK(err_code);
							
							memset(sensor_name, 0, 20);
						}
					
						isFirstPacket++;
							
						
		}
		
		//start scan every certain time
		
	   //get sensor data and save
	
}


/**@brief Function for initializing the timer. */
static void timers_init(void)
{
   
	
		 // Create sensor comms timer when connected
    ret_code_t err_code = app_timer_create(&snsr_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                timer_snsr_handler);
    
    APP_ERROR_CHECK(err_code);
		NRF_LOG_INFO("app timer err code %d", err_code);
		 // Create sensor recording timer
    err_code = app_timer_create(&snsr_flash_id,
                                APP_TIMER_MODE_REPEATED,
                                timer_flash_handler);
    
    APP_ERROR_CHECK(err_code);
		NRF_LOG_INFO("app timer err code %d", err_code);
	
	
}


/**@brief Function for starting timers.
 */
static void application_timers_start(void)
{
       ret_code_t err_code;
       err_code = app_timer_start(snsr_flash_id, FLASH_DATA_INTERVAL, NULL);
       APP_ERROR_CHECK(err_code);

}


/**@brief Function for setting up test-GPIOs.
 */
void sky66114_ctrl(void)
{
    NRF_GPIOTE->CONFIG[6] = 0x00032D03;     // CTX: GPIOTE channnel #6, TASK mode, GPIO 45 (P1.13), TOGGLE, default low
    NRF_GPIOTE->CONFIG[7] = 0x00032403;     // CRX: GPIOTE channnel #7, TASK mode, GPIO 36 (P1.04), TOGGLE, default low

    NRF_PPI->CH[14].EEP = (uint32_t)&NRF_RADIO->EVENTS_TXREADY;     // PPI channel #14, EEP <= RADIO.EVENTS_TXREADY
    NRF_PPI->CH[14].TEP = (uint32_t)&NRF_GPIOTE->TASKS_SET[6];      // PPI channel #14, TEP <= GPIOTE.TASKS_SET[6]

    NRF_PPI->CH[15].EEP = (uint32_t)&NRF_RADIO->EVENTS_RXREADY;     // PPI channel #15, EEP <= RADIO.EVENTS_RXREADY
    NRF_PPI->CH[15].TEP = (uint32_t)&NRF_GPIOTE->TASKS_SET[7];      // PPI channel #15, TEP <= GPIOTE.TASKS_SET[7]

    NRF_PPI->CH[16].EEP = (uint32_t)&NRF_RADIO->EVENTS_DISABLED;    // PPI channel #16, EEP <= RADIO.EVENTS_DISABLED
    NRF_PPI->CH[16].TEP = (uint32_t)&NRF_GPIOTE->TASKS_CLR[6];      // PPI channel #16, TEP <= GPIOTE.TASKS_CLR[6]
    NRF_PPI->FORK[16].TEP = (uint32_t)&NRF_GPIOTE->TASKS_CLR[7];    // PPI channel #16: FORK.TEP <= GPIOTE.TASKS_CLR[7]

    NRF_PPI->CHENSET = 0x0001C000;                                  // Enable PPI channel #14, 15 & 16
}


int main(void)
{
    // Initialize.
    log_init();
    timer_init();
		// Initialize.
    
		timers_init();
		application_timers_start();
    uart_init();
    buttons_leds_init();
    db_discovery_init();
    power_management_init();
		/* SHTC1 setup */
		twi_init();
		read_sensor_id();
		SHTC1_set_mode();
	
    ble_stack_init();
		sky66114_ctrl();
    gatt_init();
    nus_c_init();
	
	
		

    // Start execution.
    //printf("BLE UART central example started.\r\n");
    NRF_LOG_INFO("BLE UART central example started.");
	
			 //bsp_board_led_invert(LED_4);
		nrf_gpio_pin_set(LED_4);
		nrf_gpio_pin_set(LED_3);
		nrf_gpio_pin_set(LED_2);
		//cps 1.15 must be off
		nrf_gpio_cfg_output(CPS);
		nrf_gpio_pin_clear(CPS);
		//crx 1.04 must be on
		//nrf_gpio_cfg_output(CRX);
		//nrf_gpio_pin_clear(CRX);
		//ctx 1.13 must be off
		//nrf_gpio_cfg_output(CTX);
		//nrf_gpio_pin_clear(CTX);
		
		/* Creating device name that consists of first 3 chars of Device ID */
		uint32_t deviceID = NRF_FICR->DEVICEID[0];
		char tempID[15];
		sprintf((char *)&tempID, "%x", deviceID);
		strncpy(device_name,tempID,3);
		
		/* Sending device name and Device ID to the server */
		uint8_t buff[64];
    memset(buff, 0, sizeof(buff));
    sprintf((char *)&buff, "4:%s:%04x%04x;",device_name, NRF_FICR->DEVICEID[0], NRF_FICR->DEVICEID[1]);
		//sprintf((char *)&buff, "n=%st&v=12.34&r=11.1",device_name);
		
     /* Dump to UART. */
     //printf((char *)buff);
		 NRF_LOG_INFO("uart -> %s", (char *)buff);
		 
			//APP_ERROR_CHECK(err_code);
			//NRF_LOG_INFO("error is %d", err_code);
    // Start scanning for peripherals and initiate connection
    // with devices that advertise NUS UUID.
    NRF_LOG_INFO("BLE UART central example started.");
    scan_start();
    //scan_start();

    // Enter main loop.
    for (;;)
    {
        idle_state_handle();
    }
}
