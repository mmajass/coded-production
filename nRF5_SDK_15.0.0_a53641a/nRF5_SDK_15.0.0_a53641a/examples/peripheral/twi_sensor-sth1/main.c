/**
 * Copyright (c) 2015 - 2018, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/** @file
 * @defgroup tw_sensor_example main.c
 * @{
 * @ingroup nrf_twi_example
 * @brief TWI Sensor Example main file.
 *
 * This file contains the source code for a sample application using TWI.
 *
 */

#include <stdio.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"


#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* TWI instance ID. */
#define TWI_INSTANCE_ID     0

/* Common addresses definition for temperature sensor. */
#define SHTC1_ADDR         	0x70U // (0x90U >> 1)

#define LM75B_REG_TEMP      0x00U
#define LM75B_REG_CONF      0x01U
#define LM75B_REG_THYST     0x02U
#define LM75B_REG_TOS       0x03U

#define SHTC1_ReadIdR1 			 						0xEF // command: read ID register
#define SHTC1_ReadIdR2 			 						0xC8 // command: read ID register
#define SHTC1_SoftReset1 		 						0x80 // soft reset
#define SHTC1_SoftReset2 		 						0x5D // soft reset
#define SHTC1_MeasureTempHumPolling1 	 	0x78 // meas. read T first, clock stretching disabled
#define SHTC1_MeasureTempHumPolling2 	 	0x66 // meas. read T first, clock stretching disabled
#define SHTC1_MeasureTempHumClockstr1 	0x7C // meas. read T first, clock stretching enabled
#define SHTC1_MeasureTempHumClockstr2 	0xA2 // meas. read T first, clock stretching enabled
#define SHTC1_MeasureHumTempPolling 		0x58E0 // meas. read RH first, clock stretching disabled
#define SHTC1_MeasureHumTempClockstr 		0x5C24 // meas. read RH first, clock stretching enabled

/* Mode for LM75B. */
#define NORMAL_MODE 0U

static volatile float mTemperature;
static volatile float mHumidity;

/* Indicates if operation on TWI has ended. */
static volatile bool m_xfer_done = false;

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

/* Buffer for samples read from temperature sensor. */
static uint8_t m_sample[6];

/**
 * @brief Function for setting active mode on MMA7660 accelerometer.
 */
void SHTC1_set_mode(void)
{
    ret_code_t err_code;
		m_xfer_done = false;
    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
	
    uint8_t reg[2] = {SHTC1_MeasureTempHumClockstr1, SHTC1_MeasureTempHumClockstr2};
		NRF_LOG_INFO("setting mode");
		NRF_LOG_FLUSH();
    err_code = nrf_drv_twi_tx(&m_twi, SHTC1_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		NRF_LOG_INFO("mode setup done");
		NRF_LOG_FLUSH();
    /* Writing to pointer byte. */
		/*
    reg[0] = SHTC1_MeasureTempHumClockstr1;
		reg[1] = SHTC1_MeasureTempHumClockstr2;
    m_xfer_done = false;
    err_code = nrf_drv_twi_tx(&m_twi, LM75B_ADDR, reg, 1, false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		*/
}



void LM75B_reset(void)
{
    ret_code_t err_code;
		m_xfer_done = false;
    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
	
    uint8_t reg[2] = {SHTC1_SoftReset1, SHTC1_SoftReset2};
		NRF_LOG_INFO("resetting");
		NRF_LOG_FLUSH();
    err_code = nrf_drv_twi_tx(&m_twi, SHTC1_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		NRF_LOG_INFO("reset done");
		NRF_LOG_FLUSH();
    /* Writing to pointer byte. */
		/*
    reg[0] = SHTC1_MeasureTempHumClockstr1;
		reg[1] = SHTC1_MeasureTempHumClockstr2;
    m_xfer_done = false;
    err_code = nrf_drv_twi_tx(&m_twi, LM75B_ADDR, reg, 1, false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		*/
}


/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
					NRF_LOG_INFO("event done");
					NRF_LOG_FLUSH();
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
                NRF_LOG_INFO(" RX event done with data %d - %d - %d - %d - %d - %d", m_sample[0],m_sample[1],m_sample[2],m_sample[3], m_sample[4], m_sample[5]);
								NRF_LOG_FLUSH();
								//data_handler(&m_sample);
								uint16_t val1;
								val1 = (m_sample[0] << 8) + m_sample[1];
								mTemperature = -45.0 + 175.0 * (val1 / 65535.0);
								NRF_LOG_INFO("Temperature: %.2f Celsius degrees.", mTemperature);
								uint16_t val2;
								val2 = (m_sample[3] << 8) + m_sample[4];
								mHumidity = 100.0 * (val2 / 65535.0);
								NRF_LOG_INFO("Humidity: %.2f percent.", mHumidity);
								memset(m_sample, 0, sizeof(m_sample));
							
								//LM75B_reset();
								//LM75B_set_mode();
            }
            m_xfer_done = true;
            break;
        default:
            break;
    }
}

/**
 * @brief UART initialization.
 */
void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_shtc1_config = {
       .scl                = 4,
       .sda                = 26,
       .frequency          = NRF_DRV_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = true
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_shtc1_config, twi_handler, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}


/*

if (shtc1_send_command(shtc1_cmd_read_id_reg, SHTC1_CMD_LENGTH) == false)
	{
		return false;
	}
	
	twi_master_transfer(0x70 << 1 | TWI_READ_BIT, id, 3, TWI_ISSUE_STOP);


*/
/**
 * @brief Function for reading data from temperature sensor.
 */
static void read_sensor_id()
{
    m_xfer_done = false;
		/* Write to the SHTC1 address */
		uint8_t reg[2] = {SHTC1_ReadIdR1, SHTC1_ReadIdR2};
    ret_code_t err_code = nrf_drv_twi_tx(&m_twi, SHTC1_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		
		m_xfer_done = false;
    /* Read from the SHTC1 address */
    err_code = nrf_drv_twi_rx(&m_twi, SHTC1_ADDR, m_sample, sizeof(m_sample));
    APP_ERROR_CHECK(err_code);
		while (m_xfer_done == false);
		
}



/**
 * @brief Function for reading data from temperature sensor.
 */
static void read_sensor_data()
{
    m_xfer_done = false;

    /* Read data from SHTC1 address */
    ret_code_t err_code = nrf_drv_twi_rx(&m_twi, SHTC1_ADDR, m_sample, sizeof(m_sample));
    APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for main application entry.
 */
int main(void)
{
    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_DEFAULT_BACKENDS_INIT();

    NRF_LOG_INFO("\r\nTWI sensor example started.");
    NRF_LOG_FLUSH();
		NRF_LOG_INFO("still starting");
		NRF_LOG_FLUSH();
	
    twi_init();
		NRF_LOG_INFO("twi init done");
		NRF_LOG_FLUSH();
	
		read_sensor_id();
		NRF_LOG_INFO("read id done");
		NRF_LOG_FLUSH();
	
    SHTC1_set_mode();
		NRF_LOG_INFO("set mode done");
		NRF_LOG_FLUSH();
	
		bool isready;
		

    while (true)
    {
        nrf_delay_ms(2000);
				//LM75B_reset();
				if (isready){
					SHTC1_set_mode();
					isready = false;
				}else{
					
					isready = true;
				}
				
        do
        {
            __WFE();
        }while (m_xfer_done == false);

        read_sensor_data();
        NRF_LOG_FLUSH();
    }
}

/** @} */
