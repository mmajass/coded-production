/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
#include "sdk_common.h"
#include "fds.h"
#include "eartag_file.h"
#include "sdk_errors.h"
#include "nrf_log.h"


#define MODULE_INITIALIZED          (p_eartag_file->state != EARTAG_FILE_STATE_INIT)    /**< Macro designating whether the module has been initialized properly. */
#define DIRTY                       (0xD1)                                              /**< Dirty flag. */


/**@brief Function for notifying event to application. */
static void notify_event(eartag_file_t * const p_eartag_file, eartag_file_evt_t file_evt)
{
    if (p_eartag_file->evt_handler != NULL)
    {
        p_eartag_file->evt_handler(file_evt);
    }
}


/**@brief Function for notifying error to application. */
static void handle_error(eartag_file_t * const p_eartag_file, uint32_t err_code)
{
    if (err_code == NRF_SUCCESS)
    {
        return;
    }

    if (p_eartag_file->error_handler != NULL)
    {
        p_eartag_file->error_handler(err_code);
    }
}


static void mark_dirty(eartag_file_t * const p_eartag_file,
                           uint16_t          entry_index)
{
    uint16_t record_index = entry_index / EARTAG_FILE_ENTRIES_PER_REC;

    p_eartag_file->fds.dirty[record_index] = DIRTY;
    if (record_index >= p_eartag_file->fds.num_records)
    {
        p_eartag_file->fds.num_records = record_index + 1;
    }
}

static uint32_t find_entry(eartag_file_t           * const p_eartag_file,
                           eartag_udid_t const     * const p_udid,
                           uint16_t                * p_entry_index)
{
    for (uint16_t i = 0; i < p_eartag_file->db.num_entries; i++)
    {
        if (0 == memcmp(p_udid, &p_eartag_file->db.data[i].udid, sizeof(eartag_udid_t)))
        {
            *p_entry_index = i;
            return NRF_SUCCESS;
        }
    }

    return NRF_ERROR_NOT_FOUND;
}


static uint32_t add_entry(eartag_file_t       * const p_eartag_file,
                          eartag_udid_t const * const p_udid)
{
    if (p_eartag_file->db.num_entries == EARTAG_FILE_MAX_NUM_OF_ENTRIES)
    {
        return NRF_ERROR_NO_MEM;
    }

    memcpy(&p_eartag_file->db.data[p_eartag_file->db.num_entries].udid,
           p_udid, sizeof(eartag_udid_t));

    p_eartag_file->db.data[p_eartag_file->db.num_entries].count = 1;

    mark_dirty(p_eartag_file, p_eartag_file->db.num_entries);
    p_eartag_file->db.num_entries++;

    return NRF_SUCCESS;
}


/**@brief DB write process. */
static uint32_t db_write_process(eartag_file_t * const p_eartag_file)
{
    uint32_t err_code;
    uint16_t i;
    bool     need_to_write = false;
    fds_find_token_t tok;

    i = p_eartag_file->fds.last_key - EARTAG_FILE_DB_KEY_START;

    for (; i < p_eartag_file->fds.num_records; i++)
    {
        uint16_t entries_left     = p_eartag_file->db.num_entries - p_eartag_file->fds.entries_written;
        uint16_t entries_to_write = MIN(entries_left, EARTAG_FILE_ENTRIES_PER_REC);

        if (entries_to_write == 0)
        {
            break;
        }

        /* If dirty, write. */
        if (p_eartag_file->fds.dirty[i] == DIRTY)
        {
            p_eartag_file->fds.entries_to_write = entries_to_write;
            p_eartag_file->fds.last_key         = EARTAG_FILE_DB_KEY_START + i;
            need_to_write = true;
            break;
        }

        p_eartag_file->fds.entries_written += entries_to_write;
        if (p_eartag_file->fds.entries_written >= p_eartag_file->db.num_entries)
        {
            break;
        }
    }

    if (!need_to_write)
    {
        p_eartag_file->state = EARTAG_FILE_STATE_IDLE;
        p_eartag_file->fds.last_id  = 0;
        p_eartag_file->fds.last_key = 0;
        notify_event(p_eartag_file, EARTAG_FILE_EVT_WRITE);
        return NRF_SUCCESS;
    }
                     
    memset(&p_eartag_file->fds.rec_desc, 0, sizeof(fds_record_desc_t));
    memset(&tok, 0, sizeof(fds_find_token_t));

    p_eartag_file->fds.rec_write.file_id           = EARTAG_FILE_DB_ID;
    p_eartag_file->fds.rec_write.key               = p_eartag_file->fds.last_key;
    p_eartag_file->fds.rec_write.data.p_data       = &p_eartag_file->db.data[p_eartag_file->fds.entries_written];
    p_eartag_file->fds.rec_write.data.length_words = p_eartag_file->fds.entries_to_write
                                                     * (sizeof(eartag_file_db_entry_t) / sizeof(uint32_t));

    err_code = fds_record_find(EARTAG_FILE_DB_ID, p_eartag_file->fds.last_key,
                               &p_eartag_file->fds.rec_desc, &tok);
    if (err_code == FDS_ERR_NOT_FOUND)
    {
        p_eartag_file->fds.write_fn = fds_record_write;
    }
    else if (err_code == FDS_SUCCESS)
    {
        p_eartag_file->fds.write_fn = fds_record_update;
    }
    else
    {
        VERIFY_SUCCESS(err_code);
    }

    err_code = p_eartag_file->fds.write_fn(&p_eartag_file->fds.rec_desc, &p_eartag_file->fds.rec_write);
    if (err_code == FDS_ERR_NO_SPACE_IN_FLASH)
    {
        err_code = fds_gc();
    }
    VERIFY_SUCCESS(err_code);

    p_eartag_file->state = EARTAG_FILE_STATE_WRITE;
    return NRF_SUCCESS;
}


/**@brief Touch a configuration file in FDS. */
static uint32_t touch_config(eartag_file_t * const p_eartag_file)
{
    uint32_t err_code;

    p_eartag_file->cfg.data.mode = EARTAG_FILE_MODE_HERITAGE;

    p_eartag_file->fds.rec_write.file_id           = EARTAG_FILE_CFG_ID;
    p_eartag_file->fds.rec_write.key               = EARTAG_FILE_CFG_KEY;
    p_eartag_file->fds.rec_write.data.p_data       = &p_eartag_file->cfg.data;
    p_eartag_file->fds.rec_write.data.length_words = sizeof(eartag_file_cfg_t) / sizeof(uint32_t);
    p_eartag_file->fds.write_fn                    = fds_record_write;

    err_code = p_eartag_file->fds.write_fn(&p_eartag_file->fds.rec_desc, &p_eartag_file->fds.rec_write);
    if (err_code == FDS_ERR_NO_SPACE_IN_FLASH)
    {
        err_code = fds_gc();
    }
    VERIFY_SUCCESS(err_code);
    return NRF_SUCCESS;
}


uint32_t eartag_file_init(eartag_file_t            * const p_eartag_file,
                          eartag_file_init_t const * const p_init)
{
    uint32_t           err_code;
    uint16_t           i;
    fds_find_token_t   tok;
    fds_flash_record_t rec_read;

    VERIFY_PARAM_NOT_NULL(p_eartag_file);
    VERIFY_PARAM_NOT_NULL(p_init);

    memset(p_eartag_file, 0, sizeof(eartag_file_t));
    p_eartag_file->state                = EARTAG_FILE_STATE_INIT;
    p_eartag_file->evt_handler          = p_init->evt_handler;
    p_eartag_file->error_handler        = p_init->error_handler;

    /* Try to load DB from file. */
    for (i = 0; i < EARTAG_FILE_MAX_NUM_OF_REC; i++)
    {
        uint16_t           num_entries, total_size;

        memset(&p_eartag_file->fds.rec_desc, 0, sizeof(fds_record_desc_t));
        memset(&tok, 0, sizeof(fds_find_token_t));

        /* Try to find. */
        err_code = fds_record_find(EARTAG_FILE_DB_ID, EARTAG_FILE_DB_KEY_START + i,
                                   &p_eartag_file->fds.rec_desc, &tok);
        if (err_code == FDS_ERR_NOT_FOUND)
        {
            break;
        }
        else if (err_code != FDS_SUCCESS)
        {
            VERIFY_SUCCESS(err_code);
        }
            
        /* found, and then try to read. */
        memset(&rec_read, 0, sizeof(fds_flash_record_t));

        err_code = fds_record_open(&p_eartag_file->fds.rec_desc, &rec_read);
        if (err_code == FDS_ERR_NOT_FOUND)
        {
            break;
        }
        else if (err_code != FDS_SUCCESS)
        {
            VERIFY_SUCCESS(err_code);
        }

        total_size  = rec_read.p_header->length_words * sizeof(uint32_t);
        num_entries = total_size / sizeof(eartag_file_db_entry_t);
        if (num_entries * sizeof(eartag_file_db_entry_t) != total_size)
        {
            return NRF_ERROR_DATA_SIZE;
        }

        memcpy(&p_eartag_file->db.data[p_eartag_file->db.num_entries],
               rec_read.p_data,
               total_size);

        p_eartag_file->db.num_entries += num_entries;
        p_eartag_file->fds.num_records++;
    }
        
    /* Try to load config from file. */
    memset(&p_eartag_file->fds.rec_desc, 0, sizeof(fds_record_desc_t));
    memset(&tok, 0, sizeof(fds_find_token_t));

    err_code = fds_record_find(EARTAG_FILE_CFG_ID, EARTAG_FILE_CFG_KEY,
                               &p_eartag_file->fds.rec_desc, &tok);
    if (err_code == FDS_ERR_NOT_FOUND)
    {
        return touch_config(p_eartag_file);
    }
    else if (err_code != FDS_SUCCESS)
    {
        VERIFY_SUCCESS(err_code);
    }

    memset(&rec_read, 0, sizeof(fds_flash_record_t));

    err_code = fds_record_open(&p_eartag_file->fds.rec_desc, &rec_read);
    if (err_code == FDS_ERR_NOT_FOUND)
    {
        return touch_config(p_eartag_file);
    }
    else if (err_code != FDS_SUCCESS)
    {
        VERIFY_SUCCESS(err_code);
    }

    if (rec_read.p_header->length_words * sizeof(uint32_t) != sizeof(eartag_file_cfg_t))
    {
        return NRF_ERROR_DATA_SIZE;
    }

    memcpy(&p_eartag_file->cfg.data, rec_read.p_data, sizeof(eartag_file_cfg_t));

    p_eartag_file->state = EARTAG_FILE_STATE_IDLE;
    notify_event(p_eartag_file, EARTAG_FILE_EVT_INIT);
    return NRF_SUCCESS;
}


/**@brief FDS event handler. */
void eartag_file_on_fds_evt(eartag_file_t   * const p_eartag_file,
                            fds_evt_t const * const p_evt)
{
    uint32_t err_code;

    VERIFY_PARAM_NOT_NULL_VOID(p_eartag_file);
    VERIFY_PARAM_NOT_NULL_VOID(p_evt);

    switch (p_eartag_file->state)
    {
        case EARTAG_FILE_STATE_INIT:
            if (p_evt->id == FDS_EVT_WRITE
                && p_evt->write.file_id == EARTAG_FILE_CFG_ID
                && p_evt->write.record_key == EARTAG_FILE_CFG_KEY
                && p_evt->result == FDS_SUCCESS)
            {
                p_eartag_file->state = EARTAG_FILE_STATE_IDLE;
                notify_event(p_eartag_file, EARTAG_FILE_EVT_INIT);
            }
            break;

        case EARTAG_FILE_STATE_WRITE:
            if (p_evt->id == FDS_EVT_GC)
            {
                /* Retry */
                err_code = p_eartag_file->fds.write_fn(&p_eartag_file->fds.rec_desc, &p_eartag_file->fds.rec_write);
                handle_error(p_eartag_file, err_code);
            }
            else if ((p_evt->id == FDS_EVT_WRITE || p_evt->id == FDS_EVT_UPDATE)
                && p_evt->write.file_id == p_eartag_file->fds.last_id
                && p_evt->write.record_key == p_eartag_file->fds.last_key
                && p_evt->result == FDS_SUCCESS)
            {
                if (p_eartag_file->fds.last_id == EARTAG_FILE_DB_ID)
                {
                    /* Commit. */
                    p_eartag_file->fds.entries_written += p_eartag_file->fds.entries_to_write;
                    p_eartag_file->fds.entries_to_write = 0;

                    /* Next FDS record. */
                    p_eartag_file->fds.last_key++;

                    err_code = db_write_process(p_eartag_file);
                    handle_error(p_eartag_file, err_code);
                }
                else if (p_eartag_file->fds.last_id == EARTAG_FILE_CFG_ID)
                {
                    p_eartag_file->state        = EARTAG_FILE_STATE_IDLE;
                    p_eartag_file->fds.last_id  = 0;
                    p_eartag_file->fds.last_key = 0;
                    notify_event(p_eartag_file, EARTAG_FILE_EVT_WRITE);
                }

            }
            break;

        case EARTAG_FILE_STATE_DELETE:
            if (p_evt->id == FDS_EVT_DEL_FILE
                && p_evt->write.file_id == p_eartag_file->fds.last_id
                && p_evt->result == FDS_SUCCESS)
            {
                p_eartag_file->state = EARTAG_FILE_STATE_IDLE;

                /* clear anything else. */
                p_eartag_file->fds.entries_to_write = 0;
                p_eartag_file->fds.entries_written  = 0;
                p_eartag_file->fds.last_id          = 0;
                p_eartag_file->fds.last_key         = 0;
                p_eartag_file->fds.num_records      = 0;
                memset(p_eartag_file->fds.dirty, 0, EARTAG_FILE_MAX_NUM_OF_REC);

                p_eartag_file->db.num_entries = 0;
                memset(p_eartag_file->db.data, 0, EARTAG_FILE_MAX_NUM_OF_ENTRIES * sizeof(eartag_file_db_entry_t));

                notify_event(p_eartag_file, EARTAG_FILE_EVT_DELETE);
            }
            break;

        default:
            break;
    }
}


uint32_t eartag_file_db_put(eartag_file_t       * const p_eartag_file,
                            eartag_udid_t const * const p_udid,
                            uint16_t              num)
{
    uint32_t err_code;
    uint16_t i;

    VERIFY_PARAM_NOT_NULL(p_eartag_file);
    VERIFY_PARAM_NOT_NULL(p_udid);
    VERIFY_MODULE_INITIALIZED();

    if (p_eartag_file->state != EARTAG_FILE_STATE_IDLE)
    {
        return NRF_ERROR_INVALID_STATE;
    }

    for (i = 0; i < num; i++)
    {
        uint16_t entry_index;

        /* Check if UDID exists. */
        err_code = find_entry(p_eartag_file, &p_udid[i], &entry_index);
        if (err_code == NRF_SUCCESS)
        {
            /* If it exists, cnt++. */
            p_eartag_file->db.data[entry_index].count++;
            mark_dirty(p_eartag_file, entry_index);
        }
        else
        {
            /* Else, insert new one. */
            /* @note ignored NRF_ERROR_NO_MEM. */
            (void) add_entry(p_eartag_file, &p_udid[i]);
        }
    }

    return NRF_SUCCESS;
}


uint32_t eartag_file_db_write(eartag_file_t * const p_eartag_file)
{
    VERIFY_PARAM_NOT_NULL(p_eartag_file);
    VERIFY_MODULE_INITIALIZED();

    if (p_eartag_file->state != EARTAG_FILE_STATE_IDLE)
    {
        return NRF_ERROR_INVALID_STATE;
    }

    p_eartag_file->fds.entries_written = 0;
    p_eartag_file->fds.last_id         = EARTAG_FILE_DB_ID;
    p_eartag_file->fds.last_key        = EARTAG_FILE_DB_KEY_START;

    return db_write_process(p_eartag_file);
}


uint32_t eartag_file_db_clear(eartag_file_t * const p_eartag_file)
{
    uint32_t err_code;

    VERIFY_PARAM_NOT_NULL(p_eartag_file);
    VERIFY_MODULE_INITIALIZED();

    if (p_eartag_file->state != EARTAG_FILE_STATE_IDLE)
    {
        return NRF_ERROR_INVALID_STATE;
    }

    p_eartag_file->fds.last_id = EARTAG_FILE_DB_ID;
    err_code = fds_file_delete(EARTAG_FILE_DB_ID);
    VERIFY_SUCCESS(err_code);

    p_eartag_file->state = EARTAG_FILE_STATE_DELETE;
    return NRF_SUCCESS;
}


uint32_t eartag_file_config_write(eartag_file_t * const p_eartag_file)
{
    uint32_t err_code;
    fds_find_token_t tok;

    VERIFY_PARAM_NOT_NULL(p_eartag_file);
    VERIFY_MODULE_INITIALIZED();

    if (p_eartag_file->state != EARTAG_FILE_STATE_IDLE)
    {
        return NRF_ERROR_INVALID_STATE;
    }

    p_eartag_file->fds.last_id  = EARTAG_FILE_CFG_ID;
    p_eartag_file->fds.last_key = EARTAG_FILE_CFG_KEY;

    memset(&tok, 0, sizeof(fds_find_token_t));
    err_code = fds_record_find(EARTAG_FILE_CFG_ID, EARTAG_FILE_CFG_KEY,
                               &p_eartag_file->fds.rec_desc, &tok);
    VERIFY_SUCCESS(err_code);

    p_eartag_file->fds.rec_write.file_id           = EARTAG_FILE_CFG_ID;
    p_eartag_file->fds.rec_write.key               = EARTAG_FILE_CFG_KEY;
    p_eartag_file->fds.rec_write.data.p_data       = &p_eartag_file->cfg.data;
    p_eartag_file->fds.rec_write.data.length_words = sizeof(eartag_file_cfg_t) / sizeof(uint32_t);
    p_eartag_file->fds.write_fn                    = fds_record_update;

    err_code = p_eartag_file->fds.write_fn(&p_eartag_file->fds.rec_desc, &p_eartag_file->fds.rec_write);
    if (err_code == FDS_ERR_NO_SPACE_IN_FLASH)
    {
        err_code = fds_gc();
    }
    VERIFY_SUCCESS(err_code);

    p_eartag_file->state = EARTAG_FILE_STATE_WRITE;
    return NRF_SUCCESS;
}


uint32_t eartag_file_get_config(eartag_file_t      * const p_eartag_file,
                                eartag_file_cfg_t ** pp_cfg)
{
    VERIFY_PARAM_NOT_NULL(p_eartag_file);
    VERIFY_PARAM_NOT_NULL(pp_cfg);
    VERIFY_MODULE_INITIALIZED();

    *pp_cfg = &p_eartag_file->cfg.data;

    return NRF_SUCCESS;
}


uint32_t eartag_file_get_db(eartag_file_t           * const p_eartag_file,
                            eartag_file_db_entry_t ** pp_udid,
                            uint16_t                * p_num)
{
    VERIFY_PARAM_NOT_NULL(p_eartag_file);
    VERIFY_PARAM_NOT_NULL(pp_udid);
    VERIFY_PARAM_NOT_NULL(p_num);
    VERIFY_MODULE_INITIALIZED();

    *pp_udid = p_eartag_file->db.data;
    *p_num   = p_eartag_file->db.num_entries;

    return NRF_SUCCESS;
}


