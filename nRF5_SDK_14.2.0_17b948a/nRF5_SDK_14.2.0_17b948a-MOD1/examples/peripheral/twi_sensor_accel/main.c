/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/** @file
 * @defgroup tw_sensor_example main.c
 * @{
 * @ingroup nrf_twi_example
 * @brief TWI Sensor Example main file.
 *
 * This file contains the source code for a sample application using TWI.
 *
 */

#include <stdio.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
//#include "lis2dh12.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* TWI instance ID. */
#define TWI_INSTANCE_ID     0
//TWI device detected at address 0x19.
/* Common addresses definition for temperature sensor. */
#define ACCEL_ADDR          0x19U

#define LM75B_REG_TEMP      0x00U
#define LM75B_REG_CONF      0x01U
#define LM75B_REG_THYST     0x02U
#define LM75B_REG_TOS       0x03U

#define PIN_IN							22

/* Mode for LM75B. */
#define NORMAL_MODE 0U

/* Indicates if operation on TWI has ended. */
static volatile bool m_xfer_done = false;

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

/* Buffer for samples read from temperature sensor. */
static uint8_t m_sample[7];

/**
 * @brief Function for setting active mode on MMA7660 accelerometer.
 */
 /**
 * @brief Function for setting active mode on MMA7660 accelerometer.
 */
void ACCEL_set_mode(void)
{
    ret_code_t err_code;
		m_xfer_done = false;
    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
	
    uint8_t reg[2] = {0x20, 0x17}; //10Hz 
		
		
		NRF_LOG_INFO("setting mode");
		NRF_LOG_FLUSH();
    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		NRF_LOG_INFO("mode setup done");
		NRF_LOG_FLUSH();
		
		m_xfer_done = false;
    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
	
    uint8_t reg0[2] = {0x22, 0x10}; //interrupt on 1 if data ready
		
		
		NRF_LOG_INFO("setting mode");
		NRF_LOG_FLUSH();
    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg0, sizeof(reg0), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		NRF_LOG_INFO("mode setup done");
		NRF_LOG_FLUSH();
		
		
		m_xfer_done = false;
    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
	
    uint8_t reg2[2] = {0x23, 0x00}; //default
	
		
		NRF_LOG_INFO("setting mode");
		NRF_LOG_FLUSH();
    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg2, sizeof(reg2), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		NRF_LOG_INFO("mode setup done");
		NRF_LOG_FLUSH();
   
		
		m_xfer_done = false;
    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
	
    uint8_t reg3[2] = {0x24, 0x00}; //no fifo
	
		
		NRF_LOG_INFO("setting mode");
		NRF_LOG_FLUSH();
    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg3, sizeof(reg3), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		NRF_LOG_INFO("mode setup done");
		NRF_LOG_FLUSH();
    /* Writing to pointer byte. */
	
		m_xfer_done = false;
    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
	
    uint8_t reg4[2] = {0x2E, 0x00}; // no fifo
		
		
		NRF_LOG_INFO("setting mode");
		NRF_LOG_FLUSH();
    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg4, sizeof(reg4), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		NRF_LOG_INFO("mode setup done");
		NRF_LOG_FLUSH();
    /* Writing to pointer byte. */
		
		
		
		m_xfer_done = false;
    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
	
    uint8_t reg5[2] = {0x1E, 0x90}; //disable pullup
		
		
		NRF_LOG_INFO("setting mode");
		NRF_LOG_FLUSH();
    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg5, sizeof(reg5), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		NRF_LOG_INFO("mode setup done");
		NRF_LOG_FLUSH();
    /* Writing to pointer byte. */
		
}

 
//void LM75B_set_mode(void)
//{
//    ret_code_t err_code;

//    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
//    uint8_t reg[2] = {LM75B_REG_CONF, NORMAL_MODE};
//    err_code = nrf_drv_twi_tx(&m_twi, LM75B_ADDR, reg, sizeof(reg), false);
//    APP_ERROR_CHECK(err_code);
//    while (m_xfer_done == false);

//    /* Writing to pointer byte. */
//    reg[0] = LM75B_REG_TEMP;
//    m_xfer_done = false;
//    err_code = nrf_drv_twi_tx(&m_twi, LM75B_ADDR, reg, 1, false);
//    APP_ERROR_CHECK(err_code);
//    while (m_xfer_done == false);
//}

/**
 * @brief Function for handling data from temperature sensor.
 *
 * @param[in] temp          Temperature in Celsius degrees read from sensor.
 */
__STATIC_INLINE void data_handler(uint8_t temp)
{
    NRF_LOG_INFO("Temperature: %d Celsius degrees.", temp);
}

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
                //data_handler(m_sample);
							int x = (uint16_t) (m_sample[0] | (((uint16_t)m_sample[1]) << 8));
							int y = (uint16_t) (m_sample[2] | (((uint16_t)m_sample[3]) << 8));
							int z = (uint16_t) (m_sample[4] | (((uint16_t)m_sample[5]) << 8));
							
							if(x > 32767)
							{
								x -= 65536;
								
							}
							if(y > 32767)
							{
								y -= 65536;
							}
							if(z > 32767)
							{
								z -= 65536;
							}
									
								NRF_LOG_INFO(" RX event done %d, %d, %d with data",x, y, z);
							//NRF_LOG_INFO("something is %d",m_sample[counter]);
							
							
							
								NRF_LOG_FLUSH();
								
								memset(m_sample, 0, sizeof(m_sample));
            }
            m_xfer_done = true;
            break;
        default:
            break;
    }
}

/**
 * @brief UART initialization.
 */
void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_lm75b_config = {
       .scl                = 32,
       .sda                = 24,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_lm75b_config, twi_handler, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}


/**
 * @brief Function for reading data from temperature sensor.
 */
static void read_sensor_data()
{
    m_xfer_done = false;

    /* Read data from SHTC1 address */
    ret_code_t err_code = nrf_drv_twi_rx(&m_twi, ACCEL_ADDR, m_sample, sizeof(m_sample));
    APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for reading data from temperature sensor.
 */
static void start_sensor()
{
    m_xfer_done = false;
		/* Write to the SHTC1 address */
		//uint8_t reg[2] = {SHTC1_ReadIdR1, SHTC1_ReadIdR2};
		uint8_t reg[1] = {(0x80 | 0x28)};
		//uint8_t reg[1] = {0xA8};
		//uint8_t reg[6] = {0x28,0x29,0x2A,0x2B,0x2D,0x2C};
    ret_code_t err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg, sizeof(reg), true);
    APP_ERROR_CHECK(err_code);
    //while (m_xfer_done == false);
		
		//m_xfer_done = false;
    /* Read from the SHTC1 address */
    //err_code = nrf_drv_twi_rx(&m_twi, SHTC1_ADDR, m_sample, sizeof(m_sample));
    //APP_ERROR_CHECK(err_code);
		//while (m_xfer_done == false);
		
}


void in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
   NRF_LOG_INFO("data ready");
	 while (m_xfer_done == false);
	 read_sensor_data();
	 NRF_LOG_FLUSH();
}
/**
 * @brief Function for configuring: PIN_IN pin for input, PIN_OUT pin for output,
 * and configures GPIOTE to give an interrupt on pin change.
 */
static void gpio_init(void)
{
    ret_code_t err_code;

    err_code = nrf_drv_gpiote_init();
    APP_ERROR_CHECK(err_code);

    //nrf_drv_gpiote_out_config_t out_config = GPIOTE_CONFIG_OUT_SIMPLE(false);

    //err_code = nrf_drv_gpiote_out_init(PIN_OUT, &out_config);
    //APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_LOTOHI(true);
    in_config.pull = NRF_GPIO_PIN_PULLUP;

    err_code = nrf_drv_gpiote_in_init(PIN_IN, &in_config, in_pin_handler);
    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_in_event_enable(PIN_IN, true);
}

/**
 * @brief Function for main application entry.
 */
int main(void)
{
    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_DEFAULT_BACKENDS_INIT();
		//int 1 interrupt p0.22
	//int 2 interrupt p0.20
    NRF_LOG_INFO("\r\nTWI sensor example");
    NRF_LOG_FLUSH();
		gpio_init();
    twi_init();
    ACCEL_set_mode();
		start_sensor();
		while (m_xfer_done == false);
	 read_sensor_data();
		
    while (true)
    {
       // nrf_delay_ms(500);

        do
        {
            __WFE();
        }while (m_xfer_done == false);

       // read_sensor_data();
        NRF_LOG_FLUSH();
    }
}

/** @} */
