/**
 * Copyright (c) 2016 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "nordic_common.h"
#include "app_error.h"
#include "app_uart.h"
#include "ble_db_discovery.h"
#include "app_timer.h"
#include "app_util.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "ble_gap.h"
#include "ble_hci.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "nrf_pwr_mgmt.h"
#include "ble_advdata.h"
#include "ble_nus_c.h"
#include "nrf_ble_gatt.h"

#include "nrf_delay.h"

#include "nrf_drv_rtc.h"
#include "nrf_drv_twi.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_spi.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


#define APP_BLE_CONN_CFG_TAG    1                                       /**< A tag that refers to the BLE stack configuration we set with @ref sd_ble_cfg_set. Default tag is @ref BLE_CONN_CFG_TAG_DEFAULT. */
#define APP_BLE_OBSERVER_PRIO   3                                       /**< Application's BLE observer priority. You shoulnd't need to modify this value. */

#define UART_TX_BUF_SIZE        256                                     /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE        256                                     /**< UART RX buffer size. */

#define NUS_SERVICE_UUID_TYPE   BLE_UUID_TYPE_VENDOR_BEGIN              /**< UUID type for the Nordic UART Service (vendor specific). */

#define SCAN_INTERVAL           0x0050                                  /**< Determines scan interval in units of 0.625 millisecond. */
#define SCAN_WINDOW             0x0050                                  /**< Determines scan window in units of 0.625 millisecond. */
#define SCAN_TIMEOUT            0x0000                                  /**< Timout when scanning. 0x0000 disables timeout. */

#define MIN_CONNECTION_INTERVAL MSEC_TO_UNITS(20, UNIT_1_25_MS)         /**< Determines minimum connection interval in millisecond. */
#define MAX_CONNECTION_INTERVAL MSEC_TO_UNITS(75, UNIT_1_25_MS)         /**< Determines maximum connection interval in millisecond. */
#define SLAVE_LATENCY           0                                       /**< Determines slave latency in counts of connection events. */
#define SUPERVISION_TIMEOUT     MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Determines supervision time-out in units of 10 millisecond. */

#define UUID16_SIZE             2                                       /**< Size of 16 bit UUID */
#define UUID32_SIZE             4                                       /**< Size of 32 bit UUID */
#define UUID128_SIZE            16                                      /**< Size of 128 bit UUID */

#define ECHOBACK_BLE_UART_DATA  0                                       /**< Echo the UART data that is received over the Nordic UART Service back to the sender. */

#define COMPANY_ID                  0x0059                              /**< Nordic Semiconductor. */
#define MANU_SPEC_DATA_MIN_LENGTH   24                                  /**< Minimum length of manufacturer specific data. */


BLE_NUS_C_DEF(m_ble_nus_c);                                             /**< BLE NUS service client instance. */
NRF_BLE_GATT_DEF(m_gatt);                                               /**< GATT module instance. */
BLE_DB_DISCOVERY_DEF(m_db_disc);                                        /**< DB discovery module instance. */

static uint16_t m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - OPCODE_LENGTH - HANDLE_LENGTH; /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */


#define COMPARE_COUNTERTIME  (30UL)                                        /**< Get Compare event COMPARE_TIME seconds after the counter starts from 0. */

int uptime_counter;

char device_name[4];
char rssi[6];
static uint8_t xrssi[10]; 
static bool startup = true;
int buff_counter;
uint8_t ubuff[125];

/* Number of packets added into buffer before sending */

#define	BUFFSIZE	1

/* The board uses SKYWORKS SKY66114-11 amplifier, here are the amplifier controls */

#define CPS          NRF_GPIO_PIN_MAP(1,15)
#define CRX          NRF_GPIO_PIN_MAP(1,04)
#define CTX          NRF_GPIO_PIN_MAP(1,13)


/* TWI instance ID. */
#define TWI_INSTANCE_ID     0

/* Common addresses definition for temperature sensor. */
#define SHTC1_ADDR         	0x70U // (0x90U >> 1)

#define SHTC1_ReadIdR1 			 						0xEF // command: read ID register
#define SHTC1_ReadIdR2 			 						0xC8 // command: read ID register
#define SHTC1_SoftReset1 		 						0x80 // soft reset
#define SHTC1_SoftReset2 		 						0x5D // soft reset
#define SHTC1_MeasureTempHumPolling1 	 	0x78 // meas. read T first, clock stretching disabled
#define SHTC1_MeasureTempHumPolling2 	 	0x66 // meas. read T first, clock stretching disabled
#define SHTC1_MeasureTempHumClockstr1 	0x7C // meas. read T first, clock stretching enabled
#define SHTC1_MeasureTempHumClockstr2 	0xA2 // meas. read T first, clock stretching enabled
#define SHTC1_MeasureHumTempPolling 		0x58E0 // meas. read RH first, clock stretching disabled
#define SHTC1_MeasureHumTempClockstr 		0x5C24 // meas. read RH first, clock stretching enabled


static volatile float mTemperature;
static volatile float mHumidity;

/* Indicates if operation on TWI has ended. */
static volatile bool m_xfer_done = false;

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

/* Buffer for samples read from temperature sensor. */
static uint8_t m_sample[6];

static void ble_nus_chars_received_uart_print(uint8_t * p_data, uint16_t data_len);

/*SPI sestup for shift register */

#define SPI_INSTANCE  1 /**< SPI instance index. */
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */

#define LEDSOFF 0x60
#define	LEDSON	0xFF
#define LED0ON 	0x64	//	0010 0000	10	QC2
#define LED1ON	0x61	// "8"	//	0000 1000	8		QD3
#define LED2ON "00000100"	//	0000 0100	4		QE4
/*XBEE LED */
#define LED3ON	0x70	// "10000000" //  1000 0000	80	QA15

#define LED4 "01000000"	//	0100 0000	40	QB1
/*ON LED */
/*SOLAR LED */
/*SOLAR LED */
/*USB LED */
#define SCK 	24
#define MOSI 	22
#define SS		32



//static uint8_t       m_tx_buf[] = {0x10, (0x64 | 0x61)};           /**< TX buffer. */
static uint8_t       m_rx_buf[];    /**< RX buffer. */
static const uint8_t m_length = 2;        /**< Transfer length. */


/* ADC sampling setup */
#define ADC_REF_VOLTAGE_IN_MILLIVOLTS       600                                          /**< Reference voltage (in milli volts) used by ADC while doing conversion. */
#define ADC_PRE_SCALING_COMPENSATION        6                                            /**< The ADC is configured to use VDD with 1/3 prescaling as input. And hence the result of conversion is to be multiplied by 3 to get the actual value of the battery voltage.*/
#define DIODE_FWD_VOLT_DROP_MILLIVOLTS      270                                          /**< Typical forward voltage drop of the diode (Part no: SD103ATW-7-F) that is connected in series with the voltage supply. This is the voltage drop when the forward current is 1mA. Source: Data sheet of 'SURFACE MOUNT SCHOTTKY BARRIER DIODE ARRAY' available at www.diodes.com. */
#define ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS   1200                                         /**< Value in millivolts for voltage used as reference in ADC conversion on NRF51. */
#define ADC_INPUT_PRESCALER                 3                                            /**< Input prescaler for ADC convestion on NRF51. */
#define ADC_RES_10BIT                       1024                                         /**< Maximum digital value for 10-bit ADC conversion. */


#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE)\
        ((((ADC_VALUE) * ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_INPUT_PRESCALER)



#define SAMPLES_IN_BUFFER 1
volatile uint8_t state = 1;

static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];

static volatile int voltage;

volatile uint8_t txcount = 0;

/**
 * @brief SPI user event handler.
 * @param event
 */
void spi_event_handler(nrf_drv_spi_evt_t const * p_event,
                       void *                    p_context)
{
    spi_xfer_done = true;
    NRF_LOG_INFO("SPI Transfer completed.");
    if (m_rx_buf[0] != 0)
    {
        NRF_LOG_INFO(" Received:");
        NRF_LOG_HEXDUMP_INFO(m_rx_buf, strlen((const char *)m_rx_buf));
    }
}


/**
	* @brief SPI initialisation
	*
 */

void spi_init()
{
	
		nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.ss_pin   = SS;
    //spi_config.miso_pin = SS;
    spi_config.mosi_pin = MOSI;
    spi_config.sck_pin  = SCK;
    APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, spi_event_handler, NULL));
	
	
	
	
	
	
}

/**@brief Functions for analogue - digital conversion
 *
 * @details These functions are required to measure battery voltage
 */


void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
    
	//NRF_LOG_INFO("event\r\n");
		if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);

        int i;
        //NRF_LOG_INFO("ADC event number: %d", (int)m_adc_evt_counter);
				NRF_LOG_INFO("saadc done\r\n");
				int average_voltage = 0;

        for (i = 0; i < SAMPLES_IN_BUFFER; i++)
        {
            //NRF_LOG_INFO("%d", p_event->data.done.p_buffer[i]);
					
						//NRF_LOG_INFO("voltage is %d",ADC_RESULT_IN_MILLI_VOLTS(p_event->data.done.p_buffer[i]) +
                                 // DIODE_FWD_VOLT_DROP_MILLIVOLTS);
						average_voltage += ADC_RESULT_IN_MILLI_VOLTS(p_event->data.done.p_buffer[i]) +
                                  DIODE_FWD_VOLT_DROP_MILLIVOLTS;
					  
						//NRF_LOG_INFO("voltage %d is %f", 1500.0 / 0.333 /0.6 * 1024.0);
        }
				
				//NRF_LOG_INFO("average voltage is %d", average_voltage);
				average_voltage = average_voltage / SAMPLES_IN_BUFFER;
				//voltage = round(average_voltage * 10 / 569);
				voltage = average_voltage;//round(average_voltage /284);//1 V * (1/6) / 0.6 V * 1023 ~= 284.https://devzone.nordicsemi.com/f/nordic-q-a/15146/saadc-to-measure-battery-voltage-not-getting-proper-result/57821#57821
				//NRF_LOG_INFO("voltage final is %d", voltage);
				//err_code = eartag_adv_set_batt_lv(&m_adv, voltage);
          //  APP_ERROR_CHECK(err_code);
				//nrf_drv_saadc_uninit();
				//NRF_SAADC->INTENCLR = (SAADC_INTENCLR_END_Clear << SAADC_INTENCLR_END_Pos);
				//NVIC_ClearPendingIRQ(SAADC_IRQn);
				//NRF_LOG_FLUSH();
        //m_adc_evt_counter++;
				nrf_drv_saadc_uninit();
				//nrf_gpio_pin_set(26);
    }
}


void saadc_init(void)
{
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config =
    NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN0);//NRF_SAADC_INPUT_AIN2
		channel_config.gain = NRF_SAADC_GAIN1_6;//NRF_SAADC_GAIN1_3

    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);
		//NRF_LOG_INFO("error 11 is %d", err_code);

    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
    APP_ERROR_CHECK(err_code);
		//NRF_LOG_INFO("error 12 is %d", err_code);
    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
		//NRF_LOG_INFO("error 13 is %d", err_code);
    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
		//NRF_LOG_INFO("error 14 is %d", err_code);
}

/**
 * @brief Function for setting active mode on SHTC1 temp and humidity sensor.
 */
void SHTC1_set_mode(void)
{
    ret_code_t err_code;
		m_xfer_done = false;
    /* Writing to LM75B_REG_CONF "0" set temperature sensor in NORMAL mode. */
	
    uint8_t reg[2] = {SHTC1_MeasureTempHumClockstr1, SHTC1_MeasureTempHumClockstr2};
		NRF_LOG_INFO("setting mode");
		
    err_code = nrf_drv_twi_tx(&m_twi, SHTC1_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		NRF_LOG_INFO("mode setup done");
		

}


/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
			
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
                //NRF_LOG_INFO(" RX event done with data %d - %d - %d - %d - %d - %d", m_sample[0],m_sample[1],m_sample[2],m_sample[3], m_sample[4], m_sample[5]);
								//NRF_LOG_FLUSH();
								
								uint16_t val1;
								val1 = (m_sample[0] << 8) + m_sample[1];
								mTemperature = -45.0 + 175.0 * (val1 / 65535.0);
							
								//NRF_LOG_INFO("Temperature: %.2f Celsius degrees.", mTemperature);
								uint16_t val2;
								val2 = (m_sample[3] << 8) + m_sample[4];
								mHumidity = 100.0 * (val2 / 65535.0);
							
								//NRF_LOG_INFO("Humidity: %.2f percent.", mHumidity);
								uint8_t buff1[64];
								memset(buff1, 0, sizeof(buff1));	
					
								/** Sending  temperature and humidity at time  */
							
									
								//sprintf((char *)&buff1, "n=%st&temp=%.2f&hum=%.2f",device_name, mTemperature, mHumidity);
									
									
								
									
								printf((char *)buff1);
								//sprintf((char *)&buff2, "n=%sh&v=%.2f",device_name, mHumidity);
								//NRF_LOG_INFO("uart->%s", (char *)&buff1);
								//ble_nus_chars_received_uart_print(buff1, sizeof(buff1));
								//ble_nus_chars_received_uart_print(buff2, sizeof(buff2));
								memset(m_sample, 0, sizeof(m_sample));
								
							
								//LM75B_reset();
								//LM75B_set_mode();
            }
            m_xfer_done = true;
            break;
        default:
            break;
    }
}

/**
 * @brief TWI initialization.
 */
void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_shtc1_config = {
       .scl                = 25,
       .sda                = 26,
       .frequency          = NRF_TWI_FREQ_100K,//NRF_DRV_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = true
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_shtc1_config, twi_handler, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}


/**
 * @brief Function for reading data from temperature sensor.
 */
static void read_sensor_id()
{
    m_xfer_done = false;
		/* Write to the SHTC1 address */
		uint8_t reg[2] = {SHTC1_ReadIdR1, SHTC1_ReadIdR2};
    ret_code_t err_code = nrf_drv_twi_tx(&m_twi, SHTC1_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		
		m_xfer_done = false;
    /* Read from the SHTC1 address */
    err_code = nrf_drv_twi_rx(&m_twi, SHTC1_ADDR, m_sample, sizeof(m_sample));
    APP_ERROR_CHECK(err_code);
		while (m_xfer_done == false);
		
}



/**
 * @brief Function for reading data from temperature sensor.
 */
static void read_sensor_data()
{
    m_xfer_done = false;

    /* Read data from SHTC1 address */
    ret_code_t err_code = nrf_drv_twi_rx(&m_twi, SHTC1_ADDR, m_sample, sizeof(m_sample));
    APP_ERROR_CHECK(err_code);
}



/** @brief Parameters used when scanning. */
#if (NRF_SD_BLE_API_VERSION == 6)
static ble_gap_scan_params_t m_scan_params =
{
    .active            = 0,
    .interval          = SCAN_INTERVAL,
    .window            = SCAN_WINDOW,
    .filter_policy     = BLE_GAP_SCAN_FP_ACCEPT_ALL,
    .filter_duplicates = BLE_GAP_SCAN_DUPLICATES_REPORT,
    .scan_phy          = BLE_GAP_PHY_CODED,
    .duration          = SCAN_TIMEOUT,
    .period            = 0x0000, // No period.
};
#else
static ble_gap_scan_params_t const m_scan_params =
{
    .active   = 1,
    .interval = SCAN_INTERVAL,
    .window   = SCAN_WINDOW,
    .timeout  = SCAN_TIMEOUT,
    #if (NRF_SD_BLE_API_VERSION <= 2)
        .selective   = 0,
        .p_whitelist = NULL,
    #endif
    #if (NRF_SD_BLE_API_VERSION >= 3)
        .use_whitelist = 0,
    #endif
};
#endif


/**@brief Function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num     Line number of the failing ASSERT call.
 * @param[in] p_file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(0xDEADBEEF, line_num, p_file_name);
		NVIC_SystemReset();
}


/**@brief Function to start scanning. */
static void scan_start(void)
{
    ret_code_t ret;

    ret = sd_ble_gap_scan_start(&m_scan_params);
    APP_ERROR_CHECK(ret);

    ret = bsp_indication_set(BSP_INDICATE_SCANNING);
    APP_ERROR_CHECK(ret);
}


/**@brief Function for handling database discovery events.
 *
 * @details This function is callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function should forward the events
 *          to their respective services.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */
static void db_disc_handler(ble_db_discovery_evt_t * p_evt)
{
    ble_nus_c_on_db_disc_evt(&m_ble_nus_c, p_evt);
}


/**@brief Function for handling characters received by the Nordic UART Service.
 *
 * @details This function takes a list of characters of length data_len and prints the characters out on UART.
 *          If @ref ECHOBACK_BLE_UART_DATA is set, the data is sent back to sender.
 */
static void ble_nus_chars_received_uart_print(uint8_t * p_data, uint16_t data_len)
{
    ret_code_t ret_val;

    NRF_LOG_DEBUG("Receiving data.");
    NRF_LOG_HEXDUMP_DEBUG(p_data, data_len);

    for (uint32_t i = 0; i < data_len; i++)
    {
        do
        {
            ret_val = app_uart_put(p_data[i]);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("app_uart_put failed for index 0x%04x.", i);
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }
    if (p_data[data_len-1] == '\r')
    {
        while (app_uart_put('\n') == NRF_ERROR_BUSY);
    }
    if (ECHOBACK_BLE_UART_DATA)
    {
        // Send data back to peripheral.
        do
        {
            ret_val = ble_nus_c_string_send(&m_ble_nus_c, p_data, data_len);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("Failed sending NUS message. Error 0x%x. ", ret_val);
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }
}


/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to
 *          a string. The string will be be sent over BLE when the last character received was a
 *          'new line' '\n' (hex 0x0A) or if the string has reached the maximum data length.
 */
void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint16_t index = 0;
    //uint32_t ret_val;

    switch (p_event->evt_type)
    {
        /**@snippet [Handling data from UART] */
        case APP_UART_DATA_READY:
						if (startup)
						{
							UNUSED_VARIABLE(app_uart_get(&xrssi[index]));
						}else{
							
							UNUSED_VARIABLE(app_uart_get(&data_array[index]));
						}
						//NRF_LOG_INFO("from uart %c", (char *)data_array[index]);
            index++;

            if ((data_array[index - 1] == '\n'))
            {
                //NRF_LOG_DEBUG("Received return, data ready");
                //NRF_LOG_HEXDUMP_DEBUG(data_array, index);
								
								/*
                do
                {
                    ret_val = ble_nus_c_string_send(&m_ble_nus_c, data_array, index);
                    if ( (ret_val != NRF_ERROR_INVALID_STATE) && (ret_val != NRF_ERROR_BUSY) )
                    {
                        APP_ERROR_CHECK(ret_val);
                    }
                } while (ret_val == NRF_ERROR_BUSY);
								*/
                index = 0;
            }
            break;

        /**@snippet [Handling data from UART] */
        case APP_UART_COMMUNICATION_ERROR:
            NRF_LOG_ERROR("Communication error occurred while handling UART.");
            APP_ERROR_HANDLER(p_event->data.error_communication);
						NVIC_SystemReset();
            break;

        case APP_UART_FIFO_ERROR:
            NRF_LOG_ERROR("Error occurred in FIFO module used by UART.");
            APP_ERROR_HANDLER(p_event->data.error_code);
						NVIC_SystemReset();
            break;

        default:
            break;
    }
}


/**@brief Callback handling NUS Client events.
 *
 * @details This function is called to notify the application of NUS client events.
 *
 * @param[in]   p_ble_nus_c   NUS Client Handle. This identifies the NUS client
 * @param[in]   p_ble_nus_evt Pointer to the NUS Client event.
 */

/**@snippet [Handling events from the ble_nus_c module] */
static void ble_nus_c_evt_handler(ble_nus_c_t * p_ble_nus_c, ble_nus_c_evt_t const * p_ble_nus_evt)
{
    ret_code_t err_code;

    switch (p_ble_nus_evt->evt_type)
    {
        case BLE_NUS_C_EVT_DISCOVERY_COMPLETE:
            NRF_LOG_INFO("Discovery complete.");
            err_code = ble_nus_c_handles_assign(p_ble_nus_c, p_ble_nus_evt->conn_handle, &p_ble_nus_evt->handles);
            APP_ERROR_CHECK(err_code);

            err_code = ble_nus_c_tx_notif_enable(p_ble_nus_c);
            APP_ERROR_CHECK(err_code);
            NRF_LOG_INFO("Connected to device with Nordic UART Service.");
            break;

        case BLE_NUS_C_EVT_NUS_TX_EVT:
            ble_nus_chars_received_uart_print(p_ble_nus_evt->p_data, p_ble_nus_evt->data_len);
            break;

        case BLE_NUS_C_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected.");
            scan_start();
            break;
    }
}
/**@snippet [Handling events from the ble_nus_c module] */


/**
 * @brief Function for shutdown events.
 *
 * @param[in]   event       Shutdown type.
 */
static bool shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
    ret_code_t err_code;

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    switch (event)
    {
        case NRF_PWR_MGMT_EVT_PREPARE_WAKEUP:
            // Prepare wakeup buttons.
            err_code = bsp_btn_ble_sleep_mode_prepare();
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(shutdown_handler, APP_SHUTDOWN_HANDLER_PRIORITY);


/**@brief Reads an advertising report and checks if a EarTag manufactuer specific
 *        data is present.
 *
 * @details The function is able to search for manufactuer specific data having
 *          length at least 24 bytes, and having company ID 0x0059.
 */
static uint32_t find_eartag_data(ble_gap_evt_adv_report_t const * const p_adv_report,
                                 uint8_t                        * p_buff,
                                 uint8_t                        * p_len)
{
    uint16_t  index  = 0;
    uint8_t * p_data = (uint8_t *)p_adv_report->data;

    while (index < p_adv_report->dlen)
    {
        uint8_t field_length = p_data[index];
        uint8_t field_type   = p_data[index + 1];
			
				
        if (field_type == BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA)
        {
            
					
					 uint16_t company_id = p_data[index + 2] | (p_data[index + 3] << 8);
					
					  //printf("is manu data field %d \r\n", company_id);
						
					
						//printf("index is %d pdata is %x %x %x %x %x %x %x %x %x %x %x %x\r\n", index, p_data[index], p_data[index + 1], p_data[index+2], p_data[index+3], p_data[index+4], p_data[index + 5], p_data[index+6], p_data[index+7], p_data[index+8], p_data[index + 9], p_data[index+10], p_data[index+11]);

            if (company_id == COMPANY_ID && field_length >= (MANU_SPEC_DATA_MIN_LENGTH + 2))
            {
							//printf("is company id \r\n"); 5 is manu, up to 18 is udid, 19 is :, 20&21 is voltage, 22 is : 23&24 is temperature, 25 is : 26 is 0 27 - 31 is 0, 32 is 2, 
							printf("\r\n");
							for(int i = 0; i<37; i++)
							{
								//printf("%d=%x;",i,p_data[i]);
							}
							
							printf("\r\n");//%x %x %x %x %x %x %x %x %x %x %x %x %x %x %x\r\n",p_data[18], p_data[19], p_data[20], p_data[21], p_data[22], p_data[23], p_data[24], p_data[25], p_data[26], p_data[32], p_data[33], p_data[34], p_data[35], p_data[36], p_data[37]);
							
							 *p_len = (*p_len > field_length-3) ? field_length-3: *p_len;
                memcpy(p_buff, &p_data[index + 4], *p_len);
                return NRF_SUCCESS;
            }
        }
        index += field_length + 1;
    }
    return NRF_ERROR_NOT_FOUND;
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t            err_code;
    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;
		

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_ADV_REPORT:
        {
            ble_gap_evt_adv_report_t const * p_adv_report = &p_gap_evt->params.adv_report;
            uint8_t buff[64];
						uint8_t fbuff[64];
					
						memset(buff, 0, sizeof(buff));
						memset(fbuff, 0, sizeof(fbuff));
						
           
					
						/* prepending the UART transmission with package type (1) that is required for handling data on server side */
						//sprintf((char *)&buff[0], "1:");
						uint8_t len = sizeof(buff);
						
            err_code = find_eartag_data(p_adv_report, buff, &len);
            if (err_code == NRF_SUCCESS)
            {
                /* Fill non-ASCII data by 'x'. */
//                for (uint16_t i = 0; i < len; i++)
//                {
//                    if (buff[i] < 1 || buff[i] > 127)
//                    {
//                        buff[i] = 'x';
//												
//                    }
//										printf("%d=%x;",i, buff[i]);
//                }
								
								//long int packetversion  = 0;
								//long int payloadvar = 0;
								//char packetver[6];
								//char * str2 = "0xf00d";
								//sprintf(packetver, "%c%c");

								//packetversion = strtol((char*)buff[21], 0, 16);  //converts hexadecimal string to long.
								//num2 = strtol( str2, 0, 0); //conversion depends on the string passed in, 0x... Is hex, 0... Is octal and everything else is decimal.

							
                /* Cascade by RSSI. */
                //sprintf((char *)&buff[len], ":%2x\r\n", (uint8_t)p_adv_report->rssi);
								/* Flash the LED */
								nrf_gpio_pin_toggle(LED_4);
								/* Calculate RSSI. */
								signed int rssi_calc = (uint8_t)p_adv_report->rssi;
								rssi_calc -= 256;
								//int packetver;
								//strtol(packetver, buff[12], 2);
								
								if(buff[12] == 3)
								{
									
									sprintf((char *)&fbuff, "%x:%c%c%c%c%c%c:%c%c%c%c%c%c:%x:%x:%x:%x:%x:%x:%x:%d:%s;",buff[12], buff[0], buff[1], buff[2], buff[3], buff[4], buff[5], buff[6], buff[7], buff[8], buff[9], buff[10], buff[11], buff[13],buff[14],buff[15],buff[16],buff[17],buff[18],buff[19], abs(rssi_calc), device_name);
									
								}else{
									
									
									sprintf((char *)&fbuff, "%x:%c%c%c%c%c%c%c%c%c%c%c%c:%x:%x:%x:%x:%x:%x:%x:%d:%s;",buff[12], buff[0], buff[1], buff[2], buff[3], buff[4], buff[5], buff[6], buff[7], buff[8], buff[9], buff[10], buff[11], buff[13],buff[14],buff[15],buff[16],buff[17],buff[18],buff[19], abs(rssi_calc), device_name);
									
								}
								
								//sprintf((char *)&fbuff, "%x:%c%c%c%c%c%c%c%c%c%c%c%c:%x:%x:%x:%x:%x:%x:%x:%d:%s;\r\n",buff[12], buff[0], buff[1], buff[2], buff[3], buff[4], buff[5], buff[6], buff[7], buff[8], buff[9], buff[10], buff[11], buff[13],buff[14],buff[15],buff[16],buff[17],buff[18],buff[19], abs(rssi_calc), device_name);

                //snprintf((char *)&buff[12], 100, "%x:%x:%x:%x:%x:%x:%d:%s;\r\n",buff[14],buff[15],buff[16],buff[17],buff[18],buff[19], abs(rssi_calc), device_name);
								//NRF_LOG_INFO("rssi is %d", abs(rssi_calc));
								
								/*add packet type */
								uint8_t len2 = strlen((char *)&ubuff);
								//sprintf((char *)&ubuff[len2], "%x:", buff[12]);
								/*concentate strings to fill the buffer */
								strcat((char *)ubuff, (char *)fbuff);
								/*
								printf("\r\n uart: ");
								for(int i = 0; i< 30; i++)
								{
									printf("%c",(char *)ubuff[i]);
								}
								printf("\r\n");
								*/
								buff_counter ++;
                
								if(buff_counter > BUFFSIZE){
									/* Dump to UART. */
									printf((char *)ubuff);
									memset(ubuff, 0, sizeof(ubuff));
									buff_counter = 0;
									
									spi_xfer_done = false;
		
									static uint8_t       m_tx_buf2[] = {0x10, 0x60};

									APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, m_tx_buf2, m_length, m_rx_buf, m_length));

									//while (!spi_xfer_done)
									
								}else{
									
									spi_xfer_done = false;
									static uint8_t       m_tx_buf[] = {0x10, (0x64 | 0x61)};  

									APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, m_tx_buf, m_length, m_rx_buf, m_length));
		
		
									//NRF_LOG_INFO("listening");
									//while (!spi_xfer_done)
									//NRF_LOG_INFO("not listening");
									
								}
            }
        }break; // BLE_GAP_EVT_ADV_REPORT

        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected to target");
            err_code = ble_nus_c_handles_assign(&m_ble_nus_c, p_ble_evt->evt.gap_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);

            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);

            // start discovery of services. The NUS Client waits for a discovery result
            err_code = ble_db_discovery_start(&m_db_disc, p_ble_evt->evt.gap_evt.conn_handle);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_TIMEOUT:
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_SCAN)
            {
                NRF_LOG_INFO("Scan timed out.");
                scan_start();
            }
            else if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                NRF_LOG_INFO("Connection Request timed out.");
            }
            break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(p_ble_evt->evt.gap_evt.conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
            // Accepting parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                                    &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
            break;

#ifndef S140
        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;
#endif

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)
    {
        NRF_LOG_INFO("ATT MTU exchange completed.");

        m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        NRF_LOG_INFO("Ble NUS max data length set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
}


/**@brief Function for initializing the GATT library. */
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_central_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in] event  Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
    ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
            break;

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_ble_nus_c.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        default:
            break;
    }
}

/**@brief Function for initializing the UART. */
static void uart_init(void)
{
    ret_code_t err_code;

    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = 20, //RX_PIN_NUMBER,
        .tx_pin_no    = 41, //TX_PIN_NUMBER,
        .rts_pin_no   = 34, //RTS_PIN_NUMBER,
        .cts_pin_no   = 38, //CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_ENABLED,
        .use_parity   = false,
        .baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);

    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the NUS Client. */
static void nus_c_init(void)
{
    ret_code_t       err_code;
    ble_nus_c_init_t init;

    init.evt_handler = ble_nus_c_evt_handler;

    err_code = ble_nus_c_init(&m_ble_nus_c, &init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing buttons and leds. */
static void buttons_leds_init(void)
{
    ret_code_t err_code;
    bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LED, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the timer. */
static void timer_init(void)
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(2); /**< Declaring an instance of nrf_drv_rtc for RTC0. */

/** @brief: Function for handling the RTC2 interrupts.
 * Triggered on TICK and COMPARE0 match.
 */
static void rtc_handler(nrf_drv_rtc_int_type_t int_type)
{
    if (int_type == NRF_DRV_RTC_INT_COMPARE2)
    {
        //NRF_LOG_INFO("fire");
				nrf_drv_rtc_counter_clear(&rtc);
			
				uptime_counter++;
			
				if (uptime_counter > 30){
					
					/* Sending device name and uptime to the server */
					//uint8_t buff[64];
					//memset(buff, 0, sizeof(buff));		 
					//sprintf((char *)&buff, "3:%s:%d;",device_name, uptime_counter);

					/* Dump to UART. */
					//printf((char *)buff);
					NVIC_SystemReset();
					
					
					
				}
    }
    else if (int_type == NRF_DRV_RTC_INT_TICK)
    {
        NRF_LOG_INFO("tick");
    }
}



/** @brief Function initialization and configuration of RTC driver instance.
 */
static void rtc_config(void)
{
    uint32_t err_code;

    //Initialize RTC instance
    nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
    config.prescaler = 4095;
    err_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
    APP_ERROR_CHECK(err_code);

    //Enable tick event & interrupt
    nrf_drv_rtc_tick_enable(&rtc,false);

    //Set compare channel to trigger interrupt after COMPARE_COUNTERTIME seconds
    err_code = nrf_drv_rtc_cc_set(&rtc,2,COMPARE_COUNTERTIME * 8,true);
    APP_ERROR_CHECK(err_code);

    //Power on RTC instance
    nrf_drv_rtc_enable(&rtc);
}


/**@brief Function for initializing the nrf log module. */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing the Power manager. */
static void power_init(void)
{
    ret_code_t err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/** @brief Function for initializing the Database Discovery Module. */
static void db_discovery_init(void)
{
    ret_code_t err_code = ble_db_discovery_init(db_disc_handler);
    APP_ERROR_CHECK(err_code);
}

/*// Init for LFCLK
static void lfclk_request(void)
{
    ret_code_t err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);
    nrf_drv_clock_lfclk_request(NULL);

    if(nrf_drv_clock_lfclk_is_running()){
		NRF_LOG_INFO("LFCLK: ON \r\n ");
	}
}

*/

/**@brief Function for setting up amplifier-GPIOs PPI.
 */
void sky66114_ctrl(void)
{
    NRF_GPIOTE->CONFIG[6] = 0x00032D03;     // CTX: GPIOTE channnel #6, TASK mode, GPIO 45 (P1.13), TOGGLE, default low
    NRF_GPIOTE->CONFIG[7] = 0x00032403;     // CRX: GPIOTE channnel #7, TASK mode, GPIO 36 (P1.04), TOGGLE, default low

    NRF_PPI->CH[14].EEP = (uint32_t)&NRF_RADIO->EVENTS_TXREADY;     // PPI channel #14, EEP <= RADIO.EVENTS_TXREADY
    NRF_PPI->CH[14].TEP = (uint32_t)&NRF_GPIOTE->TASKS_SET[6];      // PPI channel #14, TEP <= GPIOTE.TASKS_SET[6]

    NRF_PPI->CH[15].EEP = (uint32_t)&NRF_RADIO->EVENTS_RXREADY;     // PPI channel #15, EEP <= RADIO.EVENTS_RXREADY
    NRF_PPI->CH[15].TEP = (uint32_t)&NRF_GPIOTE->TASKS_SET[7];      // PPI channel #15, TEP <= GPIOTE.TASKS_SET[7]

    NRF_PPI->CH[16].EEP = (uint32_t)&NRF_RADIO->EVENTS_DISABLED;    // PPI channel #16, EEP <= RADIO.EVENTS_DISABLED
    NRF_PPI->CH[16].TEP = (uint32_t)&NRF_GPIOTE->TASKS_CLR[6];      // PPI channel #16, TEP <= GPIOTE.TASKS_CLR[6]
    NRF_PPI->FORK[16].TEP = (uint32_t)&NRF_GPIOTE->TASKS_CLR[7];    // PPI channel #16: FORK.TEP <= GPIOTE.TASKS_CLR[7]

    NRF_PPI->CHENSET = 0x0001C000;                                  // Enable PPI channel #14, 15 & 16
}

int main(void)
{
    log_init();
    timer_init();
		rtc_config();
    power_init();
    uart_init();
		printf("+++");
		nrf_delay_ms(2000);
		printf("ATDB\r\n");
		nrf_delay_ms(2000);
		printf("ATCN\r\n");
    buttons_leds_init();
		/* SHTC1 setup */
		twi_init();
		read_sensor_id();
		SHTC1_set_mode();
		while (m_xfer_done == false);

		read_sensor_data();
	
		while (m_xfer_done == false);

		read_sensor_data();
	
		while (m_xfer_done == false);
	
		nrf_drv_twi_uninit(&m_twi);
		
		spi_init();
		
		// Reset rx buffer and transfer done flag
		memset(m_rx_buf, 0, m_length);
		
			
		spi_xfer_done = false;
		
		static uint8_t       m_tx_buf1[] = {0x10, 0xFF};

		APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, m_tx_buf1, m_length, m_rx_buf, m_length));

		while (!spi_xfer_done)
			
		
		
		saadc_init();
		ret_code_t err_code = nrf_drv_saadc_sample();
	
    db_discovery_init();
    ble_stack_init();
	  sky66114_ctrl();
    gatt_init();
    nus_c_init();
		 //bsp_board_led_invert(LED_4);
		//nrf_gpio_pin_set(LED_4);
		//nrf_gpio_pin_set(LED_3);
		//cps 1.15 must be off
		nrf_gpio_cfg_output(CPS);
		nrf_gpio_pin_clear(CPS);
		//crx 1.04 must be on
		//nrf_gpio_cfg_output(CRX);
		//nrf_gpio_pin_set(CRX);
		//ctx 1.13 must be off
		//nrf_gpio_cfg_output(CTX);
		//nrf_gpio_pin_clear(CTX);
		
		/* Charge status */
		
		//nrf_gpio_cfg_input(15);
		nrf_gpio_cfg_input (15, NRF_GPIO_PIN_NOPULL);
		uint32_t pin_value = nrf_gpio_pin_read (15);
		
		
		/* Creating device name that consists of first 3 chars of Device ID */
		uint32_t deviceID = NRF_FICR->DEVICEID[0];
		char tempID[15];
		sprintf((char *)&tempID, "%x", deviceID);
		strncpy(device_name,tempID,3);
		
		/* Sending device name and Device ID to the server */
		uint8_t buff[64];
    memset(buff, 0, sizeof(buff));		 
    sprintf((char *)&buff, "4:%s:%04x%04x:%d:%d:%d:%d:%c%c;\r\n",device_name, NRF_FICR->DEVICEID[0], NRF_FICR->DEVICEID[1], (uint8_t) round(mTemperature), (uint8_t)round(mHumidity), pin_value, voltage, xrssi[3], xrssi[4]);

     /* Dump to UART. */
     printf((char *)buff);

    // Start scanning for peripherals and initiate connection
    // with devices that advertise NUS UUID.
    //NRF_LOG_INFO("BLE UART central example started.");
    
		
		spi_xfer_done = false;
		
		static uint8_t       m_tx_buf2[] = {0x10, 0x60};

		APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, m_tx_buf2, m_length, m_rx_buf, m_length));

		while (!spi_xfer_done)
			
		scan_start();
		
		startup = false;
		

    for (;;)
    {
        
			 /* Dump to UART. */
     
			 if (NRF_LOG_PROCESS() == false)
        {
            nrf_pwr_mgmt_run();
        }
    }
}
