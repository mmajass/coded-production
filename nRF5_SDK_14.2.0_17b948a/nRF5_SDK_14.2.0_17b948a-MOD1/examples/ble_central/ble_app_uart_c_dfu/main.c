/**
 * Copyright (c) 2016 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "nrf_dfu_svci.h"
#include "nrf_svci_async_function.h"
#include "nrf_svci_async_handler.h"
#include "ble_srv_common.h"
#include "ble_dfu.h"

#include "nordic_common.h"
#include "app_error.h"
#include "app_uart.h"
#include "ble_db_discovery.h"
#include "app_timer.h"
#include "app_util.h"
#include "bsp_btn_ble.h"
#include "ble.h"
//#include "ble_gap.h"
#include "ble_hci.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "nrf_pwr_mgmt.h"
#include "ble_advdata.h"
#include "ble_nus_c.h"
//#include "ble_nus.h"
#include "nrf_ble_gatt.h"
#include "ble_conn_params.h"

#include "eartag_adv.h"


#include "nrf_drv_rtc.h"
#include "nrf_drv_clock.h"



#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


/* defines are copied from 3 projects and may overlap - uart_c, app_uart and buttonless DFU */

#define APP_BLE_CONN_CFG_TAG    1                                       /**< A tag that refers to the BLE stack configuration we set with @ref sd_ble_cfg_set. Default tag is @ref BLE_CONN_CFG_TAG_DEFAULT. */
#define APP_BLE_OBSERVER_PRIO   3                                       /**< Application's BLE observer priority. You shoulnd't need to modify this value. */

#define UART_TX_BUF_SIZE        256                                     /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE        256                                     /**< UART RX buffer size. */

#define DEVICE_NAME             "Reader"                                    /**< Name of device. Will be included in the advertising data. */
#define NUS_SERVICE_UUID_TYPE   BLE_UUID_TYPE_VENDOR_BEGIN              /**< UUID type for the Nordic UART Service (vendor specific). */

#define SCAN_INTERVAL           0x0050                                  /**< Determines scan interval in units of 0.625 millisecond. */
#define SCAN_WINDOW             0x0050                                  /**< Determines scan window in units of 0.625 millisecond. */
#define SCAN_TIMEOUT            0x0000                                  /**< Timout when scanning. 0x0000 disables timeout. */

#define MIN_CONNECTION_INTERVAL MSEC_TO_UNITS(20, UNIT_1_25_MS)         /**< Determines minimum connection interval in millisecond. */
#define MAX_CONNECTION_INTERVAL MSEC_TO_UNITS(75, UNIT_1_25_MS)         /**< Determines maximum connection interval in millisecond. */
#define SLAVE_LATENCY           0                                       /**< Determines slave latency in counts of connection events. */
#define SUPERVISION_TIMEOUT     MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Determines supervision time-out in units of 10 millisecond. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
//#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */


#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */


#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      10                                          /**< The advertising timeout (in units of seconds). */

#define APP_CODED_ADV_INTERVAL          MSEC_TO_UNITS(3000, UNIT_0_625_MS)          /**< The advertising (coded PHY) interval (in units of 0.625 ms. This value corresponds to 40 ms). */


#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */



#define UUID16_SIZE             2                                       /**< Size of 16 bit UUID */
#define UUID32_SIZE             4                                       /**< Size of 32 bit UUID */
#define UUID128_SIZE            16                                      /**< Size of 128 bit UUID */

#define ECHOBACK_BLE_UART_DATA  0                                       /**< Echo the UART data that is received over the Nordic UART Service back to the sender. */

#define COMPANY_ID                  0x0059                              /**< Nordic Semiconductor. */
#define MANU_SPEC_DATA_MIN_LENGTH   24                                  /**< Minimum length of manufacturer specific data. */


/* this data is being sent as part of self-diagnosis timer - its enquiring XBEE PRO module with AT commands and 
		sends data once done, UART is blocked from eartag data while analytics data is sent */
char device_name[4];
static uint8_t temp[10];
//static uint8_t volts[10];
static uint8_t rssi[10]; 

#define FW_VERSION                      0x01                                        /**< Firmware version. */
#define WRITE_PERIOD                    8                                           /**< For every 8 EARTAG_SCAN_EVT_RESULTS, write to file. */



/* The board uses SKYWORKS SKY66114-11 amplifier, here are the amplifier controls */

#define CPS          NRF_GPIO_PIN_MAP(1,15)
#define CRX          NRF_GPIO_PIN_MAP(1,04)
#define CTX          NRF_GPIO_PIN_MAP(1,13)

/* Timer for setting up self-diagnosis */
#define STATUS_DATA_INTERVAL     APP_TIMER_TICKS(30000)                  /**< Tick / Ms */
#define STATUS_RESPONSE_INTERVAL     APP_TIMER_TICKS(2000) 
APP_TIMER_DEF(sd_timer_id);

/*single-shot timer used during AT commands */
APP_TIMER_DEF(status_timer_id);


/*counting uptime in 30s intervals */
static int uptime_counter;

/* flag for stopping eartag UART transmissions while sending analytics data */
static bool uart_free = true;


/* Logic for the AT communications with XBEE PRO */
static int current_status;
static void get_status_data(void);

BLE_NUS_C_DEF(m_ble_nus_c);                                             /**< BLE NUS service client instance. */
NRF_BLE_GATT_DEF(m_gatt);                                               /**< GATT module instance. */
BLE_DB_DISCOVERY_DEF(m_db_disc);                                        /**< DB discovery module instance. */
EARTAG_ADV_DEF(m_adv);

static void scan_start(void);

static uint16_t m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - OPCODE_LENGTH - HANDLE_LENGTH; /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */


static uint16_t   m_conn_handle          = BLE_CONN_HANDLE_INVALID;                 /**< Handle of the current connection. */
static ble_uuid_t m_adv_uuids[]          =                                          /**< Universally unique service identifier. */
{
    {BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}
};


//static uint8_t    m_batt_lv = 80;                                                   /**< battery level. */
//static uint8_t    m_temperature = 25;                                               /**< Temperature. */

//static uint8_t       m_cnt = 0;                                                     /**< Test counter. */
//static bool          m_reset_pending = false;                                       /**< Flag which tells whether reset has been pending. */



/** @brief Parameters used when scanning. */
#if (NRF_SD_BLE_API_VERSION == 6)
static ble_gap_scan_params_t m_scan_params =
{
    .active            = 0,
    .interval          = SCAN_INTERVAL,
    .window            = SCAN_WINDOW,
    .filter_policy     = BLE_GAP_SCAN_FP_ACCEPT_ALL,
    .filter_duplicates = BLE_GAP_SCAN_DUPLICATES_REPORT,
    .scan_phy          = BLE_GAP_PHY_CODED,
    .duration          = SCAN_TIMEOUT,
    .period            = 0x0000, // No period.
	

};
#else
static ble_gap_scan_params_t const m_scan_params =
{
    .active   = 1,
    .interval = SCAN_INTERVAL,
    .window   = SCAN_WINDOW,
    .timeout  = SCAN_TIMEOUT,
    #if (NRF_SD_BLE_API_VERSION <= 2)
        .selective   = 0,
        .p_whitelist = NULL,
    #endif
    #if (NRF_SD_BLE_API_VERSION >= 3)
        .use_whitelist = 0,
    #endif
};
#endif

/**@brief Handler for shutdown preparation.
 *
 * @details During shutdown procedures, this function will be called at a 1 second interval
 *          untill the function returns true. When the function returns true, it means that the
 *          app is ready to reset to DFU mode.
 *
 * @param[in]   event   Power manager event.
 *
 * @retval  True if shutdown is allowed by this power manager handler, otherwise false.
 */
static bool app_shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
    switch (event)
    {
        case NRF_PWR_MGMT_EVT_PREPARE_DFU:
            NRF_LOG_INFO("Power management wants to reset to DFU mode.");
            // YOUR_JOB: Get ready to reset into DFU mode
            //
            // If you aren't finished with any ongoing tasks, return "false" to
            // signal to the system that reset is impossible at this stage.
            //
            // Here is an example using a variable to delay resetting the device.
            //
            // if (!m_ready_for_reset)
            // {
            //      return false;
            // }
            // else
            //{
            //
            //    // Device ready to enter
            //    uint32_t err_code;
            //    err_code = sd_softdevice_disable();
            //    APP_ERROR_CHECK(err_code);
            //    err_code = app_timer_stop_all();
            //    APP_ERROR_CHECK(err_code);
            //}
            break;

        default:
            // YOUR_JOB: Implement any of the other events available from the power management module:
            //      -NRF_PWR_MGMT_EVT_PREPARE_SYSOFF
            //      -NRF_PWR_MGMT_EVT_PREPARE_WAKEUP
            //      -NRF_PWR_MGMT_EVT_PREPARE_RESET
            return true;
    }

    NRF_LOG_INFO("Power management allowed to reset to DFU mode.");
    return true;
}

//lint -esym(528, m_app_shutdown_handler)
/**@brief Register application shutdown handler with priority 0.
 */
NRF_PWR_MGMT_HANDLER_REGISTER(app_shutdown_handler, 0);

/**@brief EarTag advertising event handler. */
void eartag_adv_evt_handler(eartag_adv_evt_t const adv_evt)
{
    switch (adv_evt)
    {
        case EARTAG_ADV_EVT_CODED_NONCONN_ADV:
            NRF_LOG_INFO("EARTAG_ADV_EVT_CODED_NONCONN_ADV");
						scan_start();
            break;

        case EARTAG_ADV_EVT_LEGACY_ADV:
            NRF_LOG_INFO("EARTAG_ADV_EVT_LEGACY_ADV");
            break;

        case EARTAG_ADV_EVT_STOPPED:
            NRF_LOG_INFO("EARTAG_ADV_EVT_STOPPED");
            break;

        default:
            break;
    }
}

/**@brief EarTag advertising error handler. */
void eartag_adv_error_handler (uint32_t err_code)
{
    APP_ERROR_CHECK(err_code);
}

// YOUR_JOB: Use UUIDs for service(s) used in your application.
//static ble_uuid_t m_adv_uuids[] = {{BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}};


// YOUR_JOB: Update this code if you want to do anything given a DFU event (optional).
/**@brief Function for handling dfu events from the Buttonless Secure DFU service
 *
 * @param[in]   event   Event from the Buttonless Secure DFU service.
 */
static void ble_dfu_evt_handler(ble_dfu_buttonless_evt_type_t event)
{
    switch (event)
    {
        case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
            NRF_LOG_INFO("Device is preparing to enter bootloader mode.");
            // YOUR_JOB: Disconnect all bonded devices that currently are connected.
            //           This is required to receive a service changed indication
            //           on bootup after a successful (or aborted) Device Firmware Update.
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER:
            // YOUR_JOB: Write app-specific unwritten data to FLASH, control finalization of this
            //           by delaying reset by reporting false in app_shutdown_handler
            NRF_LOG_INFO("Device will enter bootloader mode.");
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
            NRF_LOG_ERROR("Request to enter bootloader mode failed asynchroneously.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            break;

        case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
            NRF_LOG_ERROR("Request to send a response to client failed.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            APP_ERROR_CHECK(false);
            break;

        default:
            NRF_LOG_ERROR("Unknown event from ble_dfu_buttonless.");
            break;
    }
}

/**@brief Function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num     Line number of the failing ASSERT call.
 * @param[in] p_file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(0xDEADBEEF, line_num, p_file_name);
}


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t err_code;
    ble_dfu_buttonless_init_t dfus_init =
    {
        .evt_handler = ble_dfu_evt_handler
    };

    // Initialize the async SVCI interface to bootloader.
    err_code = ble_dfu_buttonless_async_svci_init();
    APP_ERROR_CHECK(err_code);


    err_code = ble_dfu_buttonless_init(&dfus_init);
    APP_ERROR_CHECK(err_code);

    /* YOUR_JOB: Add code to initialize the services used by the application.
       uint32_t                           err_code;
       ble_xxs_init_t                     xxs_init;
       ble_yys_init_t                     yys_init;

       // Initialize XXX Service.
       memset(&xxs_init, 0, sizeof(xxs_init));

       xxs_init.evt_handler                = NULL;
       xxs_init.is_xxx_notify_supported    = true;
       xxs_init.ble_xx_initial_value.level = 100;

       err_code = ble_bas_init(&m_xxs, &xxs_init);
       APP_ERROR_CHECK(err_code);

       // Initialize YYY Service.
       memset(&yys_init, 0, sizeof(yys_init));
       yys_init.evt_handler                  = on_yys_evt;
       yys_init.ble_yy_initial_value.counter = 0;

       err_code = ble_yy_service_init(&yys_init, &yy_init);
       APP_ERROR_CHECK(err_code);
     */
}

/**@brief Function for setting up advertising data. */
static void advertising_init(void)
{
    uint32_t err_code;
    eartag_adv_init_t adv_init;

    memset(&adv_init, 0, sizeof(eartag_adv_init_t));

    adv_init.advdata_legacy.name_type               = BLE_ADVDATA_FULL_NAME;
    adv_init.advdata_legacy.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    adv_init.advdata_legacy.include_appearance      = false;
    adv_init.advdata_legacy.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    adv_init.advdata_legacy.uuids_complete.p_uuids  = m_adv_uuids;
    
    adv_init.conn_cfg_tag    = APP_BLE_CONN_CFG_TAG;
    adv_init.interval_legacy = APP_ADV_INTERVAL;
    adv_init.duration_legacy = APP_ADV_TIMEOUT_IN_SECONDS * 100;
    adv_init.interval_coded  = APP_CODED_ADV_INTERVAL;
    adv_init.fw_ver          = FW_VERSION;
    adv_init.batt_lv         = 100;
    adv_init.temperature     = 25;
    adv_init.evt_handler     = eartag_adv_evt_handler;
    adv_init.error_handler   = eartag_adv_error_handler;

    err_code = eartag_adv_init(&m_adv, &adv_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function to start scanning. */
static void scan_start(void)
{
    ret_code_t ret;
	
		NRF_LOG_INFO("scan started.");

    ret = sd_ble_gap_scan_start(&m_scan_params);
    APP_ERROR_CHECK(ret);

    ret = bsp_indication_set(BSP_INDICATE_SCANNING);
    APP_ERROR_CHECK(ret);
}


/**@brief Function for handling database discovery events.
 *
 * @details This function is callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function should forward the events
 *          to their respective services.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */
static void db_disc_handler(ble_db_discovery_evt_t * p_evt)
{
    ble_nus_c_on_db_disc_evt(&m_ble_nus_c, p_evt);
}


/**@brief Function for handling characters received by the Nordic UART Service.
 *
 * @details This function takes a list of characters of length data_len and prints the characters out on UART.
 *          If @ref ECHOBACK_BLE_UART_DATA is set, the data is sent back to sender.
 */
static void ble_nus_chars_received_uart_print(uint8_t * p_data, uint16_t data_len)
{
    ret_code_t ret_val;

    NRF_LOG_DEBUG("Receiving data.");
    NRF_LOG_HEXDUMP_DEBUG(p_data, data_len);

    for (uint32_t i = 0; i < data_len; i++)
    {
        do
        {
            ret_val = app_uart_put(p_data[i]);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("app_uart_put failed for index 0x%04x.", i);
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }
    if (p_data[data_len-1] == '\r')
    {
        while (app_uart_put('\n') == NRF_ERROR_BUSY);
    }
    if (ECHOBACK_BLE_UART_DATA)
    {
        // Send data back to peripheral.
        do
        {
            ret_val = ble_nus_c_string_send(&m_ble_nus_c, p_data, data_len);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("Failed sending NUS message. Error 0x%x. ", ret_val);
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }
}


/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to
 *          a string. The string will be be sent over BLE when the last character received was a
 *          'new line' '\n' (hex 0x0A) or if the string has reached the maximum data length.
 */
void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t uart_data_array[BLE_NUS_MAX_DATA_LEN];
    static uint16_t index = 0;
		
    //uint32_t ret_val;
		
    switch (p_event->evt_type)
    {
        /**@snippet [Handling data from UART] */
        case APP_UART_DATA_READY:
					
				/* based on Current Status value loading the response data in rssi or temp array instead of uart_data_array */
						if (current_status == 2){
							UNUSED_VARIABLE(app_uart_get(&rssi[index]));
							//NRF_LOG_INFO("from uart %c", (char *)uart_data_array[index]);
							index++;
						}else if(current_status == 3){
							
							UNUSED_VARIABLE(app_uart_get(&temp[index]));
							//NRF_LOG_INFO("from uart %c", (char *)uart_data_array[index]);
							index++;
							
						}else{
							UNUSED_VARIABLE(app_uart_get(&uart_data_array[index]));
							//NRF_LOG_INFO("from uart %c", (char *)uart_data_array[index]);
							index++;
							
						}
				
					
					
/*
            if ((data_array[index - 1] == '\n') || (index >= (m_ble_nus_max_data_len)))
            {
                NRF_LOG_DEBUG("Ready to send data over BLE NUS");
                NRF_LOG_HEXDUMP_DEBUG(data_array, index);

                do
                {
                    ret_val = ble_nus_c_string_send(&m_ble_nus_c, data_array, index);
                    if ( (ret_val != NRF_ERROR_INVALID_STATE) && (ret_val != NRF_ERROR_BUSY) )
                    {
                        APP_ERROR_CHECK(ret_val);
                    }
                } while (ret_val == NRF_ERROR_BUSY);

                index = 0;
            }
						
						*/
            break;

        /**@snippet [Handling data from UART] */
        case APP_UART_COMMUNICATION_ERROR:
            NRF_LOG_ERROR("Communication error occurred while handling UART.");
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            NRF_LOG_ERROR("Error occurred in FIFO module used by UART.");
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}


/**@brief Callback handling NUS Client events.
 *
 * @details This function is called to notify the application of NUS client events.
 *
 * @param[in]   p_ble_nus_c   NUS Client Handle. This identifies the NUS client
 * @param[in]   p_ble_nus_evt Pointer to the NUS Client event.
 */

/**@snippet [Handling events from the ble_nus_c module] */
static void ble_nus_c_evt_handler(ble_nus_c_t * p_ble_nus_c, ble_nus_c_evt_t const * p_ble_nus_evt)
{
    ret_code_t err_code;

    switch (p_ble_nus_evt->evt_type)
    {
        case BLE_NUS_C_EVT_DISCOVERY_COMPLETE:
            NRF_LOG_INFO("Discovery complete.");
            err_code = ble_nus_c_handles_assign(p_ble_nus_c, p_ble_nus_evt->conn_handle, &p_ble_nus_evt->handles);
            APP_ERROR_CHECK(err_code);

            err_code = ble_nus_c_tx_notif_enable(p_ble_nus_c);
            APP_ERROR_CHECK(err_code);
            NRF_LOG_INFO("Connected to device with Nordic UART Service.");
            break;

        case BLE_NUS_C_EVT_NUS_TX_EVT:
            ble_nus_chars_received_uart_print(p_ble_nus_evt->p_data, p_ble_nus_evt->data_len);
            break;

        case BLE_NUS_C_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected.");
						/*starting scan if disconnected */
            scan_start();
            break;
    }
}
/**@snippet [Handling events from the ble_nus_c module] */


/**
 * @brief Function for shutdown events.
 *
 * @param[in]   event       Shutdown type.
 */
static bool shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
    ret_code_t err_code;

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    switch (event)
    {
        case NRF_PWR_MGMT_EVT_PREPARE_WAKEUP:
            // Prepare wakeup buttons.
            err_code = bsp_btn_ble_sleep_mode_prepare();
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(shutdown_handler, APP_SHUTDOWN_HANDLER_PRIORITY);


/**@brief Reads an advertising report and checks if a EarTag manufactuer specific
 *        data is present.
 *
 * @details The function is able to search for manufactuer specific data having
 *          length at least 24 bytes, and having company ID 0x0059.
 */
static uint32_t find_eartag_data(ble_gap_evt_adv_report_t const * const p_adv_report,
                                 uint8_t                        * p_buff,
                                 uint8_t                        * p_len)
{
    uint16_t  index  = 0;
    uint8_t * p_data = (uint8_t *)p_adv_report->data;
	
	

    while (index < p_adv_report->dlen)
    {
        uint8_t field_length = p_data[index];
        uint8_t field_type   = p_data[index + 1];

        if (field_type == BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA)
        {
            uint16_t company_id = p_data[index + 2] | (p_data[index + 3] << 8);

            if (company_id == COMPANY_ID && field_length >= (MANU_SPEC_DATA_MIN_LENGTH + 2))
            {
                *p_len = (*p_len > field_length-3) ? field_length-3: *p_len;
								if(uart_free) NRF_LOG_INFO("from scan %s", &p_data[index + 4]);
                memcpy(p_buff, &p_data[index + 4], *p_len);
                return NRF_SUCCESS;
            }
        }
        index += field_length + 1;
    }
    return NRF_ERROR_NOT_FOUND;
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t            err_code;
    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_ADV_REPORT:
        {
            ble_gap_evt_adv_report_t const * p_adv_report = &p_gap_evt->params.adv_report;
            uint8_t buff[64];
            uint8_t len = sizeof(buff);
					
						
            memset(buff, 0, sizeof(buff));
            err_code = find_eartag_data(p_adv_report, buff, &len);
            if (err_code == NRF_SUCCESS)
            {
                /* Fill non-ASCII data by 'x'. */
                for (uint16_t i = 0; i < len; i++)
                {
                    if (buff[i] < 1 || buff[i] > 127)
                    {
                        buff[i] = 'x';
                    }
                }
								/* this has been a test to try to extract device name using the gap library,
								it doesnt work */
								
						/*  uint16_t plen;
								char test[30];
								uint8_t devNameBuff[25];
								err_code = sd_ble_gap_device_name_get(devNameBuff,&plen);
                printf("device name: ");
                for(int cntData=0 ; cntData<25;cntData++)
                               {
                	printf("%02x ",devNameBuff[cntData]);
									sprintf(&test[cntData], "%c", devNameBuff[cntData]);
                               }
															 
								printf("device name again is %s\r\n", test);
								
								*/
								
								/* Flash the LED */
								nrf_gpio_pin_toggle(LED_4);
								
                /* Calculate RSSI. */
								signed int rssi_calc = (uint8_t)p_adv_report->rssi;
								rssi_calc -= 256;
                sprintf((char *)&buff[len], ":%d:%s", abs(rssi_calc), device_name);
								if(uart_free)NRF_LOG_INFO("rssi is %d", abs(rssi_calc));
                /* Dump to UART. */
								/* prepending the UART transmission with package type (1) that is required for handling data on server side */
								if(uart_free)printf("1:");
                if(uart_free)printf((char *)buff);
            }
        }break; // BLE_GAP_EVT_ADV_REPORT

        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected to target");
				
						
				
            err_code = ble_nus_c_handles_assign(&m_ble_nus_c, p_ble_evt->evt.gap_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);

            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
				
				
            // start discovery of services. The NUS Client waits for a discovery result
            err_code = ble_db_discovery_start(&m_db_disc, p_ble_evt->evt.gap_evt.conn_handle);
            APP_ERROR_CHECK(err_code);

            break;

        case BLE_GAP_EVT_TIMEOUT:
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_SCAN)
            {
                NRF_LOG_INFO("Scan timed out.");
                scan_start();
            }
            else if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                NRF_LOG_INFO("Connection Request timed out.");
            }
            break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(p_ble_evt->evt.gap_evt.conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
            // Accepting parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                                    &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
            break;

#ifndef S140
        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;
#endif

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)
    {
        NRF_LOG_INFO("ATT MTU exchange completed.");

        m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        NRF_LOG_INFO("Ble NUS max data length set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
}

/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of
 *          the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}



/**@brief Function for initializing the GATT library. */
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_central_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in] event  Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
    ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
            break;

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_ble_nus_c.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;
				case BSP_EVENT_KEY_1:
						
				/*starting legacy PHY on button press */
						NRF_LOG_INFO("button pressed");
            err_code = eartag_adv_start_legacy(&m_adv);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }
}

/**@brief Function for initializing the UART. */
static void uart_init(void)
{
    ret_code_t err_code;

    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_ENABLED,
        .use_parity   = false,
        .baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);

    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the NUS Client. */
static void nus_c_init(void)
{
    ret_code_t       err_code;
    ble_nus_c_init_t init;

    init.evt_handler = ble_nus_c_evt_handler;

    err_code = ble_nus_c_init(&m_ble_nus_c, &init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing buttons and leds. */
static void buttons_leds_init(bool * p_erase_bonds)
{
    ret_code_t err_code;
    bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);
	
		*p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}

static void get_status_data(void)
{

		uint8_t buff[64];
		uint8_t cr[64];
		static uint16_t index = 0;

		memset(buff, 0, sizeof(buff));
		ret_code_t err_code;	

			
				
		switch (current_status)
		{
			case 0:
				uart_free = false;
				err_code = app_timer_start(status_timer_id, STATUS_RESPONSE_INTERVAL, NULL);
				APP_ERROR_CHECK(err_code);
				break;
			case 1:
										 
			
				printf("+++");
				while(app_uart_get(&cr[index]) == NRF_SUCCESS){
					//NRF_LOG_INFO("+++ response is %s", (char *)cr);
					index++;
				}
				
				
				err_code = app_timer_start(status_timer_id, STATUS_RESPONSE_INTERVAL, NULL);
				APP_ERROR_CHECK(err_code);
				NRF_LOG_INFO("+++");
				
				
				break;
			case 2:
			
				NRF_LOG_INFO("ATDB\r\n");
				/* Dump to UART. */
			
				printf("ATDB\r\n");
				//printf("+++");
				while(app_uart_get(&cr[index]) == NRF_SUCCESS){
					//NRF_LOG_INFO("ATDB is %s", (char *)cr);
					index++;
				}
				err_code = app_timer_start(status_timer_id, STATUS_RESPONSE_INTERVAL, NULL);
				APP_ERROR_CHECK(err_code);
				
				break;
			case 3:
				
				NRF_LOG_INFO("ATTP\r\n");
				/* Dump to UART. */
			
				printf("ATTP\r\n");
				//printf("+++");
				while(app_uart_get(&cr[index]) == NRF_SUCCESS){
					//NRF_LOG_INFO("ATDB is %s", (char *)cr);
					index++;
				}
				err_code = app_timer_start(status_timer_id, STATUS_RESPONSE_INTERVAL, NULL);
				APP_ERROR_CHECK(err_code);
				
				break;
			case 4:
				/*
				while(app_uart_get(&cr[index]) == NRF_SUCCESS){
					NRF_LOG_INFO("ATDB is %s", (char *)cr);
					index++;
				}
				*/
					nrf_gpio_pin_toggle(LED_3);
					memset(buff, 0, sizeof(buff));		 
					sprintf((char *)&buff, "ATCN\r\n");
					NRF_LOG_INFO("ATCN\r\n");
					/* Dump to UART. */
					printf((char *)buff);
					uart_free = true;
					/*send to server*/
				
				 
				 memset(buff, 0, sizeof(buff));	
		
			
				 sprintf((char *)&buff, "3:%04x%04x:%x %x:%x %x:%d",NRF_FICR->DEVICEID[0], NRF_FICR->DEVICEID[1], rssi[3],rssi[4], temp[6], temp[7],uptime_counter); 
				 /* Dump to UART. */
				 printf((char *)buff);
				 current_status++;
				
				
				break;
			case 5:
				memset(buff, 0, sizeof(buff));		 
				sprintf((char *)&buff, "AT%%V\r\n");
				NRF_LOG_INFO("AT%%V\r\n");
				/* Dump to UART. */
				printf((char *)buff);
				while(app_uart_get(&cr[index]) == NRF_SUCCESS){
					//NRF_LOG_INFO("ATDB is %s", (char *)cr);
					index++;
				}
				
				err_code = app_timer_start(status_timer_id, STATUS_RESPONSE_INTERVAL, NULL);
				APP_ERROR_CHECK(err_code);

				
				break;
			case 6:
				/*
				while(app_uart_get(&cr[index]) == NRF_SUCCESS){
					NRF_LOG_INFO("ATV is %s", (char *)cr);
					index++;
				}		*/	
				
				err_code = app_timer_start(status_timer_id, STATUS_RESPONSE_INTERVAL, NULL);
				APP_ERROR_CHECK(err_code);

				
				break;
			case 7:
				memset(buff, 0, sizeof(buff));		 
				sprintf((char *)&buff, "ATTP\r\n");
				NRF_LOG_INFO("ATTP\r\n");
				/* Dump to UART. */
				printf((char *)buff);
				while(app_uart_get(&cr[index]) == NRF_SUCCESS){
					//NRF_LOG_INFO("ATDB is %s", (char *)cr);
					index++;
				}
					
				err_code = app_timer_start(status_timer_id, STATUS_RESPONSE_INTERVAL, NULL);
				APP_ERROR_CHECK(err_code);
				
				break;
			case 8:
	 
				 
				err_code = app_timer_start(status_timer_id, STATUS_RESPONSE_INTERVAL, NULL);
				APP_ERROR_CHECK(err_code);
				 
				break;
			case 9:
				nrf_gpio_pin_toggle(LED_3);
				memset(buff, 0, sizeof(buff));		 
				sprintf((char *)&buff, "ATCN\r\n");
				NRF_LOG_INFO("ATCN\r\n");
					/* Dump to UART. */
				printf((char *)buff);
				uart_free = true;
				current_status++;
				
				
			break;
			
			default:
				
				/* no action required */
				break;
		}
			
		
				
				// voltage AT%V convert from HEX to DEC and divide by 1023 on server side
				// temperature ATTP convert from HEX to DEC
	
}


// Timeout handler for the repeated timer
static void timer_sd_handler(void * p_context)
{
	   UNUSED_PARAMETER(p_context);
			
			uptime_counter ++;
	/*
			if (!(uptime_counter % 6)){
				uart_free = true;
				nrf_gpio_pin_toggle(LED_3);
				NRF_LOG_INFO("halt xbee diagnostics");
			}
	   */
	   if (!(uptime_counter % 40)) {
			 current_status = 0;
			 get_status_data();
			 //uart_free = false;
			 nrf_gpio_pin_toggle(LED_3);
			 NRF_LOG_INFO("start xbee diagnostics");
		 }
	  
	NRF_LOG_INFO("timer fire, uptime is %d.",uptime_counter);
		
		 
}

// Timeout handler for the repeated timer
static void timer_status_handler(void * p_context)
{
	   UNUSED_PARAMETER(p_context);
			
		 current_status ++;
	   
	  
		 NRF_LOG_INFO("single timer fire, status is %d.",current_status);
		 get_status_data();
		
		 
}


/**@brief Function for initializing the timer. */
static void timer_init(void)
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
	
		 // Create timers
    err_code = app_timer_create(&sd_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                timer_sd_handler);
    
    APP_ERROR_CHECK(err_code);
	
		err_code = app_timer_create(&status_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                timer_status_handler);
    
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting timers.
 */
static void application_timers_start(void)
{
       ret_code_t err_code;
       err_code = app_timer_start(sd_timer_id, STATUS_DATA_INTERVAL, NULL);
       APP_ERROR_CHECK(err_code);

}



/**@brief Function for initializing the nrf log module. */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing the Power manager. */
static void power_init(void)
{
    ret_code_t err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/** @brief Function for initializing the Database Discovery Module. */
static void db_discovery_init(void)
{
    ret_code_t err_code = ble_db_discovery_init(db_disc_handler);
    APP_ERROR_CHECK(err_code);
}


int main(void)
{
			
		uint32_t err_code;
    bool     erase_bonds;
	
		log_init();
    timer_init();
		application_timers_start();
    power_init();
    uart_init();
    buttons_leds_init(&erase_bonds);
    db_discovery_init();
    ble_stack_init();
		gap_params_init();
    gatt_init();
    nus_c_init();
	
		advertising_init();
	
	  /*service required for DFU */
		services_init();
		conn_params_init();
		/* keeping track of device uptime */
		uptime_counter = 0;
	
		//TRANSMIT LED
		//nrf_gpio_cfg_output(20); //31 on eartag and 45 on collar tag
    //nrf_gpio_pin_clear(20);
	
	  //bsp_board_led_invert(LED_4);
		nrf_gpio_pin_set(LED_4);
		nrf_gpio_pin_set(LED_3);
		//cps 1.15 must be off
		nrf_gpio_cfg_output(CPS);
		nrf_gpio_pin_clear(CPS);
		//crx 1.04 must be on
		nrf_gpio_cfg_output(CRX);
		nrf_gpio_pin_set(CRX);
		//ctx 1.13 must be off
		nrf_gpio_cfg_output(CTX);
		nrf_gpio_pin_clear(CTX);
		
		

	

    // Start scanning for peripherals and initiate connection
    // with devices that advertise NUS UUID.
    NRF_LOG_INFO("BLE UART central example started.");
		
		/*on startup device should stay in Legacy PHY for 20 seconds, if connection is not made, it should start scan in coded PHY */
		/*Legacy PHY based phone connection is required for 
			1. firmware update of the uart_c device (buttonless dfu) 
			2. uploading file to the uart_c device that will gradually update firmware in all EarTag devices over Coded PHY
			3. device diagnosis and troubleshooting (uptime, rssi, power status, sensor readings etc) */
				
    //scan_start();
		err_code = eartag_adv_start_legacy(&m_adv);
          APP_ERROR_CHECK(err_code);
					
		/* from what i understand timers run on HF clock, 
					TODO: move all counting, and timers to RTC clock */
		/*starting clock */
		//rtc_config();
		/* Creating device name that consists of first 3 chars of Device ID */
		uint32_t deviceID = NRF_FICR->DEVICEID[0];
		char tempID[15];
		sprintf((char *)&tempID, "%x", deviceID);
		strncpy(device_name,tempID,3);
		
		/* Sending device name and Device ID to the server */
		uint8_t buff[64];
    memset(buff, 0, sizeof(buff));		 
    sprintf((char *)&buff, "4:%s:%04x%04x",device_name, NRF_FICR->DEVICEID[0], NRF_FICR->DEVICEID[1]);

     /* Dump to UART. */
     printf((char *)buff);
		 

    for (;;)
    {
         
				if (NRF_LOG_PROCESS() == false)
        {
            nrf_pwr_mgmt_run();
        }
    }
}
