/**
 * Copyright (c) 2014 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_nus_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */



#include <stdint.h>
#include <string.h>
#include <math.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "app_timer.h"
//#include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"
//#include "nrf_drv_timer.h"
//#include "nrf_drv_rtc.h"
//#include "nrf_drv_clock.h"
//#include "nrf_delay.h"
#include "ble_nus.h"
#include "app_util_platform.h"
#include "bsp_btn_ble.h"
//#include "nrf_log.h"
//#include "nrf_log_ctrl.h"
//#include "nrf_log_default_backends.h"
//#include "fds.h"
#include "eartag_adv.h"
#include "eartag_scan.h"
//#include "eartag_file.h"
#include "nrf_nvic.h"
#include "nrf_drv_gpiote.h"
#include "app_error.h"
#include "ble.h"


//#define SUPERCAP_VOLTAGE_ON_PIN_NUMBER 26
//#define SUPERCAP_VOLTAGE_MEASURE_PIN_NUMBER  4
//#define SUPERCAP_LOW() (NRF_P0->OUTCLR = (1 << SUPERCAP_VOLTAGE_ON_PIN_NUMBER)) // set ON to low
//#define SUPERCAP_VOLTAGE_ON_OPEN() (NRF_P0->OUTSET = (1 << SUPERCAP_VOLTAGE_ON_PIN_NUMBER)) // set ON to open-drain
//#define SUPERCAP_VOLTAGE_MEASURE_OPEN() (NRF_P0->OUTSET = (1 << SUPERCAP_VOLTAGE_MEASURE_PIN_NUMBER)) // set MEASURE to open-drain

#define APP_BLE_CONN_CFG_TAG            1                                           /**< A tag identifying the SoftDevice BLE configuration. */

#define APP_FEATURE_NOT_SUPPORTED       BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2        /**< Reply when unsupported features are requested. */

#define DEVICE_NAME                     "EarTag"                                    /**< Name of device. Will be included in the advertising data. */
#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

//#define COMPARE_COUNTERTIME  (3UL)                                        /**< Get Compare event COMPARE_TIME seconds after the counter starts from 0. */


#define APP_BLE_OBSERVER_PRIO           3                                           /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      10                                          /**< The advertising timeout (in units of seconds). */

#define APP_CODED_ADV_INTERVAL          MSEC_TO_UNITS(3000, UNIT_0_625_MS)          /**< The advertising (coded PHY) interval (in units of 0.625 ms. This value corresponds to 40 ms). */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */

#define FW_VERSION                      0x01                                        /**< Firmware version. */
#define WRITE_PERIOD                    8                                           /**< For every 8 EARTAG_SCAN_EVT_RESULTS, write to file. */



BLE_NUS_DEF(m_nus);                                                                 /**< BLE NUS service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                           /**< GATT module instance. */
EARTAG_ADV_DEF(m_adv);                                                              /**< Eartag Advertising module instance. */
EARTAG_SCAN_DEF(m_scan);                                                            /**< Eartag Scanning module instance. */
//EARTAG_FILE_DEF(m_file);                                                            /**< Eartag file module instance. */
//static void pa_lna_init(uint32_t gpio_pa_pin, uint32_t gpio_lna_pin);
static uint16_t   m_conn_handle          = BLE_CONN_HANDLE_INVALID;                 /**< Handle of the current connection. */
static uint16_t   m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3;            /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
static ble_uuid_t m_adv_uuids[]          =                                          /**< Universally unique service identifier. */
{
    {BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}
};

//static uint8_t    m_batt_lv = 80;                                                   /**< battery level. */
//static uint8_t    m_temperature = 25;                                               /**< Temperature. */

//static eartag_udid_t m_udid_candidates[EARTAG_SCAN_MAX_NUM_OF_RESULTS];             /**< UDID candidates, to be put to database. */
//static uint8_t       m_cnt = 0;                                                     /**< Test counter. */
//static bool          m_reset_pending = false;                                       /**< Flag which tells whether reset has been pending. */

/* ADC sampling setup */
//#define ADC_REF_VOLTAGE_IN_MILLIVOLTS       600                                          /**< Reference voltage (in milli volts) used by ADC while doing conversion. */
//#define ADC_PRE_SCALING_COMPENSATION        6                                            /**< The ADC is configured to use VDD with 1/3 prescaling as input. And hence the result of conversion is to be multiplied by 3 to get the actual value of the battery voltage.*/
//#define DIODE_FWD_VOLT_DROP_MILLIVOLTS      270                                          /**< Typical forward voltage drop of the diode (Part no: SD103ATW-7-F) that is connected in series with the voltage supply. This is the voltage drop when the forward current is 1mA. Source: Data sheet of 'SURFACE MOUNT SCHOTTKY BARRIER DIODE ARRAY' available at www.diodes.com. */
//#define ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS   1200                                         /**< Value in millivolts for voltage used as reference in ADC conversion on NRF51. */
//#define ADC_INPUT_PRESCALER                 3                                            /**< Input prescaler for ADC convestion on NRF51. */
//#define ADC_RES_10BIT                       1024                                         /**< Maximum digital value for 10-bit ADC conversion. */


//#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE)\
        ((((ADC_VALUE) * ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_INPUT_PRESCALER)



//#define SAMPLES_IN_BUFFER 1
volatile uint8_t state = 1;
//static const nrf_drv_rtc_t     rtc = NRF_DRV_RTC_INSTANCE(2);
//const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(0); /**< Declaring an instance of nrf_drv_rtc for RTC0. */

//static const nrf_drv_timer_t m_timer = NRF_DRV_TIMER_INSTANCE(1);
//static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];
//static nrf_ppi_channel_t     m_ppi_channel;
//static uint32_t              m_adc_evt_counter;

static volatile uint8_t voltage;

//battery measurement stuff
//#define SAMPLES_IN_BUFFER 5
//volatile uint8_t state = 1;
volatile uint8_t txcount = 0;
//static const nrf_drv_timer_t m_timer = NRF_DRV_TIMER_INSTANCE(0);
//static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];
//static nrf_ppi_channel_t     m_ppi_channel;
//static nrf_ppi_channel_t		 clr_ppi_channel;
//static nrf_gpiote
//static uint32_t              m_adc_evt_counter;


static nrf_ppi_channel_t ppi_set_ch;
static nrf_ppi_channel_t ppi_clr_ch;
//static void disp_db(void); 																													/**< dump all database content to log. */
//static void send_database_data(void); 																							/**< send all database data over legacy PHY connection */


/**@brief Functions for analogue - digital conversion
 *
 * @details These functions are required to measure battery voltage
 */

/*
void timer_handler(nrf_timer_event_t event_type, void * p_context)
{

	NRF_LOG_INFO("what");
	
	
}



void saadc_sampling_event_init(void)
{
    ret_code_t err_code;

    err_code = nrf_drv_ppi_init();
    APP_ERROR_CHECK(err_code);
		NRF_LOG_INFO("error 0 is %d", err_code);
    nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
    //timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_32;
    err_code = nrf_drv_timer_init(&m_timer, &timer_cfg, timer_handler);
		NRF_LOG_INFO("error 1 is %d", err_code);
    APP_ERROR_CHECK(err_code);
		

     setup m_timer for compare event every 400ms 
    uint32_t ticks = nrf_drv_timer_ms_to_ticks(&m_timer, 400);
    nrf_drv_timer_extended_compare(&m_timer,
                                   NRF_TIMER_CC_CHANNEL0,
                                   ticks,
                                   NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK,
                                   false);
    nrf_drv_timer_enable(&m_timer);

    uint32_t timer_compare_event_addr = nrf_drv_timer_compare_event_address_get(&m_timer,
                                                                                NRF_TIMER_CC_CHANNEL0);
    uint32_t saadc_sample_task_addr   = nrf_drv_saadc_sample_task_get();

     setup ppi channel so that timer compare event is triggering sample task in SAADC 
    err_code = nrf_drv_ppi_channel_alloc(&m_ppi_channel);
    APP_ERROR_CHECK(err_code);
		NRF_LOG_INFO("error 2 is %d", err_code);

    err_code = nrf_drv_ppi_channel_assign(m_ppi_channel,
                                          timer_compare_event_addr,
                                          saadc_sample_task_addr);
    APP_ERROR_CHECK(err_code);
		NRF_LOG_INFO("error 3 is %d", err_code);
}

*/



/*
void saadc_sampling_event_init(void)
{
    ret_code_t err_code;
    //err_code = nrf_drv_ppi_init();
    
    NRF_LOG_INFO("error 1 %d", err_code);
	  APP_ERROR_CHECK(err_code);
    nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
    err_code = nrf_drv_rtc_init(&m_rtc, &config, rtc_handler);
    APP_ERROR_CHECK(err_code);
		NRF_LOG_INFO("error 1 %d", err_code);
    //Set compare channel 0 to trigger at 4096 Hz
    err_code = nrf_drv_rtc_cc_set(&m_rtc,0,32760,false);//3276
    APP_ERROR_CHECK(err_code);
		NRF_LOG_INFO("error 1 %d", err_code);
    //Power on RTC instance
    nrf_drv_rtc_enable(&m_rtc);
    
    uint32_t rtc_compare_event_addr = nrf_drv_rtc_event_address_get(&m_rtc, NRF_RTC_EVENT_COMPARE_0);
    uint32_t rtc_clear_task_addr = nrf_drv_rtc_task_address_get(&m_rtc, NRF_RTC_TASK_CLEAR);
    uint32_t saadc_sample_event_addr = nrf_drv_saadc_sample_task_get();

    // setup ppi channel so that timer compare event is triggering sample task in SAADC 
    err_code = nrf_drv_ppi_channel_alloc(&m_ppi_channel);
    APP_ERROR_CHECK(err_code);
    NRF_LOG_INFO("error 1 %d", err_code);
    err_code = nrf_drv_ppi_channel_assign(m_ppi_channel, rtc_compare_event_addr, saadc_sample_event_addr);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_ppi_channel_fork_assign(m_ppi_channel, rtc_clear_task_addr);
    APP_ERROR_CHECK(err_code);
}
void saadc_sampling_event_enable(void)
{
    ret_code_t err_code = nrf_drv_ppi_channel_enable(m_ppi_channel);
		NRF_LOG_INFO("enabled PPI");
    APP_ERROR_CHECK(err_code);
}

*/
//void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
//{
//    
//	NRF_LOG_INFO("event\r\n");
//		if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
//    {
//        ret_code_t err_code;

//        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
//        APP_ERROR_CHECK(err_code);

//        int i;
//        //NRF_LOG_INFO("ADC event number: %d", (int)m_adc_evt_counter);
//				NRF_LOG_INFO("saadc done\r\n");
//				int average_voltage = 0;

//        for (i = 0; i < SAMPLES_IN_BUFFER; i++)
//        {
//            //NRF_LOG_INFO("%d", p_event->data.done.p_buffer[i]);
//						//NRF_LOG_INFO("voltage is %d",ADC_RESULT_IN_MILLI_VOLTS(p_event->data.done.p_buffer[i]) +
//            //                      DIODE_FWD_VOLT_DROP_MILLIVOLTS);
//						average_voltage += ADC_RESULT_IN_MILLI_VOLTS(p_event->data.done.p_buffer[i]) +
//                                  DIODE_FWD_VOLT_DROP_MILLIVOLTS;
//					  
//						//NRF_LOG_INFO("voltage %d is %f", 1500.0 / 0.333 /0.6 * 1024.0);
//        }
//				
//				average_voltage = average_voltage / SAMPLES_IN_BUFFER;
//				voltage = round(average_voltage * 10 / 569);
//				//NRF_LOG_INFO("voltage is %d", voltage);
//				err_code = eartag_adv_set_batt_lv(&m_adv, voltage);
//            APP_ERROR_CHECK(err_code);
//				//nrf_drv_saadc_uninit();
//				//NRF_SAADC->INTENCLR = (SAADC_INTENCLR_END_Clear << SAADC_INTENCLR_END_Pos);
//				//NVIC_ClearPendingIRQ(SAADC_IRQn);
//				//NRF_LOG_FLUSH();
//        //m_adc_evt_counter++;
//				//nrf_drv_saadc_uninit();
//    }
//}


//void saadc_init(void)
//{
//    ret_code_t err_code;
//    nrf_saadc_channel_config_t channel_config =
//        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN2);
//		channel_config.gain = NRF_SAADC_GAIN1_3,

//    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
//    APP_ERROR_CHECK(err_code);
//		NRF_LOG_INFO("error 11 is %d", err_code);

//    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
//    APP_ERROR_CHECK(err_code);
//		NRF_LOG_INFO("error 12 is %d", err_code);
//    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
//    APP_ERROR_CHECK(err_code);
//		NRF_LOG_INFO("error 13 is %d", err_code);
//    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER);
//    APP_ERROR_CHECK(err_code);
//		NRF_LOG_INFO("error 14 is %d", err_code);
//}



///** @brief: Function for handling the RTC0 interrupts.
// * Triggered on TICK and COMPARE0 match.
// */


//static void rtc_handler(nrf_drv_rtc_int_type_t int_type)
//{
//    if (int_type == NRF_DRV_RTC_INT_COMPARE0)
//    {
//        NRF_LOG_INFO("ff\r\n");
//				nrf_drv_rtc_counter_clear(&rtc);
//    }
//    else if (int_type == NRF_DRV_RTC_INT_TICK)
//    {
//        NRF_LOG_INFO("tick");
//    }
//}

///** @brief Function starting the internal LFCLK XTAL oscillator.
// */
//static void lfclk_config(void)
//{
//    ret_code_t err_code = nrf_drv_clock_init();
//    APP_ERROR_CHECK(err_code);

//    nrf_drv_clock_lfclk_request(NULL);
//}

///** @brief Function initialization and configuration of RTC driver instance.
// */
//static void rtc_config(void)
//{
//    uint32_t err_code;

//    //Initialize RTC instance
//    nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
//    config.prescaler = 4095;
//    err_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
//    APP_ERROR_CHECK(err_code);

//    //Enable tick event & interrupt
//    nrf_drv_rtc_tick_enable(&rtc,false);

//    //Set compare channel to trigger interrupt after COMPARE_COUNTERTIME seconds
//    err_code = nrf_drv_rtc_cc_set(&rtc,0,COMPARE_COUNTERTIME * 8,true);
//    APP_ERROR_CHECK(err_code);

//    //Power on RTC instance
//    nrf_drv_rtc_enable(&rtc);
//}
/** end battery measuring snippet **/

/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyse
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of
 *          the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_nus    Nordic UART Service structure.
 * @param[in] p_data   Data to be send to UART module.
 * @param[in] length   Length of the data.
 */
/**@snippet [Handling the data received over BLE] */
/*
static void nus_data_handler(ble_nus_evt_t * p_evt)
{

    if (p_evt->type == BLE_NUS_EVT_RX_DATA)
    {
        
				NRF_LOG_DEBUG("Received data from BLE NUS. Writing data on UART.");
        NRF_LOG_HEXDUMP_DEBUG(p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);
				
			
				for (uint32_t i = 0; i < p_evt->params.rx_data.length; i++)
        {
         
                NRF_LOG_INFO("%c",p_evt->params.rx_data.p_data[i]);
					
								//waiting for 9
								if(p_evt->params.rx_data.p_data[i] == '9')
								{
									
									NRF_LOG_INFO("got 9");
									//send_database_data();
									
								}
               
        }
        
    }

}

*/
/**@snippet [Handling the data received over BLE] */


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    //uint32_t       err_code;
    //ble_nus_init_t nus_init;

    //memset(&nus_init, 0, sizeof(nus_init));

    //nus_init.data_handler = nus_data_handler;

    //err_code = ble_nus_init(&m_nus, &nus_init);
    //APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


#if 0   // Disabled sys-off mode.
/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
   // uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
   // APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
   // err_code = bsp_btn_ble_sleep_mode_prepare();
   // APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
   // err_code = sd_power_system_off();
   // APP_ERROR_CHECK(err_code);
}
#endif


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            //NRF_LOG_INFO("Connected");
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            //NRF_LOG_INFO("Disconnected");
            // LED indication will be changed when advertising starts.
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

#ifndef S140
        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            //NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;
#endif

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;
#if !defined (S112)
         case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST:
        {
            ble_gap_data_length_params_t dl_params;

            // Clearing the struct will effectivly set members to @ref BLE_GAP_DATA_LENGTH_AUTO
            memset(&dl_params, 0, sizeof(ble_gap_data_length_params_t));
            err_code = sd_ble_gap_data_length_update(p_ble_evt->evt.gap_evt.conn_handle, &dl_params, NULL);
            APP_ERROR_CHECK(err_code);
        } break;
#endif //!defined (S112)
        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_EVT_USER_MEM_REQUEST:
            err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        {
            ble_gatts_evt_rw_authorize_request_t  req;
            ble_gatts_rw_authorize_reply_params_t auth_reply;

            req = p_ble_evt->evt.gatts_evt.params.authorize_request;

            if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
            {
                if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
                {
                    if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
                    }
                    else
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
                    }
                    auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
                    err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                               &auth_reply);
                    APP_ERROR_CHECK(err_code);
                }
            }
        } break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);
	
		

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);
	
		

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
	

}


/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED))
    {
        m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        //NRF_LOG_INFO("Data len is set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
    //NRF_LOG_DEBUG("ATT MTU exchange completed. central 0x%x peripheral 0x%x",
                 // p_gatt->att_mtu_desired_central,
                 // p_gatt->att_mtu_desired_periph);
}


/**@brief Function for initializing the GATT library. */
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, 64);
    APP_ERROR_CHECK(err_code);
}

/*
void send_database_data(void)
{
		
		uint32_t            err_code;
		uint8_t buff[247];
		uint16_t len;
	
		uint16_t num;
		uint8_t cache;
		eartag_file_db_entry_t * p_udid;
	
	
	
		//clearing buff
		//memset(buff, 0, sizeof(buff));

		//accessing database

		err_code = eartag_file_get_db(&m_file, &p_udid, &num);
		APP_ERROR_CHECK(err_code);

		NRF_LOG_INFO("# of elements in DB: %d", num);
	
		memset(buff, 0, sizeof(buff));
	
		while (num > 0)
	{
		//NRF_LOG_INFO("# of element in db: %d with UDID %s and this UDID has been recorder %d times", num, &p_udid[num-1].udid, p_udid[num-1].count);
		//clearing buff
		len = strlen((char *)&buff);
		//sprintf((char *)&buff[len], "%d:%s:%d-",num,&p_udid[num-1].udid,p_udid[num-1].count);
		//sprintf((char *)&buff[len], "%d",&p_udid[num-1].udid);
		NRF_LOG_INFO("sending %d:%s:%d-",num,&p_udid[num-1].udid,p_udid[num-1].count);
		
		if (num > 10){
			
				if (cache > 10){
					cache = 0;
					len = strlen((char *)&buff);
					err_code = ble_nus_string_send(&m_nus, buff, &len);
					
					if ( (err_code != NRF_ERROR_INVALID_STATE) && (err_code != NRF_ERROR_BUSY) )
					{
							APP_ERROR_CHECK(err_code);
					}
					
					
					
				
					nrf_delay_ms(500);
					memset(buff, 0, sizeof(buff));
				
				}
				
			}
		
		num --;
		cache++;
	}
	
	cache = 0;
	len = strlen((char *)&buff);
	err_code = ble_nus_string_send(&m_nus, buff, &len);
	
	if ( (err_code != NRF_ERROR_INVALID_STATE) && (err_code != NRF_ERROR_BUSY) )
	{
			APP_ERROR_CHECK(err_code);
	}
								
	
	
	free(buff);
						
						
}
*/
/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
 
 /*
void bsp_event_handler(bsp_event_t event)
{
    uint32_t            err_code;
    eartag_file_cfg_t * p_cfg;
    uint16_t            udid[6];
	
		

    switch (event)
    {
        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BSP_EVENT_KEY_0:
            err_code = eartag_adv_start_legacy(&m_adv);
            APP_ERROR_CHECK(err_code);
            break;

        case BSP_EVENT_KEY_1:
            err_code = eartag_file_get_config(&m_file, &p_cfg);
            NRF_LOG_INFO("eartag_file_get_config(), err_code=0x%08x, mode=0x%02x", err_code, p_cfg->mode);
            APP_ERROR_CHECK(err_code);

            p_cfg->mode = (p_cfg->mode == EARTAG_FILE_MODE_HERITAGE)
                        ? EARTAG_FILE_MODE_LOCATION: EARTAG_FILE_MODE_HERITAGE;

            err_code = eartag_file_config_write(&m_file);
            NRF_LOG_INFO("eartag_file_config_write(), err_code=0x%08x, mode=0x%02x", err_code, p_cfg->mode);
            APP_ERROR_CHECK(err_code);

            m_reset_pending = true;
            break;

        case BSP_EVENT_KEY_2:
          
						//(void) eartag_file_db_clear(&m_file);
            break;

        case BSP_EVENT_KEY_3:
            m_batt_lv     ^= 1;
            m_temperature ^= 1;

            err_code = eartag_adv_set_batt_lv(&m_adv, m_batt_lv);
            APP_ERROR_CHECK(err_code);

            err_code = eartag_adv_set_temperature(&m_adv, m_temperature);
            APP_ERROR_CHECK(err_code);

            NRF_LOG_INFO("Inject test database......");
            for (uint16_t i = 0; i < 228; i++)
            {
                for (uint8_t j = 0; j < 6; j++)
                {
                    udid[j] = i;
										
                }
								
                memset(m_udid_candidates, 0, sizeof(m_udid_candidates));
                memcpy(m_udid_candidates[0].data, udid, EARTAG_FILE_UDID_LEN);

                err_code = eartag_file_db_put(&m_file, m_udid_candidates, 1);
                if (err_code != NRF_ERROR_NO_MEM)
                {
                    APP_ERROR_CHECK(err_code);
                }
            }
            NRF_LOG_INFO("Done.");
            disp_db();
            break;
        default:
            break;
    }
}

*/
/**@brief Function for displaying the number of elements in the database. */
/*
static void disp_db(void)
{
    uint32_t err_code;
    uint16_t num;
    eartag_file_db_entry_t * p_udid;

    err_code = eartag_file_get_db(&m_file, &p_udid, &num);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_INFO("# of elements in DB: %d", num);
	
		while (num > 0)
  {
		NRF_LOG_INFO("# of element in db: %d with UDID %s and this UDID has been recorder %d times", num, &p_udid[num-1].udid, p_udid[num-1].count);
		num --;
  }
}

*/
/**@brief EarTag advertising event handler. */
void eartag_adv_evt_handler(eartag_adv_evt_t const adv_evt)
{
    switch (adv_evt)
    {
        case EARTAG_ADV_EVT_CODED_NONCONN_ADV:
           // NRF_LOG_INFO("EARTAG_ADV_EVT_CODED_NONCONN_ADV");
            break;

        case EARTAG_ADV_EVT_LEGACY_ADV:
            //NRF_LOG_INFO("EARTAG_ADV_EVT_LEGACY_ADV");
            break;

        case EARTAG_ADV_EVT_STOPPED:
            //NRF_LOG_INFO("EARTAG_ADV_EVT_STOPPED");
            break;

        default:
            break;
    }
}

/**@brief EarTag advertising error handler. */
void eartag_adv_error_handler (uint32_t err_code)
{
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for setting up advertising data. */
static void advertising_init(void)
{
    uint32_t err_code;
    eartag_adv_init_t adv_init;

    memset(&adv_init, 0, sizeof(eartag_adv_init_t));

    adv_init.advdata_legacy.name_type               = BLE_ADVDATA_FULL_NAME;
    adv_init.advdata_legacy.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    adv_init.advdata_legacy.include_appearance      = false;
    adv_init.advdata_legacy.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    adv_init.advdata_legacy.uuids_complete.p_uuids  = m_adv_uuids;
    
    adv_init.conn_cfg_tag    = APP_BLE_CONN_CFG_TAG;
    adv_init.interval_legacy = APP_ADV_INTERVAL;
    adv_init.duration_legacy = APP_ADV_TIMEOUT_IN_SECONDS * 100;
    adv_init.interval_coded  = APP_CODED_ADV_INTERVAL;
    adv_init.fw_ver          = FW_VERSION;
    adv_init.batt_lv         = voltage;
    adv_init.temperature     = 25;
    adv_init.evt_handler     = eartag_adv_evt_handler;
    adv_init.error_handler   = eartag_adv_error_handler;
	
	
	
		sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, NULL, (int8_t) 0);

    err_code = eartag_adv_init(&m_adv, &adv_init);
    APP_ERROR_CHECK(err_code);
}




/**@brief EarTag scanning event handler. */
/*
void eartag_scan_evt_handler(eartag_scan_evt_t const * scan_evt)
{
    uint32_t err_code;
    char     buff[EARTAG_SCAN_UDID_LEN + 1];

    if (scan_evt->type == EARTAG_SCAN_EVT_RESULTS)
    {
        NRF_LOG_INFO("EARTAG_SCAN_EVT_RESULTS");

        memset(buff, 0, sizeof(buff));
        for (uint16_t i = 0; i < scan_evt->data.results.size; i++)
        {
            memcpy(m_udid_candidates[i].data, scan_evt->data.results.data[i].udid.data, EARTAG_SCAN_UDID_LEN);

            // Display.
            memcpy(buff, scan_evt->data.results.data[i].udid.data, EARTAG_SCAN_UDID_LEN);
            NRF_LOG_INFO("UDID=%s, rssi=%ddBm", (uint32_t)buff, scan_evt->data.results.data[i].rssi);
            NRF_LOG_FLUSH();
        }

        if (scan_evt->data.results.size != 0)
        {
            // Put to database.
            err_code = eartag_file_db_put(&m_file, m_udid_candidates, scan_evt->data.results.size);
            NRF_LOG_INFO("eartag_file_db_put(), err_code=0x%08x", err_code);
            if (err_code != NRF_ERROR_NO_MEM)
            {
                APP_ERROR_CHECK(err_code);
            }

            if (m_cnt == WRITE_PERIOD - 1)
            {
                err_code = eartag_file_db_write(&m_file);
                NRF_LOG_INFO("eartag_file_db_write(), err_code=0x%08x", err_code);
                APP_ERROR_CHECK(err_code);
            }

            m_cnt = (m_cnt + 1) % WRITE_PERIOD;
        }
    }
}
*/
/**@brief EarTag scanning error handler. */
void eartag_scan_error_handler (uint32_t err_code)
{
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for setting up scanner. */
/*
static void scan_init(void)
{
    uint32_t err_code;
    eartag_scan_init_t scan_init;

    memset(&scan_init, 0, sizeof(eartag_scan_init_t));

    scan_init.scan_period_ticks   = APP_TIMER_TICKS(180000);    // 3 minute scanning period
    scan_init.scan_duration_ticks = APP_TIMER_TICKS(3000);      // 3s scanning duration
    scan_init.evt_handler         = eartag_scan_evt_handler;
    scan_init.error_handler       = eartag_scan_error_handler;

    err_code = eartag_scan_init(&m_scan, &scan_init);
    APP_ERROR_CHECK(err_code);
}

*/
/**@brief EarTag file event handler. */
/*
void eartag_file_evt_handler(eartag_file_evt_t file_evt)
{
    uint32_t err_code;
    eartag_file_cfg_t *p_cfg;

    switch (file_evt)
    {
        case EARTAG_FILE_EVT_INIT:
            NRF_LOG_INFO("EARTAG_FILE_EVT_INIT");

            err_code = eartag_file_get_config(&m_file, &p_cfg);
            NRF_LOG_INFO("eartag_file_get_config(), err_code=0x%08x, mode=0x%02x", err_code, p_cfg->mode);
            APP_ERROR_CHECK(err_code);

            if (p_cfg->mode == EARTAG_FILE_MODE_HERITAGE)
            {
                err_code = bsp_indication_set(BSP_INDICATE_USER_STATE_1);
                APP_ERROR_CHECK(err_code);

                // Start scanning.
                m_cnt = 0;
                err_code = eartag_scan_start(&m_scan);
                NRF_LOG_INFO("eartag_scan_start(), err_code=0x%08x", err_code);
                APP_ERROR_CHECK(err_code);
            }
            break;

        case EARTAG_FILE_EVT_WRITE:
            NRF_LOG_INFO("EARTAG_FILE_EVT_WRITE");

            if (m_reset_pending)
            {
                (void) sd_nvic_SystemReset();
            }
            break;

        case EARTAG_FILE_EVT_DELETE:
            NRF_LOG_INFO("EARTAG_FILE_EVT_DELETE");
            break;

        default:
            break;
    }

    disp_db();
}

*/
/**@brief EarTag file error handler. */
void eartag_file_error_handler (uint32_t err_code)
{
    APP_ERROR_CHECK(err_code);
}


/**@brief FDS event handler. */

/*
static void fds_evt_handler(fds_evt_t const * p_evt)
{
    uint32_t err_code;
    eartag_file_init_t file_init;

    eartag_file_on_fds_evt(&m_file, p_evt);

    switch (p_evt->id)
    {
        case FDS_EVT_INIT:
            NRF_LOG_INFO("FDS_EVT_INIT");
            memset(&file_init, 0, sizeof(eartag_file_init_t));

            file_init.evt_handler   = eartag_file_evt_handler;
            file_init.error_handler = eartag_file_error_handler;

            err_code = eartag_file_init(&m_file, &file_init);
            APP_ERROR_CHECK(err_code);
            break;

        case FDS_EVT_GC:
            NRF_LOG_INFO("FDS_EVT_GC");
            break;

        default:
            break;
    }
}

*/
/**@brief Function for setting up file system. */

/*
static void file_init(void)
{
   // uint32_t err_code;

    //err_code = fds_register(fds_evt_handler);
    //APP_ERROR_CHECK(err_code);

    //err_code = fds_init();
    //APP_ERROR_CHECK(err_code);

    // Note: initialization will continue when FDS_EVT_INIT is received.
}
*/

/**@brief Function for displaying help. */
//static void disp_help(void)
//{
//    NRF_LOG_INFO("EarTag Start!");
//    NRF_LOG_INFO("Usage:");
//    NRF_LOG_INFO("_____________________________________________________________");
//    NRF_LOG_INFO("Button 1: Start connectable advertising using legacy PHY.");
//    NRF_LOG_INFO("Button 2: Toggle Heritage (1) and Location (2) modes.");
//    NRF_LOG_INFO("Button 3: Delete Database file from file system.");
//    NRF_LOG_INFO("Button 4:");NRF_LOG_FLUSH();
//    NRF_LOG_INFO("    - Change battery level and temperature in advertising data.");
//    NRF_LOG_INFO("    - Inject database having 2000 elements.");
//    NRF_LOG_INFO("_____________________________________________________________");
//    NRF_LOG_FLUSH();
//}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds)
{
    bsp_event_t startup_event;

    //uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS, bsp_event_handler);
    //APP_ERROR_CHECK(err_code);

    //err_code = bsp_btn_ble_init(NULL, &startup_event);
    //APP_ERROR_CHECK(err_code);

    *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}


/**@brief Function for initializing the nrf log module.
 */
//static void log_init(void)
//{
//    ret_code_t err_code = NRF_LOG_INIT(NULL);
//    APP_ERROR_CHECK(err_code);

//    NRF_LOG_DEFAULT_BACKENDS_INIT();
//}


/**@brief Function for placing the application in low power state while waiting for events.
 */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}


/**@brief Application main function.
 */

//NRF_POWER_Type NRF_POWER_Type_t;

/* PA LNA config */
/*
void configPA(void){
	
	
	ble_opt_t pa_lna_opt = {
		.common_opt = {
			.pa_lna = {
				.pa_cfg = {
					.enable = 1,
					.active_high = 1,
					.gpio_pin = 31
				},
				.lna_cfg = {
					.enable = 1,
					.active_high = 1,
					.gpio_pin = 34
				},
				.ppi_ch_id_set = m_ppi_channel
				//.ppi_ch_id_clr = APP_AMP_PPI_CH_ID_CLR,
				//.gpiote_ch_id = APP_AMP_GPIOTE_CH_ID
			}
		}
	};
	
	
	sd_ble_opt_set (BLE_COMMON_OPT_PA_LNA, &pa_lna_opt);
	
	
	
	
}
*/

void pa_lna_init(uint32_t gpio_pa_pin, uint32_t gpio_lna_pin)
{
    ble_opt_t opt;
    uint32_t gpiote_ch = NULL;
    ret_code_t err_code;        

    memset(&opt, 0, sizeof(ble_opt_t));
    
    err_code = nrf_drv_gpiote_init();
    if(err_code != NRF_ERROR_INVALID_STATE)
        APP_ERROR_CHECK(err_code);
    
    err_code = nrf_drv_ppi_init();
    if(err_code != NRF_ERROR_MODULE_ALREADY_INITIALIZED)
        APP_ERROR_CHECK(err_code);
    
    //nrf_ppi_channel_t ppi_set_ch;
    //nrf_ppi_channel_t ppi_clr_ch;
   
    err_code = nrf_drv_ppi_channel_alloc(&ppi_set_ch);
    APP_ERROR_CHECK(err_code);
    //NRF_LOG_INFO("channel set error %d", err_code);
    err_code = nrf_drv_ppi_channel_alloc(&ppi_clr_ch);
    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_out_config_t config = GPIOTE_CONFIG_OUT_TASK_TOGGLE(false);
    
    if((gpio_pa_pin == NULL) && (gpio_lna_pin == NULL))
    {
        err_code = NRF_ERROR_INVALID_PARAM;
        APP_ERROR_CHECK(err_code);
    }    

    if(gpio_pa_pin != NULL)
    {
        if(gpiote_ch == NULL)
        {
            err_code = nrf_drv_gpiote_out_init(gpio_pa_pin, &config);
            APP_ERROR_CHECK(err_code);
            
            gpiote_ch = nrf_drv_gpiote_out_task_addr_get(gpio_pa_pin); 
        }
        
        // PA config
        opt.common_opt.pa_lna.pa_cfg.active_high = 1;   // Set the pin to be active high
        opt.common_opt.pa_lna.pa_cfg.enable      = 1;   // Enable toggling
        opt.common_opt.pa_lna.pa_cfg.gpio_pin    = gpio_pa_pin; // The GPIO pin to toggle tx  
    }
    
    if(gpio_lna_pin != NULL)
    {
        if(gpiote_ch == NULL)
        {
            err_code = nrf_drv_gpiote_out_init(gpio_lna_pin, &config);
            APP_ERROR_CHECK(err_code);        
            
            gpiote_ch = nrf_drv_gpiote_out_task_addr_get(gpio_lna_pin); 
        }
        
        // LNA config
        opt.common_opt.pa_lna.lna_cfg.active_high  = 1; // Set the pin to be active high
        opt.common_opt.pa_lna.lna_cfg.enable       = 1; // Enable toggling
        opt.common_opt.pa_lna.lna_cfg.gpio_pin     = gpio_lna_pin;  // The GPIO pin to toggle rx
    }

    // Common PA/LNA config
		opt.common_opt.pa_lna.pa_cfg.enable = 0;
    opt.common_opt.pa_lna.gpiote_ch_id  = (gpiote_ch - NRF_GPIOTE_BASE) >> 2;   // GPIOTE channel used for radio pin toggling
    opt.common_opt.pa_lna.ppi_ch_id_clr = ppi_clr_ch;   // PPI channel used for radio pin clearing
    opt.common_opt.pa_lna.ppi_ch_id_set = ppi_set_ch;   // PPI channel used for radio pin setting
    
    err_code = sd_ble_opt_set(BLE_COMMON_OPT_PA_LNA, &opt);
		//NRF_LOG_INFO("here error is %d", err_code); //error is always 7
    APP_ERROR_CHECK(err_code);    
}

/*
static void pa_lna_assist(uint32_t gpio_pa_pin, uint32_t gpio_lna_pin)
{
    ret_code_t err_code;

    static const uint32_t gpio_toggle_ch = 0;
    static const uint32_t ppi_set_ch = 0;
    static const uint32_t ppi_clr_ch = 1;
    
    // Configure SoftDevice PA/LNA assist
    ble_opt_t opt;
    memset(&opt, 0, sizeof(ble_opt_t));
		
    // Common PA/LNA config
    opt.common_opt.pa_lna.gpiote_ch_id  = gpio_toggle_ch;        // GPIOTE channel
    opt.common_opt.pa_lna.ppi_ch_id_clr = ppi_clr_ch;            // PPI channel for pin clearing
    opt.common_opt.pa_lna.ppi_ch_id_set = ppi_set_ch;            // PPI channel for pin setting
		
    // PA config
    opt.common_opt.pa_lna.pa_cfg.active_high = 1;                // Set the pin to be active high
    opt.common_opt.pa_lna.pa_cfg.enable      = 1;                // Enable toggling
    opt.common_opt.pa_lna.pa_cfg.gpio_pin    = gpio_pa_pin;      // The GPIO pin to toggle
		
    // LNA config
    opt.common_opt.pa_lna.lna_cfg.active_high  = 1;              // Set the pin to be active high
    opt.common_opt.pa_lna.lna_cfg.enable       = 0;              // Enable toggling
    opt.common_opt.pa_lna.lna_cfg.gpio_pin     = gpio_lna_pin;   // The GPIO pin to toggle

    err_code = sd_ble_opt_set(BLE_COMMON_OPT_PA_LNA, &opt);
		NRF_LOG_INFO("here error is %d", err_code);
    APP_ERROR_CHECK(err_code);
		
}

*/


/**@brief Function for setting up test-GPIOs.
 */
 

void sky66114_ctrl(void)
{
    NRF_GPIOTE->CONFIG[6] = 0x00031F03;     // CTX: GPIOTE channnel #6, TASK mode, GPIO 31 (P0.31), TOGGLE, default low
    NRF_GPIOTE->CONFIG[7] = 0x00022403;     // CRX: GPIOTE channnel #7, TASK mode, GPIO 34 (P1.04), TOGGLE, default low
		//NRF_GPIOTE->CONFIG[8] = 0x00022403;     // CRX: GPIOTE channnel #7, TASK mode, GPIO 34 (P1.04), TOGGLE, default low
		
	
    NRF_PPI->CH[14].EEP = (uint32_t)&NRF_RADIO->EVENTS_TXREADY;     // PPI channel #14, EEP <= RADIO.EVENTS_TXREADY
    NRF_PPI->CH[14].TEP = (uint32_t)&NRF_GPIOTE->TASKS_SET[6];      // PPI channel #14, TEP <= GPIOTE.TASKS_SET[6]

    NRF_PPI->CH[15].EEP = (uint32_t)&NRF_RADIO->EVENTS_RXREADY;     // PPI channel #15, EEP <= RADIO.EVENTS_RXREADY
    NRF_PPI->CH[15].TEP = (uint32_t)&NRF_GPIOTE->TASKS_SET[7];      // PPI channel #15, TEP <= GPIOTE.TASKS_SET[7]

    NRF_PPI->CH[16].EEP = (uint32_t)&NRF_RADIO->EVENTS_DISABLED;    // PPI channel #16, EEP <= RADIO.EVENTS_DISABLED
    NRF_PPI->CH[16].TEP = (uint32_t)&NRF_GPIOTE->TASKS_CLR[6];      // PPI channel #16, TEP <= GPIOTE.TASKS_CLR[6]
    NRF_PPI->FORK[16].TEP = (uint32_t)&NRF_GPIOTE->TASKS_CLR[7];    // PPI channel #16: FORK.TEP <= GPIOTE.TASKS_CLR[7]

    NRF_PPI->CHENSET = 0x0001C000;                                  // Enable PPI channel #14, 15 & 16
}


/**@brief Function for initializing Radio Notification Software Interrupts.
 */
uint32_t radio_notification_init(uint32_t irq_priority, uint8_t notification_type, uint8_t notification_distance)
{
    uint32_t err_code;

    err_code = sd_nvic_ClearPendingIRQ(SWI1_IRQn);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = sd_nvic_SetPriority(SWI1_IRQn, irq_priority);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = sd_nvic_EnableIRQ(SWI1_IRQn);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Configure the event
    return sd_radio_notification_cfg_set(notification_type, notification_distance);
}


/**@brief Software interrupt 1 IRQ Handler, handles radio notification interrupts.
 */
void SWI1_IRQHandler(bool radio_evt)
{
    if (radio_evt)
    {
       //sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, 0, 4);
				//nrf_gpio_pin_toggle(12);
				//NRF_LOG_INFO("radio notification \r\n");
				
				//if(!txcount)
				//{
					
					 
					//nrf_ppi_channel_disable(ppi_set_ch);
					//nrf_ppi_channel_disable(ppi_clr_ch);
					//nrf_drv_saadc_uninit();
				//	NRF_SAADC->INTENCLR = (SAADC_INTENCLR_END_Clear << SAADC_INTENCLR_END_Pos);
				//	NVIC_ClearPendingIRQ(SAADC_IRQn);
				//}
				
				 txcount ++;
			  //nrf_drv_saadc_sample_task_get();
				
				//NRF_LOG_INFO("erroro on %d", err_code);
			  //nrf_drv_saadc_sample_task_get();
			
				//nrf_gpio_pin_toggle(31); //31 on tag, 45 on collar Toggle amp on / off
				//nrf_gpio_pin_toggle(2); //2 on tag and 47 on collar Toggle the status of the LED on each radio notification event
				if (txcount == 2){
					nrf_gpio_pin_clear(2);
					//sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, NULL, (int8_t) 4);
					//nrf_gpio_pin_toggle(2); //2 on tag and 47 on collar Toggle the status of the LED on each radio notification event
					//saadc_init();
					//nrf_drv_saadc_init(NULL, saadc_callback);
					//nrf_ppi_channel_enable(ppi_set_ch);
					//nrf_ppi_channel_enable(ppi_clr_ch);
					
				}
				
				if (txcount > 2){
					//saadc_init();
					//NRF_LOG_INFO("sample ADC it is %d \r\n", nrf_saadc_enable_check());
					//ret_code_t err_code = nrf_drv_saadc_sample();
					nrf_gpio_pin_set(2);
					//sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, NULL, (int8_t) 4);
					txcount = 0;
				}
			 
			//	NRF_LOG_INFO("toggle %d", txcount);
			
			
    }
}

NRF_POWER_Type NRF_POWER_Type_t;

int main(void)
{
    uint32_t err_code;
    bool     erase_bonds;
	
	//POWER MEASURE
		//nrf_gpio_cfg_output(26);
		//nrf_gpio_pin_clear(26);
	
	  //sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);
	 NRF_POWER_Type_t.DCDCEN = POWER_DCDCEN_DCDCEN_Enabled;
		
	
	
		/*	
		void nrf_gpio_cfg	(	uint32_t 	4, nrf_gpio_pin_dir_t 	GPIO_PIN_CNF_DIR_Input,
				nrf_gpio_pin_input_t 	GPIO_PIN_CNF_INPUT_Connect,
				nrf_gpio_pin_pull_t 	GPIO_PIN_CNF_PULL_Disabled,
				nrf_gpio_pin_drive_t 	GPIO_PIN_CNF_DRIVE_S0D1,
				nrf_gpio_pin_sense_t 	GPIO_PIN_CNF_SENSE_Disabled 
			);
	
	*/
	/*
		NRF_GPIO->PIN_CNF[4] = (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos)		
                             | (GPIO_PIN_CNF_DRIVE_S0D1 << GPIO_PIN_CNF_DRIVE_Pos)
														 | (GPIO_PIN_CNF_PULL_Disabled << GPIO_PIN_CNF_PULL_Pos)
														 | (GPIO_PIN_CNF_INPUT_Disconnect << GPIO_PIN_CNF_INPUT_Pos)
														 | (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos);
	
		nrf_gpio_pin_set(4);
		nrf_gpio_pin_set(4);
	
		//SUPERCAP_VOLTAGE_ON_OPEN();
		SUPERCAP_LOW();
		//SUPERCAP_VOLTAGE_MEASURE_OPEN();
*/
    // Initialize.
    err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
	
		//LED
	//	nrf_gpio_cfg_output(12);
	//	nrf_gpio_pin_clear(12);
		//nrf_delay_ms(100);


		
		//lfclk_config();

    //rtc_config();

    //log_init();
			//CPS
		nrf_gpio_cfg_output(2); //2 on tag and 47 on collar
    nrf_gpio_pin_clear(2);  //clear = low pwer mode, set = bypass mode
		
	  //CRX  //if 1 then 32 + pin number
		nrf_gpio_cfg_output(34);//34 on tag and 36 on collar
    nrf_gpio_pin_clear(34);
		
		//CTX
		nrf_gpio_cfg_output(31);//31 on tag and 45 on collar
    nrf_gpio_pin_clear(31);
		
		
	//	nrf_gpio_cfg_output(30);//31 on tag and 45 on collar // 30 on beacon
  //  nrf_gpio_pin_clear(30);
		
	
		// Init GPIO's to control PA and/or LNA, must be done while radio is inactive.
     
		
	  //

    buttons_leds_init(&erase_bonds);
		
    ble_stack_init();
		sd_power_mode_set(NRF_POWER_MODE_LOWPWR);
		//pa_lna_init(31,30);
		sky66114_ctrl();
		//pa_lna_assist(31,30);
		err_code = radio_notification_init(3, NRF_RADIO_NOTIFICATION_TYPE_INT_ON_INACTIVE, NRF_RADIO_NOTIFICATION_DISTANCE_5500US);
		APP_ERROR_CHECK(err_code);
    gap_params_init();
    gatt_init();
		
		//saadc
		//saadc_init();
		//err_code = nrf_drv_saadc_sample();
		//NRF_LOG_INFO("saadc error 100");
		//nrf_drv_saadc_sample_task_get();
    //saadc_sampling_event_init();
		//NRF_LOG_INFO("saadc1");
    //saadc_sampling_event_enable();
		//NRF_LOG_INFO("saadc2");
		//pa_lna_assist(31,34);
		//pa_lna_init(31,34);
    services_init();
    conn_params_init();
    advertising_init();
		
		
   // scan_init();
    //file_init();
		//sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);
	  //NRF_POWER_Type_t.DCDCEN = POWER_DCDCEN_DCDCEN_Enabled;
		
		
		//logic control for the amplifier
		
	
		
		
    //disp_help();
		

    /* Start advertising. */
    err_code = eartag_adv_start_coded_nonconnectable(&m_adv);
    APP_ERROR_CHECK(err_code);
		//NRF_LOG_INFO("no error yet");
		//nrf_gpio_pin_set(12);
		
		/*
		eartag_file_cfg_t * p_cfg;
		err_code = eartag_file_get_config(&m_file, &p_cfg);
    NRF_LOG_INFO("eartag_file_get_config(), err_code=0x%08x, mode=0x%02x", err_code, p_cfg->mode);
    APP_ERROR_CHECK(err_code);
		 p_cfg->mode = (p_cfg->mode == EARTAG_FILE_MODE_LOCATION)
                        ? EARTAG_FILE_MODE_LOCATION: EARTAG_FILE_MODE_LOCATION;

            err_code = eartag_file_config_write(&m_file);
            NRF_LOG_INFO("eartag_file_config_write(), err_code=0x%08x, mode=0x%02x", err_code, p_cfg->mode);
            APP_ERROR_CHECK(err_code);
*/

    // Enter main loop.
    for (;;)
    {
        //UNUSED_RETURN_VALUE(NRF_LOG_PROCESS());
        power_manage();
    }
}


/**
 * @}
 */
