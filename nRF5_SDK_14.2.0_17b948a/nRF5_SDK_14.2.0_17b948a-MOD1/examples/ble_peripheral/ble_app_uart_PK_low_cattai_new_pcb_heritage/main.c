/**
 * Copyright (c) 2014 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_nus_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */



#include <stdint.h>
#include <string.h>
#include <math.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "app_timer.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"

#include "nrf_drv_rtc.h"
#include "nrf_drv_clock.h"
#include "nrf_delay.h"
#include "ble_nus.h"
#include "app_util_platform.h"

#include "NRF_LOG.h"
#include "NRF_LOG_ctrl.h"
#include "NRF_LOG_default_backends.h"
#include "fds.h"
#include "eartag_adv.h"
#include "eartag_scan.h"
#include "eartag_file.h"
#include "nrf_nvic.h"
#include "nrf_drv_gpiote.h"
#include "app_error.h"
#include "ble.h"

#include "nrf_drv_twi.h"


#define SUPERCAP_VOLTAGE_ON_PIN_NUMBER 26
#define SUPERCAP_VOLTAGE_MEASURE_PIN_NUMBER  4
#define SUPERCAP_LOW() (NRF_P0->OUTCLR = (1 << SUPERCAP_VOLTAGE_ON_PIN_NUMBER)) // set ON to low
#define SUPERCAP_VOLTAGE_ON_OPEN() (NRF_P0->OUTSET = (1 << SUPERCAP_VOLTAGE_ON_PIN_NUMBER)) // set ON to open-drain
#define SUPERCAP_VOLTAGE_MEASURE_OPEN() (NRF_P0->OUTSET = (1 << SUPERCAP_VOLTAGE_MEASURE_PIN_NUMBER)) // set MEASURE to open-drain

#define APP_BLE_CONN_CFG_TAG            1                                           /**< A tag identifying the SoftDevice BLE configuration. */

#define APP_FEATURE_NOT_SUPPORTED       BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2        /**< Reply when unsupported features are requested. */

#define DEVICE_NAME                     "EarTag"                                    /**< Name of device. Will be included in the advertising data. */
#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

#define COMPARE_COUNTERTIME  (1UL)                                        /**< Get Compare event COMPARE_TIME seconds after the counter starts from 0. */


#define APP_BLE_OBSERVER_PRIO           3                                           /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      10                                          /**< The advertising timeout (in units of seconds). */

#define APP_CODED_ADV_INTERVAL          MSEC_TO_UNITS(3500, UNIT_0_625_MS)          /**< The advertising (coded PHY) interval (in units of 0.625 ms. This value corresponds to 40 ms). */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */

#define FW_VERSION                      0x01                                        /**< Firmware version. */
#define WRITE_PERIOD                    8                                           /**< For every 8 EARTAG_SCAN_EVT_RESULTS, write to file. */
//eartag
#define LED_OUT													12
//button
//#define LED_OUT													13
#define CPS_PIN													2																						/**< 2 on tag and 47 on collar clear = low pwer mode, set = bypass mode */
#define MAX_TOP_PAIRS_SENT							20																					/**< Number of total pairs sent between scans */

static volatile uint8_t totalPairs;																										/**< keeping track of which pair has been transmitted */
static uint8_t udidcountarr[MAX_TOP_PAIRS_SENT];																						/**< array of count of the top 20 matched pairs */
static uint16_t udidarr[MAX_TOP_PAIRS_SENT][EARTAG_SCAN_UDID_LEN +1];																/**< array of udids of top 20 matched pairs */

BLE_NUS_DEF(m_nus);                                                                 /**< BLE NUS service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                           /**< GATT module instance. */
EARTAG_ADV_DEF(m_adv);                                                              /**< Eartag Advertising module instance. */
EARTAG_SCAN_DEF(m_scan);                                                            /**< Eartag Scanning module instance. */
EARTAG_FILE_DEF(m_file);                                                            /**< Eartag file module instance. */

static uint16_t   m_conn_handle          = BLE_CONN_HANDLE_INVALID;                 /**< Handle of the current connection. */
static uint16_t   m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3;            /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
static ble_uuid_t m_adv_uuids[]          =                                          /**< Universally unique service identifier. */
{
    {BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}
};

static volatile uint8_t battcount = 135;
static bool is_scanning = true;
static bool is_fw_packet;
static bool long_sent = false;
static bool returning_from_low_power = false;


static eartag_udid_t m_udid_candidates[EARTAG_SCAN_MAX_NUM_OF_RESULTS];             /**< UDID candidates, to be put to database. */
static uint8_t       m_cnt = 0;                                                     /**< Test counter. */
static bool          m_reset_pending = false;                                       /**< Flag which tells whether reset has been pending. */



/* ADC sampling setup */
#define ADC_REF_VOLTAGE_IN_MILLIVOLTS       600                                          /**< Reference voltage (in milli volts) used by ADC while doing conversion. */
#define ADC_PRE_SCALING_COMPENSATION        6                                            /**< The ADC is configured to use VDD with 1/3 prescaling as input. And hence the result of conversion is to be multiplied by 3 to get the actual value of the battery voltage.*/
#define DIODE_FWD_VOLT_DROP_MILLIVOLTS      270                                          /**< Typical forward voltage drop of the diode (Part no: SD103ATW-7-F) that is connected in series with the voltage supply. This is the voltage drop when the forward current is 1mA. Source: Data sheet of 'SURFACE MOUNT SCHOTTKY BARRIER DIODE ARRAY' available at www.diodes.com. */
#define ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS   1200                                         /**< Value in millivolts for voltage used as reference in ADC conversion on NRF51. */
#define ADC_INPUT_PRESCALER                 3                                            /**< Input prescaler for ADC convestion on NRF51. */
#define ADC_RES_10BIT                       1024                                         /**< Maximum digital value for 10-bit ADC conversion. */
//eartage
#define saadc_source												NRF_SAADC_INPUT_AIN2
//button
//#define saadc_source												NRF_SAADC_INPUT_DISABLED

#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE)\
        ((((ADC_VALUE) * ADC_REF_VBG_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_INPUT_PRESCALER)



#define SAMPLES_IN_BUFFER 1
volatile uint8_t state = 1;
static const nrf_drv_rtc_t     rtc = NRF_DRV_RTC_INSTANCE(2);

static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];


static volatile uint8_t voltage;
static volatile uint8_t temperature;

static volatile uint8_t long_packet_version;

NRF_POWER_Type NRF_POWER_Type_t;


volatile uint8_t txcount = 0;
volatile uint8_t paircount = 0;


/* TWI instance ID. */
#define TWI_INSTANCE_ID     1

//TWI device detected at address 0x19.
/* Common addresses definition for accel sensor. */
#define ACCEL_ADDR          0x19U
#define PIN_IN							22
//eartag
#define SCL_PIN							32
#define SDA_PIN							24
//button
//#define SCL_PIN							13
//#define SDA_PIN							15
/* Indicates if operation on TWI has ended. */
static volatile bool m_xfer_done = false;

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

/* Buffer for samples read from temperature sensor. */
static uint8_t m_sample[191];

static int8_t totals_x[96];
static int8_t totals_y[96];
static int8_t totals_z[96];


static volatile uint8_t accelcounter;


/**
 * @brief Function for setting active mode on MMA7660 accelerometer.
 */
 /**
 * @brief Function for setting active mode on MMA7660 accelerometer.
 */
void ACCEL_set_mode(void)
{
    ret_code_t err_code;
		m_xfer_done = false;
    
    uint8_t reg[2] = {0x20, 0x2F}; //10Hz Low power mode, all axis enabled
		

    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);

		
    while (m_xfer_done == false);
		
		m_xfer_done = false;

	
    uint8_t regs[2] = {0x21, 0x08}; //high pass filter enabled 08
		

    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, regs, sizeof(regs), false);
    APP_ERROR_CHECK(err_code);

		
    while (m_xfer_done == false);
		
		
		m_xfer_done = false;
   
    uint8_t reg2[2] = {0x23, 0x80}; //080 BDU enabled
	
		
    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg2, sizeof(reg2), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		
		
		m_xfer_done = false;
  
    uint8_t reg3a[2] = {0x24, 0x00}; // resetting fifo
	

    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg3a, sizeof(reg3a), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		
		m_xfer_done = false;
   
    uint8_t reg3[2] = {0x24, 0x40}; // fifo 40
	
		

    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg3, sizeof(reg3), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
	
		m_xfer_done = false;

	
    uint8_t reg4[2] = {0x2E, 0x80}; // stream mode 80
		

    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg4, sizeof(reg4), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		
		
		
		m_xfer_done = false;

	
    uint8_t reg5[2] = {0x1E, 0x90}; //disable pullup
		

    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg5, sizeof(reg5), false);
    APP_ERROR_CHECK(err_code);
    while (m_xfer_done == false);
		
		
}

void power_down_accel()
{
	
		ret_code_t err_code;
		m_xfer_done = false;
    
    uint8_t reg[2] = {0x20, 0x00}; //power down, all axis disabled
		

    err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);

		
    while (m_xfer_done == false);
	
	
}

/**
 * @brief Calculate the time of acceleration direction change
 *
 */

uint8_t get_accel_count(int8_t accisData[])
{
	
	uint8_t accelCount = 0;
	static bool isBigger;

	for(int i = 0; i< 90; i++)
	{
		if((accisData[i] > (accisData[i+1] +1)) && !isBigger)
		{
			
			isBigger = true;
			accelCount++;
			
		}else if((accisData[i] < (accisData[i+1] - 1)) && isBigger){
			
			
			isBigger = false;
			accelCount++;
			
			
		}
		
	}
	
	  if(!accelCount) accelCount = 1;
		//NRF_LOG_INFO("count is %d", accelCount);
		return accelCount;
	
	
}


/**
 * @brief Find average peak value of given accis
 *
 */

uint8_t find_peak_for_accis(int8_t accisVals[])
{

 int i, j, temp;
	

	
 for (i = 0; i < 90; ++i)
 {
      for (j = 0; j < 90 - i; ++j )
      {
           if (accisVals[j] < accisVals[j+1])
           {
                temp = accisVals[j+1];
                accisVals[j+1] = accisVals[j];
                accisVals[j] = temp;
           }
      }
 }
 
 
 
 
 uint8_t returnVals = (abs(accisVals[89]) + accisVals[0] + abs(accisVals[88]) + accisVals[1] + abs(accisVals[87]) + accisVals[2])/3;
 
	//NRF_LOG_INFO("returnvals is %d", returnVals);
 
	
	return returnVals;
	
	
	
}


void prepare_long_transmission_data()
{
	


	if(!is_scanning)
	{
	
		uint8_t totalCountX = get_accel_count(totals_x);
		uint8_t peakx = find_peak_for_accis(totals_x);
		uint8_t totalCountY = get_accel_count(totals_y);
		uint8_t peaky = find_peak_for_accis(totals_y);
		uint8_t totalCountZ = get_accel_count(totals_z);
		uint8_t peakz = find_peak_for_accis(totals_z);
		

		
	 nrf_gpio_pin_clear(CPS_PIN);

	 if(paircount < totalPairs){


		char pair0[] = {udidarr[paircount][0], (udidarr[paircount][0] >> 8)};
		char pair1[] = {udidarr[paircount][1], (udidarr[paircount][1] >> 8)};
		char pair2[] = {udidarr[paircount][2], (udidarr[paircount][2] >> 8)};
		int paird0 = (int)strtol(pair0, NULL, 16);
		int paird1 = (int)strtol(pair1, NULL, 16);
		int paird2 = (int)strtol(pair2, NULL, 16);
	
		
		eartag_adv_change_long_pair_type(&m_adv, paird0, paird1, paird2, 0x03, udidcountarr[paircount], (char)totalCountX, (char)totalCountY, (char)totalCountZ, (char)peakx, (char)peaky, (char)peakz);
		
		paircount++;
	
	 }else{

			long_packet_version ++;
		
			if(long_packet_version == 1){
	
				eartag_adv_change_long_type(&m_adv, 0x07, voltage, totalCountX, totalCountY, totalCountZ, peakx, peaky, peakz);
			
			
			}else if(long_packet_version == 2){
				
				
			
				eartag_adv_change_long_type(&m_adv, 0x05, temperature, totalCountX, totalCountY, totalCountZ, peakx, peaky, peakz);
			
			
			
			}else{
			
				eartag_adv_change_long_type(&m_adv, 0x06, FW_VERSION, totalCountX, totalCountY, totalCountZ, peakx, peaky, peakz);
			
				long_packet_version = 0;
			
			
			}
	

	
		}/* end if is not pair type */


	}/* end if is-scanning */
	
}

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
  
	
		switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
                
								for(int j = 0; j < 180; j+=6)
							{
								
									int x = (((int8_t)m_sample[j+1])*256+m_sample[j+0])*0.004; //;0.061; //return values 15 is if high pass filter is set up
									int y = (((int8_t)m_sample[j+3])*256+m_sample[j+2])*0.004;
									int z = (((int8_t)m_sample[j+5])*256+m_sample[j+4])*0.004;
									
									totals_x[accelcounter] = x;
									totals_y[accelcounter] = y;
									totals_z[accelcounter] = z;
									
																			
									accelcounter++;				
								
							}
							long_sent = false;
							if(accelcounter > 80) accelcounter = 0;	
							
							memset(m_sample, 0, sizeof(m_sample));

            }
            m_xfer_done = true;
            break;
        default:
            break;
    }
}

/**
 * @brief TWI initialization.
 */
void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_lm75b_config = {
       .scl                = SCL_PIN,	// 13,//32,
       .sda                = SDA_PIN,	//15,//24,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_lm75b_config, twi_handler, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}


/**
 * @brief Function for reading data from temperature sensor.
 */
static void read_sensor_data()
{
    m_xfer_done = false;

    /* Read data from SHTC1 address */
    ret_code_t err_code = nrf_drv_twi_rx(&m_twi, ACCEL_ADDR, m_sample, sizeof(m_sample));
    APP_ERROR_CHECK(err_code);
		////NRF_LOG_INFO("read sensor error code %d", err_code);
}

/**
 * @brief Function for reading data from temperature sensor.
 */
static void start_sensor()
{
    m_xfer_done = false;
		/* Write to the SHTC1 address */

		uint8_t reg[1] = {(0x80 | 0x28)}; //28

    ret_code_t err_code = nrf_drv_twi_tx(&m_twi, ACCEL_ADDR, reg, sizeof(reg), false);
    APP_ERROR_CHECK(err_code);
		
		
}



/**@brief Functions for analogue - digital conversion
 *
 * @details These functions are required to measure battery voltage
 */

void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
    
		if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);

        int i;
       
				int average_voltage = 0;

        for (i = 0; i < SAMPLES_IN_BUFFER; i++)
        {
           
						average_voltage += ADC_RESULT_IN_MILLI_VOLTS(p_event->data.done.p_buffer[i]) +
                                  DIODE_FWD_VOLT_DROP_MILLIVOLTS;
					  

        }
				
				average_voltage = average_voltage / SAMPLES_IN_BUFFER;
				voltage = round(average_voltage * 10 / 569);
			
		
				nrf_drv_saadc_uninit();
				nrf_gpio_pin_set(26);
    }
}


void saadc_init(void)
{
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(saadc_source);//NRF_SAADC_INPUT_AIN2 - EARTAG
		channel_config.gain = NRF_SAADC_GAIN1_3;

    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
}



/** @brief: Function for handling the RTC0 interrupts.
 * Triggered on TICK and COMPARE0 match.
 */


static void rtc_handler(nrf_drv_rtc_int_type_t int_type)
{
    if (int_type == NRF_DRV_RTC_INT_COMPARE0)
    {

				nrf_drv_rtc_counter_clear(&rtc);
				if(!is_scanning)
				{
					
					//change manu data each time and restart adverts
					txcount++;
					battcount++;
					if(voltage && voltage < 22){
						
							nrf_gpio_pin_set(CPS_PIN);
							
							if(!(txcount %2)) eartag_adv_change_short_low_power_type(&m_adv, 0x01, voltage);
						
							if(!returning_from_low_power)
							{
								power_down_accel();
								nrf_drv_twi_uninit(&m_twi);
								NRF_TWI1->ENABLE = 0;
								ret_code_t err_code = eartag_scan_stop(&m_scan);
                APP_ERROR_CHECK(err_code);
								returning_from_low_power = true;
							}
						
					}else{
					
				
						if(returning_from_low_power) 
						{
						
							returning_from_low_power = false;
							NRF_TWI1->ENABLE = 1;
							twi_init();
							ACCEL_set_mode();
							start_sensor();
							while (m_xfer_done == false);
							read_sensor_data();
							ret_code_t err_code = eartag_scan_start(&m_scan);
              APP_ERROR_CHECK(err_code);

							
						}
						
						if(!(txcount % 3))
						{
							
							read_sensor_data();
						
							
						}

							if(!accelcounter && !long_sent) 
							{

								prepare_long_transmission_data();
								long_sent = true;

							}else{

								nrf_gpio_pin_set(CPS_PIN);
						
								if(is_fw_packet){

									eartag_adv_change_short_type(&m_adv, 0x01, voltage);
									is_fw_packet = false;
									
								}else{
									
									eartag_adv_change_short_type(&m_adv, 0x02, FW_VERSION);

									is_fw_packet = true;
									
									
								}
							}
						}
						if(battcount == 1){
							
							static int32_t temp;          
							ret_code_t err_code = sd_temp_get(&temp);
							APP_ERROR_CHECK(err_code);
							temperature = (uint8_t)temp/4;
							//NRF_LOG_INFO("temp is %d temperature is %d", temp/4, temperature);
							
						}
						
						if(battcount > 135){
										
								nrf_gpio_pin_clear(26);
								saadc_init();
								ret_code_t err_code = nrf_drv_saadc_sample();
								battcount = 0;
					
						}
										
				}
    }

}

/** @brief Function starting the internal LFCLK XTAL oscillator.
 */

static void lfclk_config(void)
{
    ret_code_t err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_clock_lfclk_request(NULL);
}

/** @brief Function initialization and configuration of RTC driver instance.
 */
static void rtc_config(void)
{
    uint32_t err_code;

    //Initialize RTC instance
    nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
    config.prescaler = 4095;
    err_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
    APP_ERROR_CHECK(err_code);

    //Enable tick event & interrupt
    nrf_drv_rtc_tick_enable(&rtc,false);

    //Set compare channel to trigger interrupt after COMPARE_COUNTERTIME seconds
    err_code = nrf_drv_rtc_cc_set(&rtc,0,COMPARE_COUNTERTIME * 8,true);
    APP_ERROR_CHECK(err_code);

    //Power on RTC instance
    nrf_drv_rtc_enable(&rtc);
}
/** end battery measuring snippet **/

/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyse
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of
 *          the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}






/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{

}


/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}



/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:

            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break;

        case BLE_GAP_EVT_DISCONNECTED:
          //  //NRF_LOG_INFO("Disconnected");
            // LED indication will be changed when advertising starts.
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

#ifndef S140
        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
           // //NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;
#endif

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;
#if !defined (S112)
         case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST:
        {
            ble_gap_data_length_params_t dl_params;

            // Clearing the struct will effectivly set members to @ref BLE_GAP_DATA_LENGTH_AUTO
            memset(&dl_params, 0, sizeof(ble_gap_data_length_params_t));
            err_code = sd_ble_gap_data_length_update(p_ble_evt->evt.gap_evt.conn_handle, &dl_params, NULL);
            APP_ERROR_CHECK(err_code);
        } break;
#endif //!defined (S112)
        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_EVT_USER_MEM_REQUEST:
            err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        {
            ble_gatts_evt_rw_authorize_request_t  req;
            ble_gatts_rw_authorize_reply_params_t auth_reply;

            req = p_ble_evt->evt.gatts_evt.params.authorize_request;

            if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
            {
                if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
                {
                    if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
                    }
                    else
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
                    }
                    auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
                    err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                               &auth_reply);
                    APP_ERROR_CHECK(err_code);
                }
            }
        } break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);
	
		

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);
	
		

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
	

}


/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED))
    {
        m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        ////NRF_LOG_INFO("Data len is set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
   
}


/**@brief Function for initializing the GATT library. */
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, 64);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
 

void udid_sort(uint8_t a[], uint8_t b[], uint16_t array_size)
 {
	 int i, j, temp, p_temp;
	 
	 for (i = 0; i < (array_size - 1); ++i)
	 {
				for (j = 0; j < array_size - 1 - i; ++j )
				{
						 if (a[j] < a[j+1])
						 {
									//sorting count
									temp = a[j+1];
									a[j+1] = a[j];
									a[j] = temp;
									//sorting udid index
									p_temp = b[j+1];
									b[j+1] = b[j];
									b[j] = p_temp;
									
						 }
				}
	 }
}  

/**@brief Function for displaying the number of elements in the database. */

static void disp_db(void)
{
    uint32_t err_code;
    uint16_t num;
    eartag_file_db_entry_t * p_udid;

    err_code = eartag_file_get_db(&m_file, &p_udid, &num);
    APP_ERROR_CHECK(err_code);

    //NRF_LOG_INFO("# of elements in DB: %d", num);
	
		uint8_t idcountarr[num];
		uint8_t idcountparr[num];
		uint16_t idarr[num][EARTAG_SCAN_UDID_LEN +1];
		memset(udidcountarr, 0, sizeof(udidcountarr));
		memset(udidarr, 0, sizeof(udidarr));
	
		for(int i = 0; i < num; i++)
		{
			memcpy(&idcountarr[i], &p_udid[i].count, 3);
			idcountparr[i]=i;
			memcpy(&idarr[i], &p_udid[i].udid, EARTAG_SCAN_UDID_LEN);
			////NRF_LOG_INFO("is %s same as %s\n", &p_udid[i].udid, &idarr[i]);
		}
			
		
		udid_sort((uint8_t *)&idcountarr, (uint8_t *)&idcountparr, num);
		
		if (num > MAX_TOP_PAIRS_SENT) num = MAX_TOP_PAIRS_SENT;
		
		for(int i = 0; i<num; i++)
		{
			
			memcpy(&udidarr[i], &idarr[idcountparr[i]], EARTAG_SCAN_UDID_LEN);
			memcpy(&udidcountarr[i], &idcountarr[i], 3);
			
			
		}
		
		totalPairs = num;
		
}


/**@brief EarTag advertising event handler. */
void eartag_adv_evt_handler(eartag_adv_evt_t const adv_evt)
{
    switch (adv_evt)
    {
        case EARTAG_ADV_EVT_CODED_NONCONN_ADV:
            //NRF_LOG_INFO("EARTAG_ADV_EVT_CODED_NONCONN_ADV");
            break;

        case EARTAG_ADV_EVT_LEGACY_ADV:
            //NRF_LOG_INFO("EARTAG_ADV_EVT_LEGACY_ADV");
            break;

        case EARTAG_ADV_EVT_STOPPED:
            //NRF_LOG_INFO("EARTAG_ADV_EVT_STOPPED");
            break;

        default:
            break;
    }
}

/**@brief EarTag advertising error handler. */
void eartag_adv_error_handler (uint32_t err_code)
{
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for setting up advertising data. */
static void advertising_init(void)
{
    uint32_t err_code;
    eartag_adv_init_t adv_init;

    memset(&adv_init, 0, sizeof(eartag_adv_init_t));

    adv_init.advdata_legacy.name_type               = BLE_ADVDATA_FULL_NAME;
    adv_init.advdata_legacy.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    adv_init.advdata_legacy.include_appearance      = false;
    adv_init.advdata_legacy.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    adv_init.advdata_legacy.uuids_complete.p_uuids  = m_adv_uuids;
    
    adv_init.conn_cfg_tag    = APP_BLE_CONN_CFG_TAG;
    adv_init.interval_legacy = APP_ADV_INTERVAL;
    adv_init.duration_legacy = APP_ADV_TIMEOUT_IN_SECONDS * 100;
    adv_init.interval_coded  = APP_CODED_ADV_INTERVAL;
    adv_init.fw_ver          = FW_VERSION;
    adv_init.batt_lv         = voltage;
    adv_init.temperature     = 25;
    adv_init.evt_handler     = eartag_adv_evt_handler;
    adv_init.error_handler   = eartag_adv_error_handler;


		sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, NULL, (int8_t) -12);
		
		
    err_code = eartag_adv_init(&m_adv, &adv_init);
    APP_ERROR_CHECK(err_code);
}




/**@brief EarTag scanning event handler. */

void eartag_scan_evt_handler(eartag_scan_evt_t const * scan_evt)
{
    uint32_t err_code;
    char     buff[EARTAG_SCAN_UDID_LEN + 1];

    if (scan_evt->type == EARTAG_SCAN_EVT_RESULTS)
    {
        //NRF_LOG_INFO("EARTAG_SCAN_EVT_RESULTS");
				
        memset(buff, 0, sizeof(buff));
        for (uint16_t i = 0; i < scan_evt->data.results.size; i++)
        {
            memcpy(m_udid_candidates[i].data, scan_evt->data.results.data[i].udid.data, EARTAG_SCAN_UDID_LEN);

            // Display.
            memcpy(buff, scan_evt->data.results.data[i].udid.data, EARTAG_SCAN_UDID_LEN);
            //NRF_LOG_INFO("UDID=%s, rssi=%ddBm", (uint32_t)buff, scan_evt->data.results.data[i].rssi);
            //NRF_LOG_FLUSH();
        }

        if (scan_evt->data.results.size != 0)
        {
            // Put to database.
            err_code = eartag_file_db_put(&m_file, m_udid_candidates, scan_evt->data.results.size);
       
            if (err_code != NRF_ERROR_NO_MEM)
            {
                APP_ERROR_CHECK(err_code);
            }

            if (m_cnt == WRITE_PERIOD - 1)
            {
                err_code = eartag_file_db_write(&m_file);
              
                APP_ERROR_CHECK(err_code);
            }

            m_cnt = (m_cnt + 1) % WRITE_PERIOD;
        }
    }if(scan_evt->type == EARTAG_SCAN_START)
		{
			
			nrf_gpio_pin_set(CPS_PIN);
			is_scanning = true;
			
		}if(scan_evt->type == EARTAG_SCAN_STOP)
		{
			
			
			txcount = 0;
			paircount = 0;
			is_scanning = false;
			disp_db();
			
		}
}

/**@brief EarTag scanning error handler. */
void eartag_scan_error_handler (uint32_t err_code)
{
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for setting up scanner. */

static void scan_init(void)
{
    uint32_t err_code;
    eartag_scan_init_t scan_init;

    memset(&scan_init, 0, sizeof(eartag_scan_init_t));

    scan_init.scan_period_ticks   = APP_TIMER_TICKS(210000);    // 210000 = 3.45 minute scanning period
    scan_init.scan_duration_ticks = APP_TIMER_TICKS(1000);      // 1s scanning duration
    scan_init.evt_handler         = eartag_scan_evt_handler;
    scan_init.error_handler       = eartag_scan_error_handler;

    err_code = eartag_scan_init(&m_scan, &scan_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief EarTag file event handler. */

void eartag_file_evt_handler(eartag_file_evt_t file_evt)
{
    uint32_t err_code;
    eartag_file_cfg_t *p_cfg;

    switch (file_evt)
    {
        case EARTAG_FILE_EVT_INIT:
           

            err_code = eartag_file_get_config(&m_file, &p_cfg);
           
            APP_ERROR_CHECK(err_code);

            if (p_cfg->mode == EARTAG_FILE_MODE_HERITAGE)
            {
                
                // Start scanning.
                m_cnt = 0;
                err_code = eartag_scan_start(&m_scan);
                
                APP_ERROR_CHECK(err_code);
            }
            break;

        case EARTAG_FILE_EVT_WRITE:
          

            if (m_reset_pending)
            {
                (void) sd_nvic_SystemReset();
            }
            break;

        case EARTAG_FILE_EVT_DELETE:
            
            break;

        default:
            break;
    }

    
}


/**@brief EarTag file error handler. */
void eartag_file_error_handler (uint32_t err_code)
{
    APP_ERROR_CHECK(err_code);
}


/**@brief FDS event handler. */


static void fds_evt_handler(fds_evt_t const * p_evt)
{
    uint32_t err_code;
    eartag_file_init_t file_init;

    eartag_file_on_fds_evt(&m_file, p_evt);

    switch (p_evt->id)
    {
        case FDS_EVT_INIT:
           
            memset(&file_init, 0, sizeof(eartag_file_init_t));

            file_init.evt_handler   = eartag_file_evt_handler;
            file_init.error_handler = eartag_file_error_handler;

            err_code = eartag_file_init(&m_file, &file_init);
            APP_ERROR_CHECK(err_code);
            break;

        case FDS_EVT_GC:
            
            break;

        default:
            break;
    }
}


/**@brief Function for setting up file system. */


static void file_init(void)
{
    uint32_t err_code;

    err_code = fds_register(fds_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = fds_init();
    APP_ERROR_CHECK(err_code);

    // Note: initialization will continue when FDS_EVT_INIT is received.
}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds)
{
    //bsp_event_t startup_event;

    //uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS, bsp_event_handler);
    //APP_ERROR_CHECK(err_code);

    //err_code = bsp_btn_ble_init(NULL, &startup_event);
    //APP_ERROR_CHECK(err_code);

    //*p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}


/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for placing the application in low power state while waiting for events.
 */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}


/**@brief amplifier control
 */

 

void sky66114_ctrl(void)
{
    NRF_GPIOTE->CONFIG[6] = 0x00031F03;     // CTX: GPIOTE channnel #6, TASK mode, GPIO 31 (P0.31), TOGGLE, default low
    NRF_GPIOTE->CONFIG[7] = 0x00022203;     //old tag 0x00022403; CRX: GPIOTE channnel #7, TASK mode, GPIO 34 (P1.04), TOGGLE, default low

    NRF_PPI->CH[14].EEP = (uint32_t)&NRF_RADIO->EVENTS_TXREADY;     // PPI channel #14, EEP <= RADIO.EVENTS_TXREADY
    NRF_PPI->CH[14].TEP = (uint32_t)&NRF_GPIOTE->TASKS_SET[6];      // PPI channel #14, TEP <= GPIOTE.TASKS_SET[6]

    NRF_PPI->CH[15].EEP = (uint32_t)&NRF_RADIO->EVENTS_RXREADY;     // PPI channel #15, EEP <= RADIO.EVENTS_RXREADY
    NRF_PPI->CH[15].TEP = (uint32_t)&NRF_GPIOTE->TASKS_SET[7];      // PPI channel #15, TEP <= GPIOTE.TASKS_SET[7]

    NRF_PPI->CH[16].EEP = (uint32_t)&NRF_RADIO->EVENTS_DISABLED;    // PPI channel #16, EEP <= RADIO.EVENTS_DISABLED
    NRF_PPI->CH[16].TEP = (uint32_t)&NRF_GPIOTE->TASKS_CLR[6];      // PPI channel #16, TEP <= GPIOTE.TASKS_CLR[6]
    NRF_PPI->FORK[16].TEP = (uint32_t)&NRF_GPIOTE->TASKS_CLR[7];    // PPI channel #16: FORK.TEP <= GPIOTE.TASKS_CLR[7]

    NRF_PPI->CHENSET = 0x0001C000;                                  // Enable PPI channel #14, 15 & 16
}

int main(void)
{
    uint32_t err_code;
    bool     erase_bonds;
	
		//POWER MEASURE
		nrf_gpio_cfg_output(26);
		nrf_gpio_pin_set(26);
		

		//LED
		nrf_gpio_cfg_output(LED_OUT);
		nrf_gpio_pin_clear(LED_OUT);
		
    // Initialize.
    err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

		
		lfclk_config();

    rtc_config();

    //log_init();
		
		
			//CPS
		nrf_gpio_cfg_output(CPS_PIN); //2 on tag and 47 on collar
    nrf_gpio_pin_clear(CPS_PIN);  //clear = low pwer mode, set = bypass mode
		
	  //CRX  //if 1 then 32 + pin number
		//nrf_gpio_cfg_output(34);//34 on tag and 36 on collar 32 on beacon
    //nrf_gpio_pin_clear(34);
		
		//CTX
		//nrf_gpio_cfg_output(31);//31 on tag and 45 on collar
    //nrf_gpio_pin_clear(31);
		

    buttons_leds_init(&erase_bonds);
		
    ble_stack_init();
	
		sky66114_ctrl();
		
		
    gap_params_init();
    gatt_init();
		
    services_init();
    conn_params_init();
    advertising_init();
		
		
    scan_init();
    file_init();
		//sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);
	  //NRF_POWER_Type_t.DCDCEN = POWER_DCDCEN_DCDCEN_Enabled;
		
		
	
    /* Start advertising. */
    err_code = eartag_adv_start_coded_nonconnectable(&m_adv);
    APP_ERROR_CHECK(err_code);

		
    twi_init();
    ACCEL_set_mode();
		start_sensor();
		while (m_xfer_done == false);
	  read_sensor_data();

		nrf_gpio_pin_set(LED_OUT);
		//NRF_LOG_INFO("startsss");
		


    // Enter main loop.
    for (;;)
    {
        power_manage();
			
    }
}


/**
 * @}
 */
