/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
#include "sdk_common.h"
#include "ble_advdata.h"
#include "eartag_adv.h"
#include "nrf_soc.h"
#include "nrf_log.h"
#include "nrf_fstorage.h"
#include "sdk_errors.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "ble_gap.h"


#define MODULE_INITIALIZED (p_eartag_adv->state != EARTAG_ADV_STATE_INIT)  /**< Macro designating whether the module has been initialized properly. */
#define COMPANY_ID         0x0059                                          /**< Nordic Semiconductor. */


/**@brief Function to build manufacturer specific data. */
static uint32_t build_manufacturer_specific_data(eartag_adv_t * const p_eartag_adv)
{
    uint32_t err_code;
    ble_gap_addr_t gap_addr;

    memset(p_eartag_adv->manu_spec_data, 0, EARTAG_ADV_MANU_SPEC_DATA_MAX_LENGTH);

    err_code = sd_ble_gap_addr_get(&gap_addr);
    VERIFY_SUCCESS(err_code);

    sprintf((char *)p_eartag_adv->manu_spec_data, "%02x%02x%02x%02x%02x%02x:%02x:%02x:%02x:01",
            gap_addr.addr[5],
            gap_addr.addr[4],
            gap_addr.addr[3],
            gap_addr.addr[2],
            gap_addr.addr[1],
            gap_addr.addr[0],
            p_eartag_adv->batt_lv,
            p_eartag_adv->temperature,
            p_eartag_adv->fw_ver);

    p_eartag_adv->manu_spec_data_len = strlen((char *)p_eartag_adv->manu_spec_data);

    return NRF_SUCCESS;
}


/**@brief Function for notifying event to application. */
static void notify_event(eartag_adv_t * const p_eartag_adv, eartag_adv_evt_t adv_evt)
{
    p_eartag_adv->adv_evt = adv_evt;
    if (p_eartag_adv->evt_handler != NULL)
    {
        p_eartag_adv->evt_handler(adv_evt);
    }
}


/**@brief Function for notifying error to application. */
static void notify_error(eartag_adv_t * const p_eartag_adv, uint32_t err_code)
{
    if (p_eartag_adv->error_handler != NULL)
    {
        p_eartag_adv->error_handler(err_code);
    }
}


/**@brief Function for setting up advertising data (coded PHY). */
static uint32_t set_coded_nonconnectable_advertising_data(eartag_adv_t * const p_eartag_adv)
{
    ble_advdata_manuf_data_t manu_spec_data;
    ret_code_t err_code;

    err_code = build_manufacturer_specific_data(p_eartag_adv);
    VERIFY_SUCCESS(err_code);

    manu_spec_data.company_identifier = COMPANY_ID;
    manu_spec_data.data.p_data        = p_eartag_adv->manu_spec_data;
    manu_spec_data.data.size          = p_eartag_adv->manu_spec_data_len;

    memset(&p_eartag_adv->coded.advdata, 0, sizeof(ble_advdata_t));
    p_eartag_adv->coded.advdata.flags                 = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    p_eartag_adv->coded.advdata.p_manuf_specific_data = &manu_spec_data;

    return ble_advdata_set(&p_eartag_adv->coded.advdata, NULL);
}


/**@brief Function for starting coded non-connectable advertising. */
static uint32_t start_coded_nonconnectable_advertising(eartag_adv_t * const p_eartag_adv)
{
    uint32_t err_code = set_coded_nonconnectable_advertising_data(p_eartag_adv);
    VERIFY_SUCCESS(err_code);

    return sd_ble_gap_adv_start(BLE_GAP_ADV_SET_HANDLE_DEFAULT,
                                &p_eartag_adv->coded.advparam,
                                p_eartag_adv->conn_cfg_tag);
}


/**@brief Function for setting up advertising data (1M, connectable). */
static uint32_t set_legacy_connectable_advertising_data(eartag_adv_t * const p_eartag_adv)
{
    return ble_advdata_set(&p_eartag_adv->legacy.advdata, NULL);
}


/**@brief Function for starting legacy advertising. */
static uint32_t start_legacy_connectable_advertising(eartag_adv_t * const p_eartag_adv)
{
    uint32_t err_code = set_legacy_connectable_advertising_data(p_eartag_adv);
    VERIFY_SUCCESS(err_code);

    return sd_ble_gap_adv_start(BLE_GAP_ADV_SET_HANDLE_DEFAULT,
                                &p_eartag_adv->legacy.advparam,
                                p_eartag_adv->conn_cfg_tag);
}


static uint32_t apply_manu_spec_data(eartag_adv_t * const p_eartag_adv)
{
    uint32_t err_code;

    if (p_eartag_adv->state == EARTAG_ADV_STATE_CODED_NONCONN)
    {
        (void) sd_ble_gap_adv_stop(BLE_GAP_ADV_SET_HANDLE_DEFAULT);

        err_code = start_coded_nonconnectable_advertising(p_eartag_adv);
        VERIFY_SUCCESS(err_code);
    }

    return NRF_SUCCESS;
}


/**@brief Function for handling the Connected event.
 *
 * @param[in] p_eartag_adv EarTag Advertising module instance.
 * @param[in] p_ble_evt    Event received from the BLE stack.
 */
static void on_connected(eartag_adv_t * const p_eartag_adv, ble_evt_t const * p_ble_evt)
{
    p_eartag_adv->state = EARTAG_ADV_STATE_CONNECTED;
    notify_event(p_eartag_adv, EARTAG_ADV_EVT_STOPPED);
}


/**@brief Function for handling the Disconnected event.
 *
 * @param[in] p_eartag_adv EarTag Advertising module instance.
 * @param[in] p_ble_evt    Event received from the BLE stack.
 */
static void on_disconnected(eartag_adv_t * const p_eartag_adv, ble_evt_t const * p_ble_evt)
{
    uint32_t err_code = start_coded_nonconnectable_advertising(p_eartag_adv);
    if (err_code == NRF_SUCCESS)
    {
        p_eartag_adv->state = EARTAG_ADV_STATE_CODED_NONCONN;
        notify_event(p_eartag_adv, EARTAG_ADV_EVT_CODED_NONCONN_ADV);
    }
    else
    {
        notify_error(p_eartag_adv, err_code);
    }
}


/**@brief Function for handling the Timeout event.
 *
 * @param[in] p_eartag_adv EarTag Advertising module instance.
 * @param[in] p_ble_evt    Event received from the BLE stack.
 */
static void on_timeout(eartag_adv_t * const p_eartag_adv, ble_evt_t const * p_ble_evt)
{
    ret_code_t err_code;

    if (p_ble_evt->evt.gap_evt.params.timeout.src != BLE_GAP_TIMEOUT_SRC_ADVERTISING)
    {
        return;
    }

    err_code = start_coded_nonconnectable_advertising(p_eartag_adv);
    if (err_code == NRF_SUCCESS)
    {
        p_eartag_adv->state = EARTAG_ADV_STATE_CODED_NONCONN;
        notify_event(p_eartag_adv, EARTAG_ADV_EVT_CODED_NONCONN_ADV);
    }
    else
    {
        notify_error(p_eartag_adv, err_code);
    }
}


/** @brief Function to determine if a flash write operation in in progress.
 *
 * @return true if a flash operation is in progress, false if not.
 */
static bool flash_access_in_progress()
{
    return nrf_fstorage_is_busy(NULL);
}


void eartag_adv_conn_cfg_tag_set(eartag_adv_t * const p_eartag_adv,
                                 uint8_t              ble_cfg_tag)
{
    VERIFY_PARAM_NOT_NULL_VOID(p_eartag_adv);
    VERIFY_MODULE_INITIALIZED_VOID();

    p_eartag_adv->conn_cfg_tag = ble_cfg_tag;
}


uint32_t eartag_adv_init(eartag_adv_t            * const p_eartag_adv,
                         eartag_adv_init_t const * const p_init)
{
    VERIFY_PARAM_NOT_NULL(p_eartag_adv);
    VERIFY_PARAM_NOT_NULL(p_init);

    memset(p_eartag_adv, 0, sizeof(eartag_adv_t));
    p_eartag_adv->state           = EARTAG_ADV_STATE_INIT;
    p_eartag_adv->conn_cfg_tag    = p_init->conn_cfg_tag;
    p_eartag_adv->fw_ver          = p_init->fw_ver;
    p_eartag_adv->batt_lv         = p_init->batt_lv;
    p_eartag_adv->temperature     = p_init->temperature;
    p_eartag_adv->evt_handler     = p_init->evt_handler;
    p_eartag_adv->error_handler   = p_init->error_handler;

    /* Copy advertising data. */
    memcpy(&p_eartag_adv->legacy.advdata, &p_init->advdata_legacy, sizeof(ble_advdata_t));
    
    /* Advertising parameters (legacy PHY). */
    p_eartag_adv->legacy.advparam.properties.connectable = 1;
    p_eartag_adv->legacy.advparam.properties.scannable   = 1;    
    p_eartag_adv->legacy.advparam.properties.legacy_pdu  = 1;
    p_eartag_adv->legacy.advparam.p_peer_addr = NULL;
    p_eartag_adv->legacy.advparam.fp          = BLE_GAP_ADV_FP_ANY;
    p_eartag_adv->legacy.advparam.interval    = p_init->interval_legacy;
    p_eartag_adv->legacy.advparam.duration    = p_init->duration_legacy;
    p_eartag_adv->legacy.advparam.primary_phy = BLE_GAP_PHY_1MBPS;

    /* Advertising parameters (Coded PHY). */
    p_eartag_adv->coded.advparam.properties.connectable = 1;    // Non-connectable advertising using CODED PHY is not currently supported.
    p_eartag_adv->coded.advparam.properties.scannable   = 0;    // Scan response is not currently supported in CODED PHY.
    p_eartag_adv->coded.advparam.properties.legacy_pdu  = 0;
    p_eartag_adv->coded.advparam.p_peer_addr   = NULL;
    p_eartag_adv->coded.advparam.fp            = BLE_GAP_ADV_FP_ANY;
    p_eartag_adv->coded.advparam.interval      = p_init->interval_coded;
    p_eartag_adv->coded.advparam.duration      = 0;             // Advertising forever.
    p_eartag_adv->coded.advparam.primary_phy   = BLE_GAP_PHY_CODED;
    p_eartag_adv->coded.advparam.secondary_phy = BLE_GAP_PHY_CODED;

    p_eartag_adv->state = EARTAG_ADV_STATE_IDLE;
    return NRF_SUCCESS;
}


uint32_t eartag_adv_start_coded_nonconnectable(eartag_adv_t * const p_eartag_adv)
{
    uint32_t err_code;
    VERIFY_PARAM_NOT_NULL(p_eartag_adv);
    VERIFY_MODULE_INITIALIZED();

    if (p_eartag_adv->coded.start_pending)
    {
        return NRF_SUCCESS;
    }

    if (p_eartag_adv->legacy.start_pending)
    {
        return NRF_ERROR_BUSY;
    }

    if (flash_access_in_progress())
    {
        p_eartag_adv->coded.start_pending = true;
        return NRF_SUCCESS;
    }

    switch (p_eartag_adv->state)
    {
        case EARTAG_ADV_STATE_LEGACY_CONN:
            (void) sd_ble_gap_adv_stop(BLE_GAP_ADV_SET_HANDLE_DEFAULT);

        case EARTAG_ADV_STATE_IDLE:                             // Fall through
            err_code = start_coded_nonconnectable_advertising(p_eartag_adv);
            VERIFY_SUCCESS(err_code);

            p_eartag_adv->state = EARTAG_ADV_STATE_CODED_NONCONN;
            notify_event(p_eartag_adv, EARTAG_ADV_EVT_CODED_NONCONN_ADV);
            break;

        case EARTAG_ADV_STATE_CONNECTED:
            return NRF_ERROR_BUSY;

        case EARTAG_ADV_STATE_CODED_NONCONN:
            break;

        default:
            /* Not suppose to be here. */
            return NRF_ERROR_INTERNAL;
    }

    return NRF_SUCCESS;
}


uint32_t eartag_adv_start_legacy(eartag_adv_t * const p_eartag_adv)
{
    uint32_t err_code;
    VERIFY_PARAM_NOT_NULL(p_eartag_adv);
    VERIFY_MODULE_INITIALIZED();

    if (p_eartag_adv->legacy.start_pending)
    {
        return NRF_SUCCESS;
    }

    if (p_eartag_adv->coded.start_pending)
    {
        return NRF_ERROR_BUSY;
    }

    if (flash_access_in_progress())
    {
        p_eartag_adv->legacy.start_pending = true;
        return NRF_SUCCESS;
    }

    switch (p_eartag_adv->state)
    {
        case EARTAG_ADV_STATE_CODED_NONCONN:
            (void) sd_ble_gap_adv_stop(BLE_GAP_ADV_SET_HANDLE_DEFAULT);

        case EARTAG_ADV_STATE_IDLE:                             // Fall through
            err_code = start_legacy_connectable_advertising(p_eartag_adv);
            VERIFY_SUCCESS(err_code);

            p_eartag_adv->state = EARTAG_ADV_STATE_CODED_NONCONN;
            notify_event(p_eartag_adv, EARTAG_ADV_EVT_LEGACY_ADV);
            break;

        case EARTAG_ADV_STATE_CONNECTED:
            return NRF_ERROR_BUSY;

        case EARTAG_ADV_STATE_LEGACY_CONN:
            break;

        default:
            /* Not suppose to be here. */
            return NRF_ERROR_INTERNAL;
    }

    return NRF_SUCCESS;
}


void eartag_adv_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    eartag_adv_t * p_eartag_adv = (eartag_adv_t *)p_context;

    VERIFY_PARAM_NOT_NULL_VOID(p_eartag_adv);
    VERIFY_PARAM_NOT_NULL_VOID(p_ble_evt);
    VERIFY_MODULE_INITIALIZED_VOID();

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connected(p_eartag_adv, p_ble_evt);
            break;

        // Upon disconnection, restart non-connectable advertising using code PHY.
        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnected(p_eartag_adv, p_ble_evt);
            break;

        // Upon time-out, restart non-connectable advertising using code PHY.
        case BLE_GAP_EVT_TIMEOUT:
            on_timeout(p_eartag_adv, p_ble_evt);
            break;

        default:
            break;
    }
}


void eartag_adv_on_sys_evt(uint32_t evt_id, void * p_context)
{
    eartag_adv_t * p_eartag_adv = (eartag_adv_t *)p_context;
    uint32_t err_code;

    switch (evt_id)
    {
        // When a flash operation finishes, re-attempt to start advertising operations.
        case NRF_EVT_FLASH_OPERATION_SUCCESS:
        case NRF_EVT_FLASH_OPERATION_ERROR:
            if (p_eartag_adv->coded.start_pending)
            {
                p_eartag_adv->coded.start_pending = false;
                err_code = eartag_adv_start_coded_nonconnectable(p_eartag_adv);
                if (err_code != NRF_SUCCESS)
                {
                    notify_error(p_eartag_adv, err_code);
                }
            }
            else if (p_eartag_adv->legacy.start_pending)
            {
                p_eartag_adv->legacy.start_pending = false;
                err_code = eartag_adv_start_legacy(p_eartag_adv);
                if (err_code != NRF_SUCCESS)
                {
                    notify_error(p_eartag_adv, err_code);
                }
            }
            break;

        default:
            // No implementation needed.
            break;
    }
}


uint32_t eartag_adv_set_batt_lv(eartag_adv_t * const p_eartag_adv, uint8_t batt_lv)
{
    VERIFY_PARAM_NOT_NULL(p_eartag_adv);
    VERIFY_MODULE_INITIALIZED();

    if (batt_lv > 100)
    {
        return NRF_ERROR_INVALID_DATA;
    }
    else
    {
        p_eartag_adv->batt_lv = batt_lv;
    }

    return apply_manu_spec_data(p_eartag_adv);
}


uint32_t eartag_adv_set_temperature(eartag_adv_t * const p_eartag_adv, uint8_t temperature)
{
    VERIFY_PARAM_NOT_NULL(p_eartag_adv);
    VERIFY_MODULE_INITIALIZED();

    p_eartag_adv->temperature = temperature;
    return apply_manu_spec_data(p_eartag_adv);
}


