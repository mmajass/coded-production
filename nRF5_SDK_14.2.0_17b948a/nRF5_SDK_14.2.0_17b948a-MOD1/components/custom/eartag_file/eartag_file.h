/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/**@file
 *
 * @defgroup EarTag File Module
 * @{
 * @ingroup  eartag_lib
 * @brief    Module for handling FDS.
 *
 * @details  The File Module handles Coded PHY file for your application.
 *
 */

#ifndef EARTAG_FILE_H__
#define EARTAG_FILE_H__

#include <stdint.h>
#include "nrf_error.h"
#include "eartag_scan.h"

#ifdef __cplusplus
extern "C" {
#endif

/**@brief   Macro for defining a eartag_file instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define EARTAG_FILE_DEF(_name)                                                 \
static eartag_file_t _name;                                                    \


#define EARTAG_FILE_CFG_ID              0x91D1                  /**< File ID of configuration file in FDS. */
#define EARTAG_FILE_CFG_KEY             0x9FCF                  /**< File ID of configuration file in FDS. */

#define EARTAG_FILE_DB_ID               0x1EBA                  /**< File ID of database file in FDS. */
#define EARTAG_FILE_DB_KEY_START        0x0001                  /**< Key start value in FDS. */

#define EARTAG_FILE_MAX_NUM_OF_REC      8                       /**< Maximum number of FDS records. */
#define EARTAG_FILE_ENTRIES_PER_REC     250                     /**< Entries per record. */
#define EARTAG_FILE_MAX_NUM_OF_ENTRIES  (EARTAG_FILE_MAX_NUM_OF_REC * EARTAG_FILE_ENTRIES_PER_REC)  /**< Maximum number of entries in files. */

#define EARTAG_FILE_UDID_LEN            EARTAG_SCAN_UDID_LEN    /**< Length of UDID. */

//swapping to save power
//original here:
//#define EARTAG_FILE_MODE_HERITAGE       0x000000001             /**< Heritage mode. */
//#define EARTAG_FILE_MODE_LOCATION       0x000000002             /**< Location mode. */


#define EARTAG_FILE_MODE_HERITAGE       0x000000002             /**< Heritage mode. */
#define EARTAG_FILE_MODE_LOCATION       0x000000001             /**< Location mode. */

/**@brief   State machine. */
typedef enum
{
    EARTAG_FILE_STATE_INIT = 0,             /**< Right after boot. Initialization is required. */
    EARTAG_FILE_STATE_IDLE,                 /**< Idle. */
    EARTAG_FILE_STATE_WRITE,                /**< File write in progress. */
    EARTAG_FILE_STATE_DELETE,               /**< File delete in progress. */
} eartag_file_state_t;

/**@brief   Eartag File events.
 *
 * @details These events are propagated to the main application if a handler was
 *          provided during initialization of the Eartag File Module.
 */
typedef enum
{
    EARTAG_FILE_EVT_INIT,                   /**< Initialization.*/
    EARTAG_FILE_EVT_WRITE,                  /**< File written.*/
    EARTAG_FILE_EVT_DELETE,                 /**< File deleted.*/
    EARTAG_FILE_EVT_FULL,                   /**< File system full. Suggest garbage collection. */
} eartag_file_evt_t;


typedef struct eartag_file_s eartag_file_t;


/**@brief Configuration file type. */
typedef struct
{
    uint32_t mode;                          /**< Modes. */
} eartag_file_cfg_t;


/**@brief Database entry. */
typedef struct
{
    eartag_udid_t udid;                     /**< UDID. */
    uint32_t      count;                    /**< Counter. */
} eartag_file_db_entry_t;


/**@brief   EarTag file event handler type. */
typedef void (*eartag_file_evt_handler_t) (eartag_file_evt_t const file_evt);

/**@brief   EarTag file error handler type. */
typedef void (*eartag_file_error_handler_t) (uint32_t nrf_error);

typedef ret_code_t (*eartag_file_write_fn_t) (fds_record_desc_t *, fds_record_t const *);


struct eartag_file_s
{
    eartag_file_state_t         state;                  /**< Variable to keep track of the current state. */
    eartag_file_evt_handler_t   evt_handler;            /**< Handler for the events. Can be initialized as NULL if no handling is implemented on in the main application. */
    eartag_file_error_handler_t error_handler;          /**< Handler for the error events. */

    struct
    {
        uint16_t entries_to_write;                      /**< Number of entries to write. */
        uint16_t entries_written;                       /**< Number of entries written. */
        uint16_t last_id;                               /**< Last known file ID, in FDS requests. */
        uint16_t last_key;                              /**< Last known record key, in FDS requests. */
        uint16_t num_records;                           /**< Current number of records in FDS. */
        uint8_t  dirty[EARTAG_FILE_MAX_NUM_OF_REC];     /**< Flag which tells whether the record is dirty. */
        fds_record_desc_t      rec_desc;                /**< FDS record descriptor. */
        fds_record_t           rec_write;               /**< FDS record (write). */
        eartag_file_write_fn_t write_fn;                /**< FDS write function, for retry after garbage collection. */
    } fds;

    struct
    {
        eartag_file_cfg_t data;                         /**< Configuration. */
    } cfg;

    struct
    {
        uint16_t               num_entries;                             /**< Number of entries in the database. */
        eartag_file_db_entry_t data[EARTAG_FILE_MAX_NUM_OF_ENTRIES];    /**< Database. */
    } db;
};


/**@brief   Initialization parameters for the EarTag File Module.
 * @details This structure is used to pass file options and an event handler
 *          to the EarTag File Module during initialization.
 */
typedef struct
{
    eartag_file_evt_handler_t   evt_handler;    /**< Event handler that will be called upon events. */
    eartag_file_error_handler_t error_handler;  /**< Error handler that will propogate internal errors to the main applications. */
} eartag_file_init_t;


/**@brief   Function for initializing the EarTag File Module.
 *
 * @details Initializes FDS amd local database. If configuration file does not exist,
 *          then touch a new one. Then, load database and configuration file from flash.
 *
 * @param[out] p_eartag_file EarTag File module instance. This structure 
 *                           must be supplied by the application. It is initialized
 *                           by this function and will later be used to identify
 *                           this particular module instance.
 * @param[in] p_init         Information needed to initialize the module.
 *
 * @retval NRF_SUCCESS If initialization was successful. Otherwise, an error code is returned.
 */
uint32_t eartag_file_init(eartag_file_t            * const p_eartag_file,
                          eartag_file_init_t const * const p_init);


/**@brief   Function for handling FDS event.
 * 
 * @details Handles events from FDS module.
 *
 * @param[in] p_eartag_file EarTag File module instance.
 * @param[in] p_evt         FDS event structure.
 *
 * @retval NRF_SUCCESS If successful. Otherwise, an error code is returned.
 */
void eartag_file_on_fds_evt(eartag_file_t   * const p_eartag_file,
                            fds_evt_t const * const p_evt);


/**@brief   Function for putting UDID entries to database.
 * 
 * @details Put UDID entries to database. Checks if UDID exists in the database.
 *          If it exists, then counter increment. Else, insert new one. Discards
 *          elements when inserting to a database which is already full.
 *
 * @param[in] p_eartag_file EarTag File module instance.
 * @param[in] p_udid        Array of UDID.
 * @param[in] num           Number of UDID.
 *
 * @retval NRF_SUCCESS If successful. Otherwise, an error code is returned.
 */
uint32_t eartag_file_db_put(eartag_file_t       * const p_eartag_file,
                            eartag_udid_t const * const p_udid,
                            uint16_t              num);


/**@brief   Function for writing local database to file.
 *
 * @details Write file. Divide into multiple records when necessary.
 *
 * @param[in] p_eartag_file EarTag File module instance.
 *
 * @retval NRF_SUCCESS If successful. Otherwise, an error code is returned.
 */
uint32_t eartag_file_db_write(eartag_file_t * const p_eartag_file);


/**@brief   Function for clearing local database to file.
 *
 * @details Delete DB file, and then clear local database.
 *
 * @param[in] p_eartag_file EarTag File module instance.
 *
 * @retval NRF_SUCCESS If successful. Otherwise, an error code is returned.
 */
uint32_t eartag_file_db_clear(eartag_file_t * const p_eartag_file);


/**@brief   Function for writing configuation structure to file.
 *
 * @details Write file.
 *
 * @param[in] p_eartag_file EarTag File module instance.
 *
 * @retval NRF_SUCCESS If successful. Otherwise, an error code is returned.
 */
uint32_t eartag_file_config_write(eartag_file_t * const p_eartag_file);


/**@brief   Function for getting configuration file.
 *
 * @details Get raw data of configuration file.
 *
 * @param[in] p_eartag_file EarTag File module instance.
 * @param[out] p_cfg        Space provided by user to store configuration file.
 *
 * @retval NRF_SUCCESS If successful. Otherwise, an error code is returned.
 */
uint32_t eartag_file_get_config(eartag_file_t      * const p_eartag_file,
                                eartag_file_cfg_t ** pp_cfg);


/**@brief   Function for getting data of local database.
 *
 * @details Get raw data of local database.
 *
 * @param[in] p_eartag_file EarTag File module instance.
 * @param[out] pp_udid      Space provided by user to store pointer to UDID.
 * @param[out] p_num        Number of UDIDs, to be returned.
 *
 * @retval NRF_SUCCESS If successful. Otherwise, an error code is returned.
 */
uint32_t eartag_file_get_db(eartag_file_t           * const p_eartag_file,
                            eartag_file_db_entry_t ** pp_udid,
                            uint16_t                * p_num);


/** @} */


#ifdef __cplusplus
}
#endif

#endif // EARTAG_FILE_H__

/** @} */
