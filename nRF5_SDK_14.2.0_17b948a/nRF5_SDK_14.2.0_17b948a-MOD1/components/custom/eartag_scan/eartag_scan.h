/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/**@file
 *
 * @defgroup EarTag Scanning Module
 * @{
 * @ingroup  eartag_lib
 * @brief    Module for handling Coded PHY scanning.
 *
 * @details  The Scanning Module handles Coded PHY scanning for your application.
 *
 */

#ifndef EARTAG_SCAN_H__
#define EARTAG_SCAN_H__

#include <stdint.h>
#include "nrf_error.h"
#include "ble.h"
#include "ble_advdata.h"
#include "app_timer.h"

#ifdef __cplusplus
extern "C" {
#endif

/**@brief   Macro for defining a eartag_scan instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define EARTAG_SCAN_DEF(_name)                                                 \
static eartag_scan_t _name;                                                    \
NRF_SDH_BLE_OBSERVER(_name ## _ble_obs,                                        \
                     BLE_ADV_BLE_OBSERVER_PRIO,                                \
                     eartag_scan_on_ble_evt, &_name);                          \


#define EARTAG_SCAN_MAX_NUM_OF_RESULTS               10      /**< Maximum number of scan results. */
#define EARTAG_SCAN_UDID_LEN                         12      /**< Length of UDID. */
#define EARTAG_SCAN_RSSI_MIN                         (-128)  /**< Minimum RSSI value. */


/**@brief   State machine. */
typedef enum
{
    EARTAG_SCAN_STATE_INIT = 0,             /**< Right after boot. Initialization is required. */
    EARTAG_SCAN_STATE_STOPPED,              /**< Stopped. */
    EARTAG_SCAN_STATE_IDLE,                 /**< Idle. */
    EARTAG_SCAN_STATE_SCAN,                 /**< Scanning. */
} eartag_scan_state_t;

/**@brief   Eartag Scanning events.
 *
 * @details These events are propagated to the main application if a handler was
 *          provided during initialization of the Eartag Scanning Module.
 */
typedef enum
{
    EARTAG_SCAN_EVT_RESULTS,                /**< Scan result*/
		EARTAG_SCAN_START,
		EARTAG_SCAN_STOP,
} eartag_scan_evt_type_t;


typedef struct eartag_scan_s eartag_scan_t;


/**@brief UDID. */
typedef struct
{
    uint8_t data[EARTAG_SCAN_UDID_LEN];     /**< 12-byte UDID. */
} __attribute__((aligned(4))) eartag_udid_t;

/**@brief UDID backlog entry. */
typedef struct
{
    eartag_udid_t udid;                     /**< UDID. */
    int8_t        rssi;                     /**< RSSI. */
} eartag_udid_entry_t;

/**@brief   Eartag Scanning event on scan results.
 *
 * @details These events are propagated to the main application at the end of scanning window.
 */
typedef struct
{
    uint16_t              size;             /**< Number of entries. */
    eartag_udid_entry_t * data;             /**< udid entries. */
} eartag_scan_evt_results_t;

/**@brief   Eartag Scanning event structure.
 *
 * @details This structure is passed to an event coming from service.
 */
typedef struct
{
    eartag_scan_evt_type_t  type;           /**< Event type. */
    eartag_scan_t         * p_eartag_scan;  /**< A pointer to the instance. */
    union
    {
        eartag_scan_evt_results_t results;  /**< Scan results. */
    } data;
} eartag_scan_evt_t;


/**@brief   EarTag scanning event handler type. */
typedef void (*eartag_scan_evt_handler_t) (eartag_scan_evt_t const * scan_evt);

/**@brief   EarTag scanning error handler type. */
typedef void (*eartag_scan_error_handler_t) (uint32_t nrf_error);


struct eartag_scan_s
{
    eartag_scan_state_t         state;                  /**< Variable to keep track of the current state. */
    ble_gap_scan_params_t       scan_params;            /**< Scan parameters. */
    uint32_t                    scan_period_ticks;      /**< Scan period in number of app_timer ticks. See @ref app_timer_start. */
    uint32_t                    scan_duration_ticks;    /**< Scan duration in number of app_timer ticks. See @ref app_timer_start. */
    app_timer_t                 period_timer;           /**< Period timer. */
    app_timer_t                 duration_timer;         /**< Duration timer. */

    bool                        is_connected;           /**< flag which tells whether connection is active. */
    eartag_scan_evt_t           scan_evt;               /**< Eartag Scanning event propogated to the main application. */

    eartag_scan_evt_handler_t   evt_handler;            /**< Handler for the events. Can be initialized as NULL if no handling is implemented on in the main application. */
    eartag_scan_error_handler_t error_handler;          /**< Handler for the error events. */

    struct
    {
        uint16_t            num_entries;                            /**< Number of entries stored. */
        eartag_udid_entry_t data[EARTAG_SCAN_MAX_NUM_OF_RESULTS];   /**< Stored UDIDs. */
    } sort;
};


/**@brief   Initialization parameters for the EarTag Scanning Module.
 * @details This structure is used to pass scanning options and an event handler
 *          to the EarTag Scanning Module during initialization.
 */
typedef struct
{
    uint32_t scan_period_ticks;                 /**< Scan period in number of app_timer ticks. See @ref app_timer_start. */
    uint32_t scan_duration_ticks;               /**< Scan duration in number of app_timer ticks. See @ref app_timer_start. */
    eartag_scan_evt_handler_t   evt_handler;    /**< Event handler that will be called upon events. */
    eartag_scan_error_handler_t error_handler;  /**< Error handler that will propogate internal errors to the main applications. */
} eartag_scan_init_t;


/**@brief   Function for handling BLE events.
 *
 * @details This function must be called from the BLE stack event dispatcher for
 *          the module to handle BLE events that are relevant for the EarTag
 *          Scanning Module.
 *
 * @param[in] p_ble_evt     BLE stack event.
 * @param[in] p_eartag_scan EarTag Scanning module instance.
 */
void eartag_scan_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_eartag_scan);


/**@brief   Function for initializing the EarTag Scanning Module.
 *
 * @details Initializes scanning parameters and timers which are essential elements
 *          for periodic scannning.
 *
 * @param[out] p_eartag_scan EarTag Scanning module instance. This structure 
 *                           must be supplied by the application. It is initialized
 *                           by this function and will later be used to identify
 *                           this particular module instance.
 * @param[in] p_init         Information needed to initialize the module.
 *
 * @retval NRF_SUCCESS If initialization was successful. Otherwise, an error code is returned.
 */
uint32_t eartag_scan_init(eartag_scan_t            * const p_eartag_scan,
                          eartag_scan_init_t const * const p_init);



/**@brief   Function for starting scanning.
 *
 * @details Start scanning when idle.
 *
 * @param[out] p_eartag_scan EarTag Scanning module instance.
 *
 * @retval NRF_SUCCESS If initialization was successful. Otherwise, an error code is returned.
 */
uint32_t eartag_scan_start(eartag_scan_t * const p_eartag_scan);


/**@brief   Function for stopping scanning.
 *
 * @details Start scanning when idle.
 *
 * @param[out] p_eartag_scan EarTag Scanning module instance.
 *
 * @retval NRF_SUCCESS If initialization was successful. Otherwise, an error code is returned.
 */
uint32_t eartag_scan_stop(eartag_scan_t * const p_eartag_scan);


/** @} */


#ifdef __cplusplus
}
#endif

#endif // EARTAG_SCAN_H__

/** @} */
