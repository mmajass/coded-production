/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
#include "sdk_common.h"
#include "ble_advdata.h"
#include "eartag_scan.h"
#include "nrf_soc.h"
#include "nrf_log.h"
#include "nrf_fstorage.h"
#include "sdk_errors.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "ble_gap.h"
#include <stdlib.h>


#define MODULE_INITIALIZED          (p_eartag_scan->state != EARTAG_SCAN_STATE_INIT)    /**< Macro designating whether the module has been initialized properly. */
#define COMPANY_ID                  0x0059                                              /**< Nordic Semiconductor. */
#define MANU_SPEC_DATA_MIN_LENGTH   24                                                  /**< Minimum length of manufacturer specific data. */
#define SHORT_PACKET_MAX						2

#define SCAN_INTERVAL               0x0050                                              /**< Determines scan interval in units of 0.625 millisecond. */
#define SCAN_WINDOW                 0x0050                                              /**< Determines scan window in units of 0.625 millisecond. */

#ifndef __compar_fn_t
typedef int (*__compar_fn_t) (const void *, const void *);                              /**< Definition of compare function. */
#endif


/**@brief Reads an advertising report and checks if a EarTag manufactuer specific
 *        data is present.
 *
 * @details The function is able to search for manufactuer specific data having
 *          length at least 24 bytes, and having company ID 0x0059.
 */
static uint32_t find_eartag_data(eartag_scan_t * const p_eartag_scan,
                                 ble_gap_evt_adv_report_t const * p_adv_report,
                                 eartag_udid_entry_t * p_udid_entry)
{
    uint16_t  index  = 0;
    uint8_t * p_data = (uint8_t *)p_adv_report->data;

    while (index < p_adv_report->dlen)
    {
        uint8_t field_length = p_data[index];
        uint8_t field_type   = p_data[index + 1];

        if (field_type == BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA)
        {
            uint16_t company_id = p_data[index + 2] | (p_data[index + 3] << 8);
						
						uint8_t packetver = p_data[index + 16];
						//NRF_LOG_INFO("scan packet version is %x or %d or %d", p_data[index + 16],  p_data[index + 16], packetver);
					
					
            if (company_id == COMPANY_ID && field_length >= (MANU_SPEC_DATA_MIN_LENGTH + 2) && packetver <= SHORT_PACKET_MAX)
            {
               // NRF_LOG_INFO("yes save");
							  memcpy(p_udid_entry->udid.data, &p_data[index + 4], EARTAG_SCAN_UDID_LEN);
								p_udid_entry->rssi = p_adv_report->rssi;
									
                return NRF_SUCCESS;
            }
        }
        index += field_length + 1;
    }
    return NRF_ERROR_NOT_FOUND;
}


/**@brief Initialize sorting-context. */
static uint32_t sort_init(eartag_scan_t * const p_eartag_scan)
{
    uint16_t i;

    p_eartag_scan->sort.num_entries = 0;
        
    for (i = 0; i < EARTAG_SCAN_MAX_NUM_OF_RESULTS; i++)
    {
        memset(p_eartag_scan->sort.data[i].udid.data, 0xFF, EARTAG_SCAN_UDID_LEN);
        p_eartag_scan->sort.data[i].rssi = EARTAG_SCAN_RSSI_MIN;
    }

    return NRF_SUCCESS;
}


/**@brief Compare function for qsort.
 * @details Perform sorting in descending order of RSSI.
 */
static int comp_fn (eartag_udid_entry_t * a, eartag_udid_entry_t * b)
{
    if (a->rssi > b->rssi)
    {
        return -1;
    }
    else if (a->rssi < b->rssi)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


/**@brief Insert UDID entry. */
static uint32_t insert_entry(eartag_scan_t * const p_eartag_scan, eartag_udid_entry_t * data)
{
    uint16_t i;

    /* Check if there is already an existing element in the list. */
    for (i = 0; i < EARTAG_SCAN_MAX_NUM_OF_RESULTS; i++)
    {
        if (0 == memcmp(p_eartag_scan->sort.data[i].udid.data, data->udid.data, EARTAG_SCAN_UDID_LEN))
        {
            p_eartag_scan->sort.data[i].rssi = data->rssi;
            return NRF_SUCCESS;
        }
    }

    i = p_eartag_scan->sort.num_entries;
    if (p_eartag_scan->sort.num_entries == EARTAG_SCAN_MAX_NUM_OF_RESULTS)
    {
        i--;
    }

    if (data->rssi > p_eartag_scan->sort.data[i].rssi)
    {
        memcpy(&p_eartag_scan->sort.data[i], data, sizeof(eartag_udid_entry_t));

        if (p_eartag_scan->sort.num_entries < EARTAG_SCAN_MAX_NUM_OF_RESULTS)
        {
            p_eartag_scan->sort.num_entries++;
        }

        return NRF_SUCCESS;
    }

    return NRF_ERROR_NOT_FOUND;
}


/**@brief Function for notifying results to application. */
static void notify_results(eartag_scan_t * const p_eartag_scan)
{
    if (p_eartag_scan->evt_handler != NULL)
    {
        memset (&p_eartag_scan->scan_evt, 0, sizeof(eartag_scan_evt_t));

        p_eartag_scan->scan_evt.type               = EARTAG_SCAN_EVT_RESULTS;
        p_eartag_scan->scan_evt.p_eartag_scan      = p_eartag_scan;
        p_eartag_scan->scan_evt.data.results.size  = p_eartag_scan->sort.num_entries;
        p_eartag_scan->scan_evt.data.results.data  = p_eartag_scan->sort.data;
        p_eartag_scan->evt_handler(&p_eartag_scan->scan_evt);
    }
}


/**@brief Function for notifying error to application. */
static void handle_error(eartag_scan_t * const p_eartag_scan, uint32_t err_code)
{
    if (err_code == NRF_SUCCESS)
    {
        return;
    }

    if (p_eartag_scan->error_handler != NULL)
    {
        p_eartag_scan->error_handler(err_code);
    }
}


/**@brief Function for handling the Connected event.
 *
 * @param[in] p_eartag_scan EarTag Scanning module instance.
 * @param[in] p_ble_evt     Event received from the BLE stack.
 */
static void on_connected(eartag_scan_t * const p_eartag_scan, ble_evt_t const * p_ble_evt)
{
    p_eartag_scan->is_connected = true;
}


/**@brief Function for handling the Disconnected event.
 *
 * @param[in] p_eartag_scan EarTag Scanning module instance.
 * @param[in] p_ble_evt     Event received from the BLE stack.
 */
static void on_disconnected(eartag_scan_t * const p_eartag_scan, ble_evt_t const * p_ble_evt)
{
    p_eartag_scan->is_connected = false;
}


/**@brief Function for handling the Advertising reports.
 *
 * @param[in] p_eartag_scan EarTag Scanning module instance.
 * @param[in] p_ble_evt     Event received from the BLE stack.
 */
static void on_adv_report(eartag_scan_t * const p_eartag_scan, ble_evt_t const * p_ble_evt)
{
    uint32_t err_code;
    eartag_udid_entry_t udid_entry;
    ble_gap_evt_adv_report_t const * p_adv_report = &p_ble_evt->evt.gap_evt.params.adv_report;

    err_code = find_eartag_data(p_eartag_scan, p_adv_report, &udid_entry);
    if (err_code == NRF_SUCCESS)
    {
        err_code = insert_entry(p_eartag_scan, &udid_entry);
        if (err_code == NRF_SUCCESS)
        {
            qsort(p_eartag_scan->sort.data, EARTAG_SCAN_MAX_NUM_OF_RESULTS,
                  sizeof(eartag_udid_entry_t), (__compar_fn_t)comp_fn);
        }
    }
}


/**@brief Function for handling the periodic timer event.
 *
 * @details This function will be called each time the periodic timer expires.
 *
 * @param[in] p_context EarTag Scanning module instance.
 */
static void scan_period_timeout_handler(void * p_context)
{
    uint32_t err_code;
    eartag_scan_t * p_eartag_scan = (eartag_scan_t *)p_context;

    VERIFY_PARAM_NOT_NULL_VOID(p_eartag_scan);

    /* Initialize sorting context. */
    err_code = sort_init(p_eartag_scan);
    handle_error(p_eartag_scan, err_code);

    /* Start scanning. */
    err_code = sd_ble_gap_scan_start(&p_eartag_scan->scan_params);
    handle_error(p_eartag_scan, err_code);

    /* Start scan duration timer. */
    err_code = app_timer_start(&p_eartag_scan->duration_timer,
                               p_eartag_scan->scan_duration_ticks,
                               p_eartag_scan);
    handle_error(p_eartag_scan, err_code);
	
		//notify application that scan starts
		if (p_eartag_scan->evt_handler != NULL)
		{
				memset (&p_eartag_scan->scan_evt, 0, sizeof(eartag_scan_evt_t));

				p_eartag_scan->scan_evt.type               = EARTAG_SCAN_START;
				p_eartag_scan->evt_handler(&p_eartag_scan->scan_evt);
		}

    p_eartag_scan->state = EARTAG_SCAN_STATE_SCAN;
}


/**@brief Function for handling the scan duration timer event.
 *
 * @details This function will be called each time the scan duration timer expires.
 *
 * @param[in] p_context EarTag Scanning module instance.
 */
static void scan_duration_timeout_handler(void * p_context)
{
    uint32_t err_code;
    eartag_scan_t * p_eartag_scan = (eartag_scan_t *)p_context;

    VERIFY_PARAM_NOT_NULL_VOID(p_eartag_scan);

    /* Stop scanning. */
    err_code = sd_ble_gap_scan_stop();
    handle_error(p_eartag_scan, err_code);
		
		/* Notify application that scanning stopped. */
		if (p_eartag_scan->evt_handler != NULL)
		{
				memset (&p_eartag_scan->scan_evt, 0, sizeof(eartag_scan_evt_t));

				p_eartag_scan->scan_evt.type               = EARTAG_SCAN_STOP;
				p_eartag_scan->evt_handler(&p_eartag_scan->scan_evt);
		}
		

    notify_results(p_eartag_scan);
    p_eartag_scan->state = EARTAG_SCAN_STATE_IDLE;
}


uint32_t eartag_scan_init(eartag_scan_t            * const p_eartag_scan,
                          eartag_scan_init_t const * const p_init)
{
    uint32_t err_code;
    app_timer_t * p_period_timer;
    app_timer_t * p_duration_timer;

    VERIFY_PARAM_NOT_NULL(p_eartag_scan);
    VERIFY_PARAM_NOT_NULL(p_init);

    memset(p_eartag_scan, 0, sizeof(eartag_scan_t));
    p_eartag_scan->state                = EARTAG_SCAN_STATE_INIT;
    p_eartag_scan->scan_period_ticks    = p_init->scan_period_ticks;
    p_eartag_scan->scan_duration_ticks  = p_init->scan_duration_ticks;
    p_eartag_scan->evt_handler          = p_init->evt_handler;
    p_eartag_scan->error_handler        = p_init->error_handler;

    /* Initialize timers. */
    p_period_timer = &p_eartag_scan->period_timer;
    err_code = app_timer_create(&p_period_timer,
                                APP_TIMER_MODE_REPEATED,
                                scan_period_timeout_handler);
    VERIFY_SUCCESS(err_code);
    
    p_duration_timer = &p_eartag_scan->duration_timer;
    err_code = app_timer_create(&p_duration_timer,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                scan_duration_timeout_handler);
    VERIFY_SUCCESS(err_code);
    
    /* Scanning parameters (coded PHY). */
    p_eartag_scan->scan_params.active            = 0;
    p_eartag_scan->scan_params.interval          = SCAN_INTERVAL;
    p_eartag_scan->scan_params.window            = SCAN_WINDOW;
    p_eartag_scan->scan_params.filter_policy     = BLE_GAP_SCAN_FP_ACCEPT_ALL;
    p_eartag_scan->scan_params.filter_duplicates = BLE_GAP_SCAN_DUPLICATES_REPORT;
    p_eartag_scan->scan_params.scan_phy          = BLE_GAP_PHY_CODED;
    p_eartag_scan->scan_params.duration          = 0;   // Duration not supported in Alpha softdevice.
    p_eartag_scan->scan_params.period            = 0;   // period not supported in Alpha softdevice.

    /* Initialize sorting context. */
    err_code = sort_init(p_eartag_scan);
    VERIFY_SUCCESS(err_code);

    p_eartag_scan->state = EARTAG_SCAN_STATE_STOPPED;
    return NRF_SUCCESS;
}


uint32_t eartag_scan_start(eartag_scan_t * const p_eartag_scan)
{
    uint32_t err_code;

    VERIFY_PARAM_NOT_NULL(p_eartag_scan);
    VERIFY_MODULE_INITIALIZED();
	
	

    switch (p_eartag_scan->state)
    {
        case EARTAG_SCAN_STATE_STOPPED:
            err_code = sd_ble_gap_scan_start(&p_eartag_scan->scan_params);
            VERIFY_SUCCESS(err_code);

            err_code = app_timer_start(&p_eartag_scan->period_timer,
                                       p_eartag_scan->scan_period_ticks,
                                       p_eartag_scan);
            VERIFY_SUCCESS(err_code);
            err_code = app_timer_start(&p_eartag_scan->duration_timer,
                                       p_eartag_scan->scan_duration_ticks,
                                       p_eartag_scan);
            VERIFY_SUCCESS(err_code);
            p_eartag_scan->state = EARTAG_SCAN_STATE_SCAN;
						
            break;

        case EARTAG_SCAN_STATE_IDLE:
        case EARTAG_SCAN_STATE_SCAN:
					
				
            break;

        default:
            // Not supposed to be here
            return NRF_ERROR_INTERNAL;
    }

    return NRF_SUCCESS;
}


uint32_t eartag_scan_stop(eartag_scan_t * const p_eartag_scan)
{
    uint32_t err_code;
    bool     is_notify = false;

    VERIFY_PARAM_NOT_NULL(p_eartag_scan);
    VERIFY_MODULE_INITIALIZED();

    switch (p_eartag_scan->state)
    {
        case EARTAG_SCAN_STATE_STOPPED:
            break;

        case EARTAG_SCAN_STATE_SCAN:
            err_code = sd_ble_gap_scan_stop();
            VERIFY_SUCCESS(err_code);

            is_notify = true;

            err_code = app_timer_stop(&p_eartag_scan->duration_timer);
            VERIFY_SUCCESS(err_code);

        case EARTAG_SCAN_STATE_IDLE:                            // Fall through
            err_code = app_timer_stop(&p_eartag_scan->period_timer);
            VERIFY_SUCCESS(err_code);
						
            p_eartag_scan->state = EARTAG_SCAN_STATE_STOPPED;
            break;

        default:
            // Not supposed to be here
            return NRF_ERROR_INTERNAL;
    }

    if (is_notify)
    {
        notify_results(p_eartag_scan);
    }

    return NRF_SUCCESS;
}


void eartag_scan_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    eartag_scan_t * p_eartag_scan = (eartag_scan_t *)p_context;

    VERIFY_PARAM_NOT_NULL_VOID(p_eartag_scan);
    VERIFY_PARAM_NOT_NULL_VOID(p_ble_evt);
    VERIFY_MODULE_INITIALIZED_VOID();
	
	


    switch (p_ble_evt->header.evt_id)
    {
        // Upon connection, disable notification of scan results to application.
        case BLE_GAP_EVT_CONNECTED:
            on_connected(p_eartag_scan, p_ble_evt);
            break;

        // Upon disconnection, enable notification of scan results to application.
        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnected(p_eartag_scan, p_ble_evt);
            break;

        // Upon advertising report....
        case BLE_GAP_EVT_ADV_REPORT:
            on_adv_report(p_eartag_scan, p_ble_evt);
            break;

        default:
            break;
    }
}


