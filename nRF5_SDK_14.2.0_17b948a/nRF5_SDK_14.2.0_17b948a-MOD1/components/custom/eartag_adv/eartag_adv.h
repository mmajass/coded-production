/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/**@file
 *
 * @defgroup EarTag Advertising Module
 * @{
 * @ingroup  eartag_lib
 * @brief    Module for handling Coded PHY non-connectable advertising and 1Mbps
 *           connectable advertising.
 *
 * @details  The Advertising Module handles Coded PHY non-connectagle advertising
 *           and 1Mbps connectable advertising for your application.
 *
 * @note     The Code PHY advertising must be connectable in s140_nrf52840_6.0.0-6.alpha.
 *
 */

#ifndef EARTAG_ADV_H__
#define EARTAG_ADV_H__

#include <stdint.h>
#include "nrf_error.h"
#include "ble.h"
#include "ble_gattc.h"
#include "ble_advdata.h"

#ifdef __cplusplus
extern "C" {
#endif

/**@brief   Macro for defining a eartag_adv instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define EARTAG_ADV_DEF(_name)                                                  \
static eartag_adv_t _name;                                                     \
NRF_SDH_BLE_OBSERVER(_name ## _ble_obs,                                        \
                     BLE_ADV_BLE_OBSERVER_PRIO,                                \
                     eartag_adv_on_ble_evt, &_name);                           \
NRF_SDH_SOC_OBSERVER(_name ## _soc_obs,                                        \
                     BLE_ADV_SOC_OBSERVER_PRIO,                                \
                     eartag_adv_on_sys_evt, &_name)


#define EARTAG_ADV_MANU_SPEC_DATA_MAX_LENGTH        32      /**< MAximum length of manufacturer specific data. */

/**@brief   State machine. */
typedef enum
{
    EARTAG_ADV_STATE_INIT = 0,      /**< Right after boot. Initialization is required. */
    EARTAG_ADV_STATE_IDLE,          /**< Idle. */
    EARTAG_ADV_STATE_CODED_NONCONN, /**< Non-connectable advertising with coded PHY is active. */
    EARTAG_ADV_STATE_LEGACY_CONN,   /**< Legacy connectable advertising using 1Mbps PHY. */
    EARTAG_ADV_STATE_CONNECTED,     /**< Connected. */
} eartag_adv_state_t;

/**@brief   Eartag Advertising events.
 *
 * @details These events are propagated to the main application if a handler was
 *          provided during initialization of the Eartag Advertising Module.
 */
typedef enum
{
    EARTAG_ADV_EVT_STOPPED,             /**< Stopped.*/
    EARTAG_ADV_EVT_CODED_NONCONN_ADV,   /**< Non-connectable advertising with coded PHY has started. */
    EARTAG_ADV_EVT_LEGACY_ADV,          /**< Legacy connectable advertising using 1Mbps PHY has started. */
} eartag_adv_evt_t;

/**@brief   EarTag advertising event handler type. */
typedef void (*eartag_adv_evt_handler_t) (eartag_adv_evt_t const adv_evt);

/**@brief   EarTag advertising error handler type. */
typedef void (*eartag_adv_error_handler_t) (uint32_t nrf_error);

typedef struct
{
    eartag_adv_state_t         state;           /**< Variable to keep track of the current state. */
    eartag_adv_evt_t           adv_evt;         /**< Eartag Advertising event propogated to the main application. */
    uint8_t                    conn_cfg_tag;    /**< Variable to keep track of what connection settings will be used if the advertising results in a connection. */

    struct
    {
        bool                   start_pending;   /**< Flag which tells whether previous request has failed. */
        ble_advdata_t          advdata;         /**< Used by the initialization function to set name, appearance, and UUIDs and advertising flags visible to peer devices. */
        ble_gap_adv_params_t   advparam;        /**< Advertising parameters. */
    } legacy;

    struct
    {
        bool                   start_pending;   /**< Flag which tells whether previous request has failed. */
        ble_advdata_t          advdata;         /**< Used by the initialization function to set name, appearance, and UUIDs and advertising flags visible to peer devices. */
        ble_gap_adv_params_t   advparam;        /**< Advertising parameters. */
    } coded;

    uint8_t                    fw_ver;          /**< Firmware version. */
    uint8_t                    batt_lv;         /**< Battery level. */
    uint8_t                    temperature;     /**< Temperature. */
		uint8_t										 pair0_or_gap2;		/**< Full gap id or pair shortened id position. */
		uint8_t										 pair1_or_gap1;		/**< Full gap id or pair shortened id position. */
		uint8_t										 pair2_or_gap0;		/**< Full gap id or pair shortened id position. */
		char	                     payload_var;     /**< Variable payload based on packet type */
		char											 packet_version;	/**< Packet version specifying payload data */
		char											 accel_xdata;			/**< Accelerometer x sum of acceleration change	*/
		char											 accel_ydata;			/**< Accelerometer y sum of acceleration change */
		char											 accel_zdata;			/**< Accelerometer z sum of acceleration change */
		char											 accel_xpeak;			/**< Average acceleration peak on x vector		*/
		char											 accel_ypeak;			/**< Average acceleration peak on y vector		*/
		char											 accel_zpeak;			/**< Average acceleration peak on z vector		*/
		
		
    eartag_adv_evt_handler_t   evt_handler;     /**< Handler for the events. Can be initialized as NULL if no handling is implemented on in the main application. */
    eartag_adv_error_handler_t error_handler;   /**< Handler for the error events. */

    uint8_t                    manu_spec_data_len; /**< Length of manufacturer specific data. */
    uint8_t                    manu_spec_data[EARTAG_ADV_MANU_SPEC_DATA_MAX_LENGTH]; /**< Encoded manufacturer specific data. */
} eartag_adv_t;


/**@brief   Initialization parameters for the EarTag Advertising Module.
 * @details This structure is used to pass advertising options, advertising data,
 *          and an event handler to the EarTag Advertising Module during initialization.
 */
typedef struct
{
    ble_advdata_t              advdata_legacy;  /**< Advertising data for legacy advertising. */
    uint8_t                    conn_cfg_tag;    /**< Variable to keep track of what connection settings will be used if the advertising results in a connection. */
    uint32_t                   interval_legacy; /**< Legacy mode: Advertising interval. See @ref BLE_GAP_ADV_INTERVALS. */
    uint16_t                   duration_legacy; /**< Legacy mode: Advertising duration between 0x0001 and 0xFFFF in 10ms units. Setting the value to 0x0000 disables the timeout. */
    uint32_t                   interval_coded;  /**< Coded mode: Advertising interval. See @ref BLE_GAP_ADV_INTERVALS. */
    uint8_t                    fw_ver;          /**< Firmware version. */
    uint8_t                    batt_lv;         /**< Initial battery level. */
    uint8_t                    temperature;     /**< Initial temperature. */
		uint8_t										 pair0_or_gap2;		/**< Full gap id or pair shortened id position. */
		uint8_t										 pair1_or_gap1;		/**< Full gap id or pair shortened id position. */
		uint8_t										 pair2_or_gap0;		/**< Full gap id or pair shortened id position. */
		char	                     payload_var;     /**< Variable payload based on packet type */
		char											 packet_version;	/**< Packet version specifying payload data */
		char											 accel_xdata;			/**< Accelerometer x sum of acceleration change	*/
		char											 accel_ydata;			/**< Accelerometer y sum of acceleration change */
		char											 accel_zdata;			/**< Accelerometer z sum of acceleration change */
		char											 accel_xpeak;			/**< Average acceleration peak on x vector		*/
		char											 accel_ypeak;			/**< Average acceleration peak on y vector		*/
		char											 accel_zpeak;			/**< Average acceleration peak on z vector		*/
    eartag_adv_evt_handler_t   evt_handler;     /**< Event handler that will be called upon events. */
    eartag_adv_error_handler_t error_handler;   /**< Error handler that will propogate internal errors to the main applications. */
} eartag_adv_init_t;


/**@brief   Function for handling BLE events.
 *
 * @details This function must be called from the BLE stack event dispatcher for
 *          the module to handle BLE events that are relevant for the EarTag
 *          Advertising Module.
 *
 * @param[in] p_ble_evt     BLE stack event.
 * @param[in] p_eartag_adv  EarTag Advertising module instance.
 */
void eartag_adv_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_eartag_adv);


/**@brief   Function for handling system events.
 *
 * @details This function must be called to handle system events that are relevant
 *          for the Advertising Module. Specifically, the eartag advertising module
 *          can not use the softdevice as long as there are pending writes to the
 *          flash memory. This event handler is designed to delay advertising until
 *          there is no flash operation.
 *
 * @param[in] evt_id        System event.
 * @param[in] p_context     EarTag Advertising module instance.
 */
void eartag_adv_on_sys_evt(uint32_t evt_id, void * p_context);


/**@brief   Function for initializing the EarTag Advertising Module.
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 *          The supplied advertising data is copied to a local structure and is
 *          manipulated depending on what advertising modes are started in @ref eartag_adv_start.
 *
 * @param[out] p_eartag_adv EarTag Advertising module instance. This structure 
 *                          must be supplied by the application. It is initialized
 *                          by this function and will later be used to identify
 *                          this particular module instance.
 * @param[in] p_init        Information needed to initialize the module.
 *
 * @retval NRF_SUCCESS If initialization was successful. Otherwise, an error code is returned.
 */
uint32_t eartag_adv_init(eartag_adv_t            * const p_eartag_adv,
                         eartag_adv_init_t const * const p_init);


 /**@brief  Function for changing the connection settings tag that will be used for upcoming connections.
 *
 * @details See @ref sd_ble_cfg_set for more details about changing connection settings. If this
 *          function is never called, @ref BLE_CONN_CFG_TAG_DEFAULT will be used.
 *
 * @param[in] p_eartag_adv EarTag Advertising module instance.
 * @param[in] ble_cfg_tag  Configuration for the connection settings (see @ref sd_ble_cfg_set).
 */
void eartag_adv_conn_cfg_tag_set(eartag_adv_t * const p_eartag_adv, uint8_t ble_cfg_tag);


/**@brief   Function for starting non-connectable advertising using coded PHY.
 *
 * @details You can start non-connectable advertising using coded PHY.
 *
 * @param[in] p_eartag_adv EarTag Advertising module instance.
 *
 * @retval @ref NRF_SUCCESS On success, else an error code indicating reason for failure.
 * @retval @ref NRF_ERROR_INVALID_STATE If the module is not initialized.
 */
uint32_t eartag_adv_start_coded_nonconnectable(eartag_adv_t * const p_eartag_adv);


/**@brief   Function for starting legacy connectable advertising.
 *
 * @details You can start legacy connectable advertising.
 *
 * @param[in] p_eartag_adv EarTag Advertising module instance.
 *
 * @retval @ref NRF_SUCCESS On success, else an error code indicating reason for failure.
 * @retval @ref NRF_ERROR_INVALID_STATE If the module is not initialized.
 */
uint32_t eartag_adv_start_legacy(eartag_adv_t * const p_eartag_adv);


/**@brief   Function for setting battery level.
 *
 * @details You can set battery level, which will then be reflected in manufacturer
 *          specific advertising data.
 *
 * @param[in] p_eartag_adv EarTag Advertising module instance.
 * @param[in] batt_lv      Battery level.
 *
 * @retval @ref NRF_SUCCESS On success, else an error code indicating reason for failure.
 * @retval @ref NRF_ERROR_INVALID_STATE If the module is not initialized.
 * @retval @ref NRF_ERROR_INVALID_DATA  If battery level is out of range.
 */
uint32_t eartag_adv_set_batt_lv(eartag_adv_t * const p_eartag_adv, uint8_t batt_lv);


/**@brief   Function for setting temperature.
 *
 * @details You can set temperature, which will then be reflected in manufacturer
 *          specific advertising data.
 *
 *
 * @param[in] p_eartag_adv EarTag Advertising module instance.
 * @param[in] temperature  Temperature.
 *
 * @retval @ref NRF_SUCCESS On success, else an error code indicating reason for failure.
 * @retval @ref NRF_ERROR_INVALID_STATE If the module is not initialized.
 */
uint32_t eartag_adv_set_temperature(eartag_adv_t * const p_eartag_adv, uint8_t temperature);

uint32_t eartag_adv_change_short_type(eartag_adv_t * const p_eartag_adv, char packet_type, char payload_var);

uint32_t eartag_adv_change_short_low_power_type(eartag_adv_t * const p_eartag_adv, char packet_type, char payload_var);

uint32_t eartag_adv_change_long_type(eartag_adv_t * const p_eartag_adv, char packet_type, char payload_var, char accelx, char accely, char accelz, char peakx, char peaky, char peakz);

uint32_t eartag_adv_change_long_pair_type(eartag_adv_t * const p_eartag_adv, uint8_t pair0, uint8_t pair1, uint8_t pair2, char packet_type, char payload_var, char accelx, char accely, char accelz, char peakx, char peaky, char peakz);


/** @} */


#ifdef __cplusplus
}
#endif

#endif // EARTAG_ADV_H__

/** @} */
